// Main menu for screens less than 1024px

export default initMainMenuMobile;

function initMainMenuMobile() {
	$('.main-navbar__menu li.dropdown > a').click(function(event) {
		event.preventDefault();
	});

	$('.main-navbar__menu li.dropdown a').click(function() {
		$(this).parent('li').toggleClass('open');
	});
};
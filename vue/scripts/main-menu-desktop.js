// Main menu for screens more than 1024px

export default initMainMenuDesktop;

function initMainMenuDesktop() {
	function is_touch_device() {  
	  try {  
	    document.createEvent("TouchEvent");  
	    return true;  
	  } catch (e) {  
	    return false;  
	  }  
	}

	function drawSubmenu(menuParent){
		var submenuId = menuParent.children('ul').attr('id'),
			newMenuEl = $('#'+submenuId).clone().appendTo('.egw-main-menu__submenus'),
			mainMenuPos = $('#egw-main-menu')[0].getBoundingClientRect(),
			parentPos = menuParent[0].getBoundingClientRect(),
			submenuPos = {};

		submenuPos = {
			top: (parentPos.top - mainMenuPos.top),
			left: parentPos.right,
			right: (parentPos.left - newMenuEl.width()),
			maxHeight: ($(window).height() - parentPos.top - 20)
		}

		if($(window).width() > parentPos.right+newMenuEl.width()){
			newMenuEl.css('top', submenuPos.top).css('left', submenuPos.left).css('max-height', submenuPos.maxHeight).show();
		} else {
			newMenuEl.css('top', submenuPos.top).css('left', submenuPos.right).css('max-height', submenuPos.maxHeight).show();
		}
	};


	$('#egw-main-menu, .egw-main-menu__submenus').on('click', 'a', function(e){
		if($(this).siblings('ul.dropdown-menu').length){
			e.preventDefault();
		}
	});

	$('#egw-main-menu .nav ul').each(function(index, el) {
		$(this).attr('id', 'submenu'+index);
		$(this).attr('data-menulvl', $(this).parents('ul').length);
	});


	if(!is_touch_device()){
		$('.egw-main-menu__submenus').on('mouseenter', 'li.dropdown-submenu', function(){
			$('.egw-main-menu__submenus > .dropdown-menu[data-menulvl="'+$(this).children('.dropdown-menu').attr('data-menulvl')+'"]').remove();
			drawSubmenu($(this));
		}).on('mouseenter', '.dropdown-menu', function(event) {
			$('.egw-main-menu__submenus .dropdown-menu, #egw-main-menu .nav > li.dropdown > ul').stop(true, true);
		}).on('mouseleave', '.dropdown-menu', function(event) {
			if ($(event.relatedTarget).parents('.dropdown-menu').attr('data-menulvl') || $(event.relatedTarget).attr('data-menulvl')){
	 			// Do nothing
	 		} else {
				$('.egw-main-menu__submenus .dropdown-menu, #egw-main-menu .nav > li.dropdown > ul').stop(true, true).delay(500).fadeOut(400, function(){
					$('.egw-main-menu__submenus .dropdown-menu').remove();	
				});
			}
		});

		$('#egw-main-menu li.dropdown-submenu').mouseenter(function(){
			$('.egw-main-menu__submenus .dropdown-menu').remove();
			drawSubmenu($(this));
		}).mouseleave(function(event){
			if($(event.relatedTarget).attr('id') == $(this).parent().attr('id') || $(event.relatedTarget).parents('.dropdown-menu').attr('data-menulvl') > 1 || $(event.relatedTarget).attr('data-menulvl') > 1) {
				// Do nothing
			} else if($(event.relatedTarget).parents('.dropdown-menu').attr('data-menulvl') == 1 || $(event.relatedTarget).attr('data-menulvl') == 1){
				$('.egw-main-menu__submenus .dropdown-menu').stop(true, true).delay(500).fadeOut(400, function(){
					$(this).remove();	
				});
			} else {
				$('.egw-main-menu__submenus .dropdown-menu, #egw-main-menu .nav > li.dropdown > ul').stop(true, true).delay(500).fadeOut(400, function(){
					$('.egw-main-menu__submenus .dropdown-menu').remove();	
				});
			}
		});

		$('#egw-main-menu .nav > li.dropdown').mouseenter(function(e){
			if($(e.target).siblings('.dropdown-menu').attr('data-menulvl') == 1) {
				$('.egw-main-menu__submenus .dropdown-menu').remove();
			};
			$(this).parent().children('.dropdown').children('.dropdown-menu').stop(true, true).fadeOut(400);
			$(this).children('.dropdown-menu').stop(true, true).show().css('max-height', $(window).height() - $(this).children('.dropdown-menu')[0].getBoundingClientRect().top - 20);
			if($(this).children('.dropdown-menu')[0].getBoundingClientRect().right > $(window).width()){
				$(this).children('.dropdown-menu').css({
					'left': 'auto',
					'right': 0
				});
			}
		}).mouseleave(function(e) {
			if ($(e.relatedTarget).parents('#egw-main-menu .nav, .egw-main-menu__submenus').length == 0) {
				$(this).children('.dropdown-menu').stop(true, true).delay(500).fadeOut(400);
			}
		});
	} else {
		$('#egw-main-menu .nav > li.dropdown > a').click(function(){
			$('#egw-main-menu .nav > li.dropdown > ul.dropdown-menu').hide();
			$('.egw-main-menu__submenus .dropdown-menu').remove();
			if($(this).parent().hasClass('opened')) {
				$(this).siblings('ul.dropdown-menu').hide();
				$('#egw-main-menu .nav > li.dropdown').removeClass('opened');
			} else {
				$('#egw-main-menu .nav > li.dropdown').removeClass('opened');
				$(this).parent().addClass('opened');
				$(this).siblings('ul.dropdown-menu').show().css('max-height', $(window).height() - $(this).siblings('ul.dropdown-menu')[0].getBoundingClientRect().top - 20);

				if($(this).siblings('ul.dropdown-menu')[0].getBoundingClientRect().right > $(window).width()){
					$(this).siblings('ul.dropdown-menu').css({
						'left': 'auto',
						'right': 0
					});
				}
			}
		});
		$('#egw-main-menu li.dropdown-submenu').click(function(){
			$('.egw-main-menu__submenus .dropdown-menu').remove();
			drawSubmenu($(this));
		});
		$('.egw-main-menu__submenus').on('click', 'li.dropdown-submenu', function(event) {
			$('.egw-main-menu__submenus > .dropdown-menu[data-menulvl="'+$(this).children('.dropdown-menu').attr('data-menulvl')+'"]').remove();
			drawSubmenu($(this));
		});

		$('body').click(function(e){
			if($(e.target).parents('#egw-main-menu').length == 0){
				$('.egw-main-menu__submenus .dropdown-menu').remove();
				$('#egw-main-menu .nav > li.dropdown').removeClass('opened');
				$('#egw-main-menu .nav > li.dropdown > ul.dropdown-menu').hide();
			};
		});

		$('#egw-main-menu').addClass('touch-device');
	}
};
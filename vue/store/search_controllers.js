let searchControllers = {
    search_text: {
        type: 'search-text',
        value: null, // string or null
    },
    types: {
        correspondence: {
            type: 'checkbox',
            label: 'Correspondence',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {
                collection: {
                    type: 'multiselect',
                    label: 'Collections',
                    amount: 0,
                    options: [],
                    value: []
                },
                senders: {
                    type: 'multiselect',
                    label: 'Senders',
                    amount: 0,
                    options: [],
                    value: []
                },
                sender_location: {
                    type: 'multiselect',
                    label: 'Sender location',
                    amount: 0,
                    options: [],
                    value: []
                },
                receivers: {
                    type: 'multiselect',
                    label: 'Receivers',
                    amount: 0,
                    options: [],
                    value: []
                },
                receiver_location: {
                    type: 'multiselect',
                    label: 'Receiver location',
                    amount: 0,
                    options: [],
                    value: []
                },
                correspondence_type: {
                    type: 'multiselect',
                    label: 'Correspondence type',
                    amount: 0,
                    options: [],
                    value: []
                },
                is_public: {
                    type: 'select',
                    label: 'Is public',
                    amount: 0,
                    options: [true, false, null], // bool or null. Available only for the users who have permissions,
                    value: null
                },
                letterhead: {
                    type: 'select',
                    label: 'Letterhead',
                    amount: 0,
                    options: [true, false, null], // bool or null
                    value: null
                },
                handwritten: {
                    type: 'select',
                    label: 'Handwritten',
                    amount: 0,
                    options: [true, false, null], // bool or null
                    value: null
                },
                typewritten: {
                    type: 'select',
                    label: 'Typewritten',
                    amount: 0,
                    options: [true, false, null], // bool or null
                    value: null
                },
            }
        },
        location: {
            type: 'checkbox',
            label: 'Location',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {}
        },
        pamphlet: {
            type: 'checkbox',
            label: 'Pamphlets',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {}
        },
        person: {
            type: 'checkbox',
            label: 'People',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {}
        },
        publication: {
            type: 'checkbox',
            label: 'Publications',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {
                ellenOnly: {
                    type: 'checkbox',
                    label: 'Ellen White Only',
                    amount: 0,
                    value: false, // false
                }
            }
        },
        event: {
            type: 'checkbox',
            label: 'Events',
            amount: 0,
            value: false, // bool
            defaultSort: 'newest',
            controllers: {},
        },
        book: {
            type: 'checkbox',
            label: 'Letterbooks',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {}
        },
        article: {
            type: 'checkbox',
            label: 'Articles',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {},
        },
        lesson: {
            type: 'checkbox',
            label: 'Lessons',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        },
        news: {
            type: 'checkbox',
            label: 'News',
            amount: 0,
            value: false, // bool
            defaultSort: 'newest',
            controllers: {}
        },
        topic: {
            type: 'checkbox',
            label: 'Topics',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {}
        },
        faq: {
            type: 'checkbox',
            label: 'FAQ',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        },
        media_doc: {
            type: 'checkbox',
            label: 'Documents',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {},
        },
        media_image: {
            type: 'checkbox',
            label: 'Images',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        },
        media_audio: {
            type: 'checkbox',
            label: 'Audio',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        },
        media_video: {
            type: 'checkbox',
            label: 'Video',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        },
    },
    media_types : {
        document: {
            type: 'checkbox',
            label: 'Documents',
            amount: 0,
            value: false, // bool
            defaultSort: 'alphabeticalDown',
            controllers: {},
        },
        image: {
            type: 'checkbox',
            label: 'Images',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        },
        audio: {
            type: 'checkbox',
            label: 'Audio',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        },
        video: {
            type: 'checkbox',
            label: 'Video',
            amount: 0,
            value: false, // bool
            defaultSort: 'relevant',
            controllers: {},
        }
    },
    date: {
        minDatetime: new Date('1786/01/01'),
        maxDatetime: new Date('2017/08/08'),
        dateTimeFormat: 'yyyy/mm/dd',
        enabled: false,
        value: {
            start: null,
            end: null
        }, // bool
    },
    galleriesOptions: {
        type: 'multiselect',
        label: 'Receiver location',
        amount: 0,
        options: []
    },
    pager: {
        offset: 0, // integer
        limit: 25, // integer
    },
    // sort object
    sort: {
        type: 'select',
        value: 'relevant',
        options: [
            {
                label: 'Relevant',
                value: 'relevant'
            },
            {
                label: 'Newest',
                value: 'newest'
            },
            {
                label: 'Oldest',
                value: 'oldest'
            },
            {
                label: 'Alphabetical (A to Z)',
                value: 'alphabeticalDown'
            },
            {
                label: 'Alphabetical (Z to A)',
                value: 'alphabeticalUp'
            },
        ],
    },
};

export { searchControllers};

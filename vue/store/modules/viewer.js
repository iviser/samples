

// initial state
const state = {
    enabled: false,
    refsOpen: true,
    item: {},
}

// getters
const getters = {
    getViewer: (state) => {
        return {
            enabled: state.enabled,
            refsOpen: true,
            item: state.item
        }
    }
}

// actions
const actions = {

}

// mutations
const mutations = {
    setPathValue: (state, {path, value}) => {
        _.set(state, path , value);
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}

import Vue from 'vue'
import Vuex from 'vuex'
import viewerModule from './modules/viewer'
import writingsModule from './modules/writings'
import { searchControllers } from './search_controllers';

var _ = require('lodash');
var defaultControllers = _.cloneDeep(searchControllers);

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        viewer: viewerModule,
        writings: writingsModule
    },

    state: {
        controllers: searchControllers,
        response: {},
        user: {
            roles: [],
            permissions: [],
        },
        defaultControllers: defaultControllers
    },

    mutations: {
        /**
         * Sets controller's value
         * @see  state.controllers
         * @param state
         * @param path
         *      Controller name or path if controller is nested @see https://lodash.com/docs/4.17.4#set
         * @param value
         *      Value to be set
         */
        controllerPathSetValue: (state, {path, value}) => {
            if (_.has(state.controllers, path)) {
                _.set(state.controllers, path , value);
            }
        },

        /**
         *
         * @param state
         * @param playload
         * @todo remove this method since it will be replaced ny the
         */
        updateTypesAmount: (state, playload) => {
            let bucketsIntoObject = function(playload) {
                let obj = {};
                _.forEach(playload.buckets, function(value) {
                    if (_.has(state.controllers.types, value['key'])) {
                        obj[value['key']] = value['doc_count'];
                    }
                });

                return obj;
            };

            let buckets = bucketsIntoObject(playload);

            _.forOwn(state.controllers.types, function(value, key) {
                if (buckets[key] > 0) {
                    value['amount'] = buckets[key];
                } else {
                    value['amount'] = 0;
                }
            });
        },

        /**
         * Update types amount
         * @todo Get rid of code duplication
         */
        updateTypesAmounts: (state, {aggregations}) => {
            var obj = {};
            if (_.get(aggregations, 'type.buckets', null)) {
                _.forEach(aggregations.type.buckets, function({key, doc_count}) {
                    obj[key] = doc_count;
                });
            }
            _.forEach(state.controllers.types, function (value, key) {
                if (_.has(obj, key)) {
                    value['amount'] = obj[key];
                } else {
                    value['amount'] = 0;
                }
            });
        },
        /**
         * Update media types amount
         * @todo Get rid of code duplication
         */
        updateMediaTypesAmounts: (state, {aggregations}) => {
            var obj = {};
            if (_.get(aggregations, 'media_type.buckets', null)) {
                _.forEach(aggregations.media_type.buckets, function({key, doc_count}) {
                    obj[key] = doc_count;
                });
            }
            _.forEach(state.controllers.media_types, function (value, key) {
                if (_.has(obj, key)) {
                    value['amount'] = obj[key];
                } else {
                    value['amount'] = 0;
                }
            });
        },

        /**
         * Update response
         */
        updateResponse: (state, playload) => {
            state.response = playload;
        },

        /**
         * Update types controllers
         * @TODO function is similar to defaultMediaTypesControllers.
         * Get rid of code duplication
         */
        defaultTypesControllers: (state, {path}) => {
            if (_.has(state.controllers.types, path)) {
                state.controllers.types[path].controllers = state.defaultControllers.types[path].controllers;
            }
        },

        /**
         * Update media types controllers
         * @TODO function is similar to defaultTypesControllers.
         * Get rid of code duplication
         */
        defaultMediaTypesControllers: (state, {path}) => {
            if (_.has(state.controllers.media_types, path)) {
                state.controllers.media_types[path].controllers = state.defaultControllers.media_types[path].controllers;
            }
        },
    },
    getters: {
        getControllers: (state) => state.controllers,
        getTypes: (state) => state.controllers.types,
        getTotalPages: (state) => _.get(state.response, 'hits.hits.total', 0),
        searchResults: (state) => {
            if (_.has(state.response, 'hits.hits.hits'))
                return state.response.hits.hits.hits
        }
    },
    actions: {
        /**
         * Perform search request
         * @todo remake logic in order to user store.state.
         */
        performRequest({state, dispatch}) {
            // Content types
            var filtersTypes = {};
            _.forEach(_.pickBy(state.controllers.types, (type) => type.value), function(value, key){
                let type = {
                    controllers: {}
                };

                _.forEach(value.controllers, function(value, key){
                    if(value.value == null || (_.isArray(value.value) && value.value.length == 0)){
                        // Do nothing
                    } else {
                        let typeController = {
                            type: value.type,
                            value: value.value
                        };

                        type.controllers[key] = typeController;
                    }
                });

                filtersTypes[key] = type;
            });

            // Media types
            var filtersMediaTypes = {};
            _.forEach(_.pickBy(state.controllers.media_types, (type) => type.value), function(value, key){

                let type = { controllers: {} };

                _.forEach(value.controllers, function(value, key){
                    if(value.value == null || (_.isArray(value.value) && value.value.length == 0)){
                        // Do nothing
                    } else {
                        type.controllers[key] = {
                            type: value.type,
                            value: value.value
                        };
                    }
                });

                filtersMediaTypes[key] = type;
            });

            return Vue.http.get('/search/docs', {
                params: {
                    'search_text': state.controllers.search_text.value,
                    'offset': state.controllers.pager.offset,
                    'limit': state.controllers.pager.limit,
                    'filters': JSON.stringify({
                        types: filtersTypes,
                        media_types: filtersMediaTypes,
                        dateEnabled: state.controllers.date.enabled,
                        startTime: state.controllers.date.value.start,
                        endTime: state.controllers.date.value.end,
                        sort: state.controllers.sort.value
                    })
                }
            });
        },
        clearSearchFilters({state}){
            _.forEach(state.defaultControllers, (value, key) => {
                if(key !== "search_text"){
                    state.controllers[key] = _.cloneDeep(value);
                }
            });
        },
    }
});
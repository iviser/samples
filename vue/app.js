
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

const bootstrap = require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

// Vue config
Vue.config.ignoredElements = ['question', 'answer'];

// Search
Vue.component('search', require('./components/Search/Search.vue'));
Vue.component('search-results', require('./components/Search/SearchResults.vue'));

// Search Controllers
Vue.component('controllers', require('./components/Search/Controllers/Controllers.vue'));
Vue.component('top-controllers', require('./components/Search/Controllers/TopControllers.vue'));
Vue.component('search-text', require('./components/Search/Controllers/SearchText.vue'));
Vue.component('query-options-content', require('./components/Search/Controllers/QueryOptionsContent.vue'));

// Search nested controllers
Vue.component('controllers-correspondence', require('./components/Search/Controllers/ContentTypeControllers/CorrespondenceControllers.vue'));
// Vue.component('controllers-publication', require('./components/Search/Controllers/ContentTypeControllers/PublicationControllers.vue'));

// Search Viewers
Vue.component('search-viewer', require('./components/Search/Viewers/Viewers.vue'));
Vue.component('search-viewer-corr', require('./components/Search/Viewers/ViewerCorr.vue'));
Vue.component('search-viewer-location', require('./components/Search/Viewers/ViewerLocation.vue'));
Vue.component('search-viewer-pamphlet', require('./components/Search/Viewers/ViewerPamphlet.vue'));
Vue.component('search-viewer-people', require('./components/Search/Viewers/ViewerPeople.vue'));
Vue.component('search-viewer-publication', require('./components/Search/Viewers/ViewerPublication.vue'));
Vue.component('search-viewer-event', require('./components/Search/Viewers/ViewerEvent.vue'));
Vue.component('search-viewer-book', require('./components/Search/Viewers/ViewerBook.vue'));
Vue.component('search-viewer-article', require('./components/Search/Viewers/ViewerArticle.vue'));
Vue.component('search-viewer-lesson', require('./components/Search/Viewers/ViewerLesson.vue'));
Vue.component('search-viewer-news', require('./components/Search/Viewers/ViewerNews.vue'));
Vue.component('search-viewer-topic', require('./components/Search/Viewers/ViewerTopic.vue'));
Vue.component('search-viewer-faq', require('./components/Search/Viewers/ViewerFaq.vue'));
Vue.component('search-viewer-doc', require('./components/Search/Viewers/ViewerDoc.vue'));
Vue.component('search-viewer-img', require('./components/Search/Viewers/ViewerImg.vue'));
Vue.component('search-viewer-audio', require('./components/Search/Viewers/ViewerAudio.vue'));
Vue.component('search-viewer-video', require('./components/Search/Viewers/ViewerVideo.vue'));

// Pages
Vue.component('correspondence-list', require('./components/Pages/correspondence/List.vue'));
Vue.component('correspondence-single', require('./components/Pages/correspondence/Single.vue'));
Vue.component('Faq', require('./components/Pages/Faq.vue'));
Vue.component('locations', require('./components/Pages/location/List.vue'));
Vue.component('location', require('./components/Pages/location/Single.vue'));
Vue.component('branches', require('./components/Pages/Branches.vue'));
Vue.component('feedback', require('./components/Pages/Feedback.vue'));
Vue.component('people', require('./components/Pages/people/List.vue'));
Vue.component('person', require('./components/Pages/people/Single.vue'));
Vue.component('articles', require('./components/Pages/Articles.vue'));
Vue.component('news', require('./components/Pages/News.vue'));
Vue.component('images-list', require('./components/Pages/images/List.vue'));
Vue.component('images-single-page', require('./components/Pages/images/Single.vue'));
Vue.component('staff-page', require('./components/Pages/Staff.vue'));

// Main
Vue.component('main-menu', require('./components/MainMenu/Index.vue'));
Vue.component('main-menu-subitem', require('./components/MainMenu/SubItem.vue'));

// Other
Vue.component('refs-list', require('./components/ReferencesList.vue'));
Vue.component('pagination', require('./components/Pagination.vue'));
Vue.component('multiselect', require('./components/Multiselect.vue'));
Vue.component('multiselect-remote', require('./components/MultiselectRemote.vue'));
Vue.component('select-remote', require('./components/SelectRemote.vue'));
Vue.component('image-viewer', require('./components/ImageViewer.vue'));
Vue.component('flowpaper-viewer', require('./components/FlowpaperViewer.vue'));
Vue.component('letterbook', require('./components/Letterbook.vue'));
Vue.component('audio-player', require('./components/AudioPlayer.vue'));
Vue.component('home-section', require('./components/HomeSection.vue'));

import store from './store';

const app = new Vue({
    el: '#app',
    apolloProvider,
    store
});

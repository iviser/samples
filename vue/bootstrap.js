window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
window.gpl = require('graphql-tag');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');
window.VueResource = require('vue-resource');
window.VueRouter = require('vue-router');
window.VueGoogleMaps = require('vue2-google-maps');
window.SimpleVueValidation = require('simple-vue-validator');

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(SimpleVueValidation);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAtKJall6X5HN_b5hJW21Eq345uDsaf92c',
        v: '3',
        libraries: 'places'
    }
});


/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);
    next();
});

// ------------------------------------------------------------------------------------------------------

// import { ApolloClient } from 'apollo-client'
// import { HttpLink } from 'apollo-link-http'
// import VueApollo from 'vue-apollo'
//
// const httpLink = new HttpLink({
//     uri: window.location.origin + '/graphql',
// })
//
// const apolloClient = new ApolloClient({
//     link: httpLink,
//     connectToDevTools: true,
// })

import { ApolloClient, createNetworkInterface } from 'apollo-client'
import VueApollo from 'vue-apollo'

// Create the apollo client
const apolloClient = new ApolloClient({
    networkInterface: createNetworkInterface({
        uri: window.location.origin + '/graphql',
        transportBatching: true,
    }),
    connectToDevTools: true,
});

Vue.use(VueApollo);

window.apolloProvider = new VueApollo({
    defaultClient: apolloClient,
});

<?php

namespace Modules\Books\Es\MappingTypes;


use Phirames\LaraElastic\MappingTypes\MappingTypeInterface;

class BookMappingType implements MappingTypeInterface
{
    public static function name(): string
    {
        return 'book';
    }

    public static function properties(): array
    {
        return [];
    }
}

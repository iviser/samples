<?php

namespace Modules\Books\Console;

use Illuminate\Console\Command;
use Modules\Books\Entities\Book;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DeleteAllBooks extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'books:delete-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes all books and references.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $delIds = [];
        $bookClass = Book::class;

        foreach (Book::all() as $book) {
            $delIds[$bookClass][] = $book->id;
            if ($book->references) {
                if(isset($book->references->medium_resolution) && $book->references->medium_resolution) {
                    $delIds[$book->references->medium_resolution->type][] = $book->references->medium_resolution->id;
                }
                if(isset($book->references->text_copy) && $book->references->text_copy) {
                    $delIds[$book->references->text_copy->type][] = $book->references->text_copy->id;
                }
            }
        }
        foreach ($delIds as $type => $ids) {
            $type::whereIn('id', $ids)->delete();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}

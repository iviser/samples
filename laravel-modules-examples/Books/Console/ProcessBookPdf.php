<?php

namespace Modules\Books\Console;

use Illuminate\Console\Command;
use Modules\Books\Entities\Book;
use Modules\Flowpaper\Jobs\ProcessPdfJob;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProcessBookPdf extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'books:process-pd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(Book::all() as $book) {
            if ($pdf = $book->pdf) {
                dispatch(new ProcessPdfJob($pdf->getPath()));
            }
            if ($txt = $book->txt) {
                dispatch(new ProcessPdfJob($txt->getPath()));
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
//            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
//            ['id', null, InputOption::VALUE_OPTIONAL, 'Process particular book ID', null],
//            ['no-queue', null, InputOption::VALUE_OPTIONAL, 'Cancel queue execution', null],
        ];
    }
}

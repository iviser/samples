@extends('page-base')

@section('stylesheets')

@endsection

@section('page-header')
    <h1>{{ $book->title }}</h1>
@stop

@php


@endphp

@section('content')
    <div class="row media-letterbook">
        <div class="col-md-9">
            <letterbook
                first-pdf-path="{{ $pdf_path ?: null }}"
                first-pdf-url="{{ $pdf_url ?: null  }}"
                second-pdf-path="{{ $pdf_text_path ?: null  }}"
                second-pdf-url="{{ $pdf_text_url ?: null  }}"
            ></letterbook>
        </div>

        <div id="book-info" class="col-md-3 media-metadata">
            <div class="panel panel-default row">
                <div class="panel-heading"><h4>Metadata</h4></div>
                
                <div class="panel-body">
                    @if ($book->thumb_site_path)
                        <div class="text-center media-metadata__thumb">
                            <img src="{{ $book->thumb_site_path }}" alt="">
                        </div>
                    @endif
                    @if($author = $book->metadata->author)
                        <div><b>Author:</b> {{ $author }}</div>
                    @endif
                    @if($number = $book->metadata->number)
                        <div><b>Number:</b> {{ $number }}</div>
                    @endif
                    @if($status = $book->metadata->status)
                        <div><b>Status:</b> {{ $status }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        
    </script>
@endpush
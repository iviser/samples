@extends('page-base')

@section('stylesheets')

@endsection

@section('page-header')
    <h1>W.C. White Letter Books</h1>
@stop

@section('content')
    <div class="row">
        <table class="table table-bordered" id="book-data-table">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Date</th>
                <th>Number</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection

@push('scripts')
<script>
    $(function() {
        $('#book-data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('books.data') !!}',
            pageLength: 50,
            lengthMenu: [ 25, 50, 100, 250,  ],
            columns: [
                { data: 'title', name: 'title' },
                { data: 'author', name: 'author', searchable: false },
                { data: 'date', name: 'date', searchable: false },
                { data: 'number', name: 'number', searchable: false },
                { data: 'status', name: 'status', searchable: false },
            ]
        });
    });
</script>
@endpush
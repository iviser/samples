<?php

namespace Modules\Books\Tests\Api;

use App\Services\Registries\EntityRegistry;
use App\User;
use Illuminate\Http\UploadedFile;
use Modules\Books\Entities\Book;
use Tests\CreatesApplication;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Tests\Traits\TestReferenceTrait;
use Tests\Traits\TestTermsReferenceTrait;

class BooksApiCrudTest extends TestCase
{
    use CreatesApplication, TestReferenceTrait, TestTermsReferenceTrait;

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Books'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'Taxonomy'
        ]);
    }

    /**
     * @param User $user
     * @return Book
     * @throws \Exception
     */
    public function createBook(User $user): Book
    {
        $filename1 = "{$this->faker->md5}.pdf";
        $filename2 = "{$this->faker->md5}.pdf";

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'pdf' => UploadedFile::fake()->image($filename1),
            'pdf_text' => UploadedFile::fake()->image($filename2)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/letterbooks/",
            $payload,
            $this->getUserHeaders($user)
        )
            ->decodeResponseJson();

        $book = Book::find($resp['id']);

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();
        $book->ref($referencedACollection);
        $book->load('references');

        $book->load(['pdf', 'pdf_text']);

        return $book;
    }

    /**
     * Create book correctly.
     *
     * @return void
     */
    public function testBookIsCreatedCorrectly()
    {
        $user = $this->createUser();
        $filename1 = "{$this->faker->md5}.pdf";
        $filename2 = "{$this->faker->md5}.pdf";

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'pdf' => UploadedFile::fake()->image($filename1),
            'pdf_text' => UploadedFile::fake()->image($filename2)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/letterbooks?with[]=pdf&with[]=pdf_text",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
                'pdf' => [
                    'file_name' => $payload['pdf']->name
                ],
                'pdf_text' => [
                    'file_name' => $payload['pdf_text']->name
                ]
            ]);
    }

    /**
     * Create book correctly.
     *
     * @return void
     */
    public function testBookIsCreatedWithReferencesCorrectly()
    {
        $user = $this->createUser();

        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');
        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/letterbooks?with[]=references",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date']
            ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    public function testBookIsCreatedWithTermsCorrectly()
    {
        $user = $this->createUser();

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'terms' => json_encode($terms)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/letterbooks?with[]=terms",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
            ])
            ->assertJsonCount(count($terms), 'terms');
    }

    /**
     * Update book correctly.
     *
     * @return void
     */
    public function testBookIsUpdatedCorrectly()
    {
        $user = $this->createUser();
        $book = $this->createBook($user);

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date()
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/letterbooks/{$book->id}",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date']
            ]);
    }

    public function testBookIsUpdatedWithReferencesCorrectly()
    {
        $user = $this->createUser();
        $book = $this->createBook($user);

        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/letterbooks/{$book->id}?with[]=references",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date']
            ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    public function testBookIsUpdatedWithTermsCorrectly()
    {
        $user = $this->createUser();
        $book = $this->createBook($user);

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'terms' => json_encode($terms)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/letterbooks/{$book->id}?with[]=terms",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date']
            ])
            ->assertJsonCount(count($terms), 'terms');
    }

    public function testBookIsDeletedCorrectly()
    {
        $user = $this->createUser();
        $book = $this->createBook($user);

        $payload = [];

        // Perform a request
        $this->json(
            'DELETE',
            "http://localhost/api/v1/letterbooks/{$book->id}",
            $payload,
            $this->getUserHeaders($user)
        )
            ->assertStatus(204);
    }
}

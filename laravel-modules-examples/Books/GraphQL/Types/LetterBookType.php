<?php

namespace Modules\Books\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class LetterBookType extends GraphQLType
{
    protected $attributes = [
        'name' => 'LetterBook',
        'description' => 'A letter book'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the letter book'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of the letter book'
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'The body of the letter book'
            ],
            'date' => [
                'type' => Type::string(),
                'description' => 'The date of the letter book'
            ],
            'pdf' => [
                'type' => GraphQL::type('Media'),
                'description' => 'The path to the book pdf file'
            ],
            'pdf_text' => [
                'type' => GraphQL::type('Media'),
                'description' => 'The url to the book pdf file'
            ],
            'thumbnail' => [
                'type' => Type::string(),
                'description' => 'Relative path to the thumbnail'
            ],
            'route' => [
                'type' => Type::string(),
                'description' => 'Relative route to the content'
            ],
        ];
    }


    public function interfaces()
    {
        return [
            GraphQL::type('ContentInterface'),
        ];
    }

}

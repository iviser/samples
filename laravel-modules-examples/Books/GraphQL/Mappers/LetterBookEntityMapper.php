<?php

namespace Modules\Books\GraphQL\Mappers;


use App\GraphQL\Mappers\EntityMapper;
use Modules\Books\Entities\Book;

class LetterBookEntityMapper extends EntityMapper
{

    public function __construct(Book $model)
    {
        parent::__construct($model);
    }

}
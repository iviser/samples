<?php

namespace Modules\Books\GraphQL\Mappers;


use App\GraphQL\Mappers\EntityCollectionMapper;
use Modules\Books\Entities\Book;

class LetterBookCollectionEntityMapper extends EntityCollectionMapper
{

    /**
     * @var string
     */
    protected $table;

    public function toArray()
    {
        $this->mappedItems = collect();

        foreach ($this->items as $letterBook) {
            if ($letterBook instanceof Book) {
                $letterBook = new LetterBookEntityMapper($letterBook);
                $this->mappedItems->push($letterBook->toArray());
            }
        }

        return [
            'total' => $this->total,
            'offset' => $this->offset,
            'size' => $this->size,
            'items' => $this->mappedItems,
        ];
    }

}

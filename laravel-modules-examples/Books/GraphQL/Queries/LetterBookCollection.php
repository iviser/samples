<?php

namespace Modules\Books\GraphQL\Queries;

use Folklore\GraphQL\Support\Query;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Modules\Books\Entities\Book;
use Modules\Books\GraphQL\Mappers\LetterBookCollectionEntityMapper as Mapper;

class LetterBookCollection extends Query
{
    protected $attributes = [
        'name' => 'letterBooks'
    ];

    public function type()
    {
        return GraphQL::type('LetterBookCollection');
    }

    public function args()
    {
        return [
            // Filter
            'title' => ['name' => 'title', 'type' => Type::string()],

            // Pagination
            'take' => ['name' => 'take', 'type' => Type::int(), 'defaultValue' => 25],
            'skip' => ['name' => 'skip', 'type' => Type::int(), 'defaultValue' => 0],

            // Ordering
            'order_direction' => [
                'name' => 'order_direction',
                'type' => GraphQL::type('OrderDirection'),
                'defaultValue'=> 'asc'
            ],
        ];
    }

    public function resolve($root, $args)
    {
        // TODO it supposed to use repository to get article items
        $query = (new Book())->query();
        $query->take($args['take']);
        $query->skip($args['skip']);
        if (isset($args['title']) && ($title = $args['title'])) {
            $query->where('title', 'ilike', "%{$title}%");
        }
        $result = $query->get();
        // /TODO =================================================

        $mapper = new Mapper(
            $result, $query->toBase()->getCountForPagination(),
            $args['take'],
            $args['skip']
        );


        return $mapper->toArray();
    }
}

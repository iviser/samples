<?php

namespace Modules\Books\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Modules\Books\Entities\Book;

class LetterBookSuggest  extends Query
{
    protected $attributes = [
        'name' => 'letterBook'
    ];

    public function type()
    {
        return GraphQL::type('LetterBook');
    }

    public function args()
    {
        return [
            'search' => ['name' => 'search', 'type' => Type::nonNull(Type::string())],
            'field' => ['name' => 'field', 'type' => Type::string(), 'defaultValue' => 'title'],
            'limit' => ['name' => 'limit', 'type' => Type::int(), 'defaultValue' => 10],
        ];
    }

    public function resolve($root, $args)
    {
        return Book::where($args['field'], 'ilike', '%' . $args['search'] . '%')->limit($args['limit'])->get();
    }
}

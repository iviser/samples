<?php

namespace Modules\Books\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EgwPermissionsSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \App\Entities\Permission::firstOrCreate([
            'name'       => 'letter_book_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'letter_book_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'letter_book_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'letter_book_delete',
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('body')->nullable();
            $table->integer('user_id')->nullable();

            // Date when the book was written
            $table->date('date')->nullable();

            /*
             * Metadata
             *
             * {
             *      'migrations': {
             *          'source_id': 0, // integer
             *          'old_url': 'http://example.com/path/to/content' // string
             *          'type': 'wcw-letterbooks-full | wcw-letterbooks' // string
             *       },
             *
             *      'domain': 'string'
             *      'highlights': boolean
             *
             * }
             *
             */
            $table->jsonb('metadata')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_books');
    }
}

<?php

namespace Modules\Books\Providers;

use App\Services\Registries\EntityRegistry;
use Illuminate\Support\ServiceProvider;
use Modules\Books\Console\DeleteAllBooks;
use Modules\Books\Console\ProcessBookPdf;
use Modules\Books\Entities\Book;
use Modules\Books\Es\Documents\BookDoc;
use Modules\Books\Es\MappingTypes\BookMappingType;
use Modules\Books\GraphQL\Queries\LetterBook;
use Modules\Books\GraphQL\Queries\LetterBookCollection;
use Modules\Books\GraphQL\Queries\LetterBookSuggest;
use Modules\Books\GraphQL\Types\LetterBookCollectionType;
use Modules\Books\GraphQL\Types\LetterBookType;
use Modules\Books\Repositories\BookRepository;

class BooksServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerGraphQL();
        $this->app->make(EntityRegistry::class)->add(Book::class);

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerEs();
        $this->commands([
            DeleteAllBooks::class,
            ProcessBookPdf::class,
        ]);
        $this->registerRepo();
    }

    protected function registerRepo()
    {
        $this->app->bind(BookRepository::class, function ($app) {
            return new BookRepository($app);
        });
    }

    public function registerEs()
    {
        $laraElastic = $this->app->make(\Phirames\LaraElastic\LaraElastic::class);
        $laraElastic->indices()
            ->registry()
            ->getIndex(\App\Es\Indices\EgwIndex::name())
            ->addMapping(BookMappingType::name(), BookMappingType::properties());

        $laraElastic->docs()
            ->registry()->add(BookDoc::class);
    }

    public function registerGraphQL()
    {
        \GraphQL::addSchema('default', [
            'query' => [
                'letterBook' => LetterBook::class,
                'letterBooks' => LetterBookCollection::class,
                'letterBookSuggest' => LetterBookSuggest::class,
            ],
            'mutation' => []
        ]);
        \GraphQL::addType(LetterBookType::class);
        \GraphQL::addType(LetterBookCollectionType::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('books.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'books'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/books');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/books';
        }, \Config::get('view.paths')), [$sourcePath]), 'books');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/books');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'books');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'books');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}

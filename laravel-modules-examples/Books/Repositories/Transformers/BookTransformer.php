<?php

namespace Modules\Books\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use App\Repositories\Transformers\MediaFileTransformer;
use App\Repositories\Transformers\Traits\ReferenceTransformerTrait;
use App\Repositories\Transformers\Traits\TermTransformerTrait;
use Modules\Books\Entities\Book as Entity;

class BookTransformer extends EloquentTransformer
{

    use ReferenceTransformerTrait, TermTransformerTrait;

    protected $defaultIncludes = [
        'pdf',
        'pdf_text',
        'references',
        'loadedReferences',
        'terms'
    ];

    public function transform(Entity $entity)
    {
        return [
            'id' => (int) $entity->id,
            'title' => $entity->title,
            'body' => $entity->body,
            'date' => $entity->date,
            'route' => $entity->route(),
            'created_at' => $entity->created_at ? $entity->created_at->toDateTimeString() : null,
            'updated_at' => $entity->updated_at ? $entity->updated_at->toDateTimeString() : null,
            'deleted_at' => $entity->deleted_at ? $entity->deleted_at->toDateTimeString() : null,
        ];
    }

    public function includePdf(Entity $entity)
    {
        $pdf = $this->getRelationAsItem($entity, 'pdf');

        return $pdf ? $this->item($pdf, new MediaFileTransformer()) : null;
    }

    public function includePdfText(Entity $entity)
    {
        $pdf_text = $this->getRelationAsItem($entity, 'pdf_text');

        return $pdf_text ? $this->item($pdf_text, new MediaFileTransformer()) : null;
    }

}

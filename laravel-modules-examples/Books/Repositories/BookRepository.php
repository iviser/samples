<?php

namespace Modules\Books\Repositories;

use App\Repositories\BaseAppRepository;
use Modules\Books\Entities\Book;

class BookRepository extends BaseAppRepository
{
    public function model()
    {
        return Book::class;
    }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageMediaGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_image_galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('body')->nullable();

            $table->timestamps();
        });

        Schema::create('media_image_gallery_images', function (Blueprint $table) {
            $table->integer('gallery_id', false, true);
            $table->integer('image_id', false, true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_image_galleries');
        Schema::dropIfExists('media_image_gallery_images');
    }
}

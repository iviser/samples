<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_image', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('body')->nullable();
            $table->integer('user_id')->nullable();
            /*
             * Media data
             */
            $table->jsonb('media_data')->nullable();

            /*
             * Metadata
             *
             * {
             *      'migrations': {
             *          'source_id': 0, // integer
             *          'old_url': 'http://example.com/path/to/content' // string
             *       }
             * }
             *
             */
            $table->jsonb('metadata')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_image');
    }
}

<?php

namespace Modules\Media\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EgwPermissionsSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //Media Image Galleries
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_gallery_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_gallery_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_gallery_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_gallery_delete',
        ]);

        //Media Images
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_image_delete',
        ]);

        //Media Videos
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_video_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_video_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_video_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_video_delete',
        ]);

        //Media Audios
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_delete',
        ]);

        //Media Audio Albums
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_album_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_album_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_album_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_audio_album_delete',
        ]);

        //Media Documents
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_document_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_document_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_document_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'media_document_delete',
        ]);
    }
}

<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Media\Entities\AudioAlbum::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence()
    ];
});

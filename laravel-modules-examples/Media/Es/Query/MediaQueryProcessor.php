<?php

namespace Modules\Media\Es\Query;

use Modules\Search\Search\Query\QueryProcessor;
use Modules\Search\Search\Query\Query;
use Illuminate\Support\Collection;


class MediaQueryProcessor extends QueryProcessor
{

    public function __construct(Query $query, Collection $source)
    {
        parent::__construct($query, $source);

        $this->body = $this->query->getBody();

    }

    /**
     * Processes query taking parameters from source
     */
    public function process()
    {

        if (isset($this->getFilters()->media_types) && (array) $this->getFilters()->media_types) {

            $mediaFilters = array_keys((array) $this->getFilters()->media_types);
            $this->body->addTermsFilter('media_resources.type', $mediaFilters);

        }
    }
}
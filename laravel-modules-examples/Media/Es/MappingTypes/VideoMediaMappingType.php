<?php

namespace Modules\Media\Es\MappingTypes;

use Phirames\LaraElastic\MappingTypes\MappingTypeInterface;

class VideoMediaMappingType implements MappingTypeInterface
{
    public static function name(): string
    {
        return 'media_video';
    }

    public static function properties(): array
    {
        return [];
    }
}
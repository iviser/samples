<?php

namespace Modules\Media\Es\MappingTypes;

use Phirames\LaraElastic\MappingTypes\MappingTypeInterface;

class AudioMediaMappingType implements MappingTypeInterface
{
    public static function name(): string
    {
        return 'media_audio';
    }

    public static function properties(): array
    {
        return [];
    }
}
<?php

namespace Modules\Media\Es\MappingTypes;

use Phirames\LaraElastic\MappingTypes\MappingTypeInterface;

class DocumentMediaMappingType implements MappingTypeInterface
{
    public static function name(): string
    {
        return 'media_doc';
    }

    public static function properties(): array
    {
        return [];
    }
}
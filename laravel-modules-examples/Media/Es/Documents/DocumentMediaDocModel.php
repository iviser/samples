<?php

namespace Modules\Media\Es\Documents;


use App\Es\Documents\EgwEloquentDocModel;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use Modules\Media\Entities\DocMedia;
use Modules\Media\Es\MappingTypes\DocumentMediaMappingType;
use Modules\Search\Search\Mappings\PdfDocMediaResource;
use Modules\Search\Search\Mappings\Snippet;

class DocumentMediaDocModel extends EgwEloquentDocModel
{
    public function __construct(DocMedia $docMedia)
    {
        parent::__construct($docMedia);
    }

    public static function modelType(): string
    {
        return DocMedia::class;
    }

    public static function mappingType(): string
    {
        return DocumentMediaMappingType::name();
    }

    public function source(): array
    {
        return [
            'title' => $this->modelEntry->title,
            'suggest' => $this->modelEntry->title,

            'body' => $this->modelEntry->body,

            'snippet' => $this->snippet(),

            'resources' => $this->resources(),
            'media_resources' => $this->mediaResources(),

            'created_at' => $this->modelEntry->created_at ? $this->modelEntry->created_at->toDateTimeString() : null,
            'updated_at' => $this->modelEntry->updated_at ? $this->modelEntry->updated_at->toDateTimeString() : null,
            'deleted_at' => $this->modelEntry->deleted_at ? $this->modelEntry->deleted_at->toDateTimeString() : null,
        ];
    }

    /**
     * Create search snippet
     *
     * @return Snippet
     */
    protected function snippet(): Snippet
    {

        $title = $this->modelEntry->title;
        $body = $this->modelEntry->body ? str_limit(strip_tags($this->modelEntry->body), 255) : null;
        $url = route('document.item', ['id' => $this->modelEntry->id], false);
        $type = 'Document';
        $thumbUrl = $this->modelEntry instanceof EntityThumbInterface ? $this->modelEntry->thumb_site_path : null;

        $snippet = new Snippet($title, $body, $url, $thumbUrl, $type);

        return $snippet;
    }

    protected function resources(): array
    {
        if ($this->modelEntry->doc_path) {
            return [
                'pdf' => str_replace(base_path('storage'), '', $this->modelEntry->doc_path),
            ];
        }

        return [];
    }

    protected function mediaResources(): array
    {
        $mediaResources = [];

        if ($this->modelEntry->doc_path) {
            $path = str_replace(base_path('storage'), '',  $this->modelEntry->doc_path);
            $mediaResources[] = new PdfDocMediaResource($this->modelEntry->doc->name, $path);
        }

        return $mediaResources;
    }

}

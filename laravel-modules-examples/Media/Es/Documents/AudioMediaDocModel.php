<?php

namespace Modules\Media\Es\Documents;


use App\Es\Documents\EgwEloquentDocModel;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use Modules\Media\Entities\AudioMedia;
use Modules\Media\Es\MappingTypes\AudioMediaMappingType;
use Modules\Search\Search\Mappings\MP3AudioMediaResource;
use Modules\Search\Search\Mappings\Snippet;

class AudioMediaDocModel extends EgwEloquentDocModel
{
    public function __construct(AudioMedia $docMedia)
    {
        parent::__construct($docMedia);
    }

    public static function modelType(): string
    {
        return AudioMedia::class;
    }

    public static function mappingType(): string
    {
        return AudioMediaMappingType::name();
    }

    public function source(): array
    {
        return [
            'title' => $this->modelEntry->title,
            'suggest' => $this->modelEntry->title,

            'body' => $this->modelEntry->body,

            'snippet' => $this->snippet(),

            'resources' => $this->resources(),
            'media_resources' => $this->mediaResources(),

            'created_at' => $this->modelEntry->created_at ? $this->modelEntry->created_at->toDateTimeString() : null,
            'updated_at' => $this->modelEntry->updated_at ? $this->modelEntry->updated_at->toDateTimeString() : null,
            'deleted_at' => $this->modelEntry->deleted_at ? $this->modelEntry->deleted_at->toDateTimeString() : null,
        ];
    }

    /**
     * Create search snippet
     *
     * @return Snippet
     */
    protected function snippet(): Snippet
    {

        $title = $this->modelEntry->title;
        $body = $this->modelEntry->body ? str_limit(strip_tags($this->modelEntry->body), 255) : null;
        $url = route('audio.item', ['id' => $this->modelEntry->id], false);
        $type = 'Audio';
        $thumbUrl = $this->modelEntry instanceof EntityThumbInterface ? $this->modelEntry->thumb_site_path : null;

        $snippet = new Snippet($title, $body, $url, $thumbUrl, $type);

        return $snippet;
    }

    protected function resources(): array
    {
        if ($this->modelEntry->track_url) {
            return [
                'mp3' => str_replace(base_path('storage'), '', $this->modelEntry->track_url),
            ];
        }

        return [];
    }

    protected function mediaResources(): array
    {
        $mediaResources = [];

        if ($this->modelEntry->track_url) {
            $path = str_replace(base_path('storage'), '',  $this->modelEntry->track_url);
            $mediaResources[] = new MP3AudioMediaResource($this->modelEntry->media->name, $path);
        }

        return $mediaResources;
    }
}
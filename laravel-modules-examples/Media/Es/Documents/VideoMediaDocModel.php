<?php

namespace Modules\Media\Es\Documents;


use App\Es\Documents\EgwEloquentDocModel;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use Modules\Media\Entities\VideoMedia;
use Modules\Media\Es\MappingTypes\VideoMediaMappingType;
use Modules\Search\Search\Mappings\Snippet;

class VideoMediaDocModel extends EgwEloquentDocModel
{
    public function __construct(VideoMedia $docMedia)
    {
        parent::__construct($docMedia);
    }

    public static function modelType(): string
    {
        return VideoMedia::class;
    }

    public static function mappingType(): string
    {
        return VideoMediaMappingType::name();
    }

    public function source(): array
    {
        return [
            'title' => $this->modelEntry->title,
            'suggest' => $this->modelEntry->title,

            'body' => $this->modelEntry->body,

            'snippet' => $this->snippet(),

            'resources' => $this->resources(),

            'created_at' => $this->modelEntry->created_at ? $this->modelEntry->created_at->toDateTimeString() : null,
            'updated_at' => $this->modelEntry->updated_at ? $this->modelEntry->updated_at->toDateTimeString() : null,
            'deleted_at' => $this->modelEntry->deleted_at ? $this->modelEntry->deleted_at->toDateTimeString() : null,
        ];
    }

    /**
     * Create search snippet
     *
     * @return Snippet
     */
    protected function snippet(): Snippet
    {

        $title = $this->modelEntry->title;
        $body = $this->modelEntry->body ? str_limit(strip_tags($this->modelEntry->body), 255) : null;
        $url = route('video.item', ['id' => $this->modelEntry->id], false);
        $type = 'Video';
        $thumbUrl = $this->modelEntry instanceof EntityThumbInterface ? $this->modelEntry->thumb_site_path : null;

        $snippet = new Snippet($title, $body, $url, $thumbUrl, $type);

        return $snippet;
    }

    protected function resources(): array
    {
        if ($path = $this->modelEntry->video_path) {
            return [
               //  'mpeg4' => str_replace(base_path('storage'), '', $path),
            ];
        }

        return [];
    }
}
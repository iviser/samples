<?php

namespace Modules\Media\Es\Documents;


use App\Es\Documents\EgwEloquentDocModel;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Es\MappingTypes\ImageMediaMappingType;
use Modules\Search\Search\Mappings\ImageMediaResource;
use Modules\Search\Search\Mappings\Snippet;

class ImageMediaDocModel extends EgwEloquentDocModel
{
    public function __construct(ImageMedia $docMedia)
    {
        parent::__construct($docMedia);
    }

    public static function modelType(): string
    {
        return ImageMedia::class;
    }

    public static function mappingType(): string
    {
        return ImageMediaMappingType::name();
    }

    public function source(): array
    {
        return [
            'title' => $this->modelEntry->title,
            'suggest' => $this->modelEntry->title,

            'body' => $this->modelEntry->body,

            'snippet' => $this->snippet(),

            'resources' => $this->resources(),
            'media_resources' => $this->mediaResources(),

            'created_at' => $this->modelEntry->created_at ? $this->modelEntry->created_at->toDateTimeString() : null,
            'updated_at' => $this->modelEntry->updated_at ? $this->modelEntry->updated_at->toDateTimeString() : null,
            'deleted_at' => $this->modelEntry->deleted_at ? $this->modelEntry->deleted_at->toDateTimeString() : null,
        ];
    }

    /**
     * Create search snippet
     *
     * @return Snippet
     */
    protected function snippet(): Snippet
    {

        $title = $this->modelEntry->title;
        $body = $this->modelEntry->body ? str_limit(strip_tags($this->modelEntry->body), 255) : null;
        $url = route('image.item', ['id' => $this->modelEntry->id], false);
        $type = 'Image';
        $thumbUrl = $this->modelEntry instanceof EntityThumbInterface ? $this->modelEntry->thumb_site_path : null;

        $snippet = new Snippet($title, $body, $url, $thumbUrl, $type);

        return $snippet;
    }

    protected function resources(): array
    {
        if ($path = $this->modelEntry->image_path) {
            return [
                'img' => str_replace(base_path('storage'), '', $path),
            ];
        }

        return [];
    }

    protected function mediaResources(): array
    {
        $mediaResources = [];

        if ($media = $this->modelEntry->getMedia('media_image')->first()) {
            $path = str_replace(base_path('storage'), '',  $media->getPath());
            $mediaResources[] = new ImageMediaResource($media->name, 'jpeg', $path);
        }

        return $mediaResources;
    }

}

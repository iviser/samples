<?php

namespace Modules\Media\Http\Controllers;

use App\Repositories\Criteria\RelationsRequestCriteria;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Media\Entities\AudioMedia;
use Modules\Media\Repositories\AudioMediaRepository;
use Modules\Media\Repositories\Presenters\AudioMediaPresenter;

class MediaAudioController extends Controller
{
    /**
     * @var AudioMediaRepository
     */
    protected $repo;

    /**
     * MediaAudioController constructor.
     * @param AudioMediaRepository $repository
     */
    public function __construct(AudioMediaRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('media::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @param int $id Entity id
     * @return Response
     */
    public function show(int $id)
    {
        $entity = AudioMedia::findOrFail($id);
        return view('media::audio.show', [
            'audio' => $entity,
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function dataList(Request $request)
    {
        $relations = $request->get('with', []);
        $limit = $request->get('limit', 25);

        if ($searchString = $request->get('search_string')) {

            $entities = $this->repo
                ->load($relations)
                ->setPresenter($this->getPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                }, $limit);

        } else {

            $entities = $this->repo
                ->pushCriteria(new RelationsRequestCriteria())
                ->setPresenter($this->getPresenter())
                ->paginate($limit);
        }

        return \Response::json($entities, 206);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function dataItem(Request $request, int $id)
    {
        $location = $this->repo
            ->with($relations = $request->get('with', []))
            ->setPresenter($this->getPresenter())
            ->find($id);

        return \Response::json($location, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    protected function getPresenter()
    {
        return new AudioMediaPresenter();
    }
}

<?php

namespace Modules\Media\Http\Controllers\Api;

use App\Repositories\Criteria\SearchRequestByTitle;
use App\Repositories\Criteria\SortById;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Media\Entities\VideoMedia as Entity;
use Modules\Media\Repositories\VideoMediaRepository;
use Modules\Media\Repositories\Presenters\VideoMediaPresenter;

class RestVideoController extends Controller
{
    /**
     * @var VideoMediaRepository
     */
    private $repo;

    public function __construct(VideoMediaRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        //Takes 'mediaFile', 'references', 'terms'
        $relations = $request->get('with', []);

        $entity = $this->repo
            ->pushCriteria(new SearchRequestByTitle($request))
            ->pushCriteria(new SortById())
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->orderBy('id', 'asc')
            ->paginate(25);

        return response()->json($entity, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'       => 'required|max:255',
            'body'        => 'string',
            'mediaFile'   => 'required|file|max:2048000|mimes:mp4,mov,ogg,qt,avi,ogx,oga,ogv,webm',
            'references' => 'json',
            'terms'       => 'json'
        ]);

        $entity = Entity::create([
            'title'      => $request->get('title'),
            'body'       => $request->get('body'),
            'references' => $request->get('references'),
        ]);

        $entity->addMediaFromRequest('mediaFile')
              ->toMediaCollection($entity->getMediaCollectionName());

        $entity->save();

        $entity->syncRefsRequest($request);
        $entity->syncTermsRequest($request);

        //Takes 'mediaFile', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $entity->load($relations);
        }

        return response()->json($this->getPresenter()->present($entity), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        //Takes 'mediaFile', 'references', 'terms'
        $relations = $request->get('with', []);

        $entity = $this->repo
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->find($id);

        return response()->json($entity, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title'       => 'required|max:255',
            'body'        => 'string',
            'references'  => 'json',
            'terms'       => 'json'
        ]);

        $entity = $this->repo->find($id);

        $this->authorize('update', $entity);

        $entity->update([
            'title' => $request->get('title'),
            'body'  => $request->get('body'),
            'references' => $request->get('references'),
        ]);

        $entity->syncRefsRequest($request);
        $entity->syncTermsRequest($request);

        //Takes 'mediaFile', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $entity->load($relations);
        }

        return response()->json($this->getPresenter()->present($entity), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $entity = $this->repo->find($id);

        $this->authorize('delete', $entity);

        $entity->delete();

        return response()->json($entity, 204);
    }

    /**
     * Updates media file nested in the content media type entity
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateMediaFile(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'mediaFile'   => 'required|file|max:2048000|mimes:mp4,mov,ogg,qt,avi,ogx,oga,ogv,webm',
        ]);

        $media = $this->repo->with('mediaFile')->find($id);

        $this->authorize('update', $media);

        // Delete existing media file

        if ($media->mediaFile) {
            $media->mediaFile->delete();
        }

        //Add new media file
        $media->addMediaFromRequest('mediaFile')
              ->toMediaCollection($media->getMediaCollectionName());

        $media->load('mediaFile');

        return response()->json($this->getPresenter()->present($media), 201);
    }

    /**
     * @return VideoMediaPresenter
     * @throws \Exception
     */
    public function getPresenter()
    {
        return new VideoMediaPresenter();
    }
}

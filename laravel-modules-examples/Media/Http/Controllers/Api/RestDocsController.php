<?php

namespace Modules\Media\Http\Controllers\Api;

use App\Repositories\Criteria\SortById;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Media\Entities\DocMedia;
use Modules\Media\Repositories\DocMediaRepository;
use Modules\Media\Repositories\Presenters\DocMediaPresenter;

class RestDocsController extends Controller
{

    /**
     * @var DocMediaRepository
     */
    private $repo;

    public function __construct(DocMediaRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        //Takes 'mediaFile', 'references', 'terms'
        $relations = $request->get('with', []);

        $media = $this->repo
            ->pushCriteria(new SortById())
            ->with($relations)
            ->setPresenter($this->createPresenter())
            ->paginate();

        return response()->json($media, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'   => 'required|max:255',
            'body'    => 'string',
            'text'    => 'string',
            'summary' => 'string|max:1024',
            'media'   => 'required|file|max:20480',
            'references' => 'json',
            'terms'     => 'json'
        ]);
        $media = DocMedia::create($request->all());

        $media->addMediaFromRequest('media')
              ->toMediaCollection($media->getMediaCollectionName());

        $media->syncRefsRequest($request);
        $media->syncTermsRequest($request);

        //Takes 'mediaFile', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $media->load($relations);
        }

        return response()->json($this->createPresenter()->present($media), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        //Takes 'mediaFile', 'references', 'terms'
        $relations = $request->get('with', []);

        $media = $this->repo
            ->with($relations)
            ->setPresenter($this->createPresenter())
            ->find($id);

        return response()->json($media, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title'   => 'required|max:255',
            'body'    => 'string',
            'text'    => 'string',
            'summary' => 'string|max:1024',
            'references' => 'json',
            'terms'     => 'json'
        ]);

        $media = $this->repo->find($id);

        $this->authorize('update', $media);

        $media->update($request->all());

        $media->syncRefsRequest($request);
        $media->syncTermsRequest($request);

        //Takes 'mediaFile', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $media->load($relations);
        }

        return response()->json($this->createPresenter()->present($media), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $media = $this->repo->find($id);

        $this->authorize('delete', $media);

        $media->delete();

        return response()->json($media, 204);
    }

    /**
     * Updates media file nested in the content media type entity
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateMediaFile(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'media' => 'required|file|max:20480',
        ]);

        $media = $this->repo->find($id);

        $this->authorize('update', $media);

        // Delete existing media file
        if ($media->mediaFile) {
            $media->mediaFile->delete();
        }

        //Add new media file
        $media->addMediaFromRequest('media')
              ->toMediaCollection($media->getMediaCollectionName());

        $media->load('mediaFile');

        return response()->json($this->createPresenter()->present($media), 201);
    }

    /**
     * @return DocMediaPresenter
     * @throws \Exception
     */
    public function createPresenter()
    {
        return new DocMediaPresenter();
    }
}

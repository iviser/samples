<?php

namespace Modules\Media\Http\Controllers\Api;

use App\Repositories\Criteria\SearchRequestByTitle;
use App\Repositories\Criteria\SortById;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Entities\ImageMediaGallery as Entity;
use Modules\Media\Entities\ImageMediaGallery;
use Modules\Media\Repositories\Criteria\ImageGallery\GalleryImages;
use Modules\Media\Repositories\ImageMediaGalleryRepository as Repository;
use Modules\Media\Repositories\ImageMediaRepository;
use Modules\Media\Repositories\Presenters\ImageMediaGalleryPresenter as Presenter;
use Modules\Media\Repositories\Presenters\ImageMediaPresenter;

/**
 * REST API controller for image media galleries
 *
 * Class RestImageMediaController
 * @package Modules\Media\Http\Controllers\Api
 */
class RestImageGalleryController extends Controller
{
    /**
     * @var Repository
     */
    private $repo;

    /**
     * @var ImageMediaRepository
     */
    private $imageRepo;

    public function __construct(Repository $repository, ImageMediaRepository $imageRepository)
    {
        $this->repo = $repository;
        $this->imageRepo = $imageRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        //Takes 'cover', 'images', 'references'
        $relations = $request->get('with', []);

        $media = $this->repo
            ->pushCriteria(new SearchRequestByTitle($request))
            ->pushCriteria(new SortById())
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->orderBy('id', 'asc')
            ->paginate(25);

        return response()->json($media, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body'  => 'string',
            'cover' => 'required|file|max:2048|image:jpeg,png,bmp,gif,svg',
            'references' => 'json'

        ]);
        $gallery = Entity::create($request->all());

        //Add new media file
        $gallery->addMediaFromRequest('cover')
                ->toMediaCollection(ImageMediaGallery::COVER_MEDIA_COLLECTION_NAME);

        $gallery->syncRefsRequest($request);

        //Takes 'cover', 'images', 'references'
        if ($relations = $request->get('with', [])) {
            $gallery->load($relations);
        }

        return response()->json($this->getPresenter()->present($gallery), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        //Takes 'cover', 'images', 'references'
        $relations = $request->get('with', []);

        $imgLimit = $request->get('imgLimit', 10);

        $gallery = $this->repo
            ->with(array_merge($relations, ['images' => function($q) use($imgLimit) {
                return $q->take($imgLimit);
            }]))
            ->setPresenter($this->getPresenter())
            ->find($id);

        return response()->json($gallery, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body'  => 'string',
            'references' => 'json'
        ]);

        $gallery = $this->repo->find($id);

        $this->authorize('update', $gallery);

        $gallery->update($request->all());

        $gallery->syncRefsRequest($request);

        //Takes 'cover', 'images', 'references'
        if ($relations = $request->get('with', [])) {
            $gallery->load($relations);
        }

        return response()->json($this->getPresenter()->present($gallery), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $gallery = $this->repo->find($id);

        $this->authorize('delete', $gallery);

        $gallery->delete();

        return response()->json($gallery, 204);
    }

    /**
     * Updates media file nested in the content media type entity
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateCover(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'cover' => 'required|file|max:2048|image:jpeg,png,bmp,gif,svg',
        ]);

        $gallery = $this->repo
            ->find($id);

        $this->authorize('update', $gallery);

        // Delete existing media file
        $gallery->cover->delete();

        //Add new media file
        $gallery->addMediaFromRequest('cover')
              ->toMediaCollection(ImageMediaGallery::COVER_MEDIA_COLLECTION_NAME);

        $gallery->load('cover');

        return response()->json($this->getPresenter()->present($gallery), 201);
    }

    /**
     * @param Request $request
     *
     * @param int $galleryId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function showImages(Request $request, int $galleryId)
    {
        $media = $this->imageRepo
            ->pushCriteria(new GalleryImages($galleryId, $request))
            ->setPresenter($this->getImagePresenter())
            ->paginate();

        return response()->json($media, 206);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function bulkImageStore(Request $request, int $id)
    {
        $request->validate([
            'images'       => 'array',
            'images.*'     => 'file|max:20480|image:jpeg,png,bmp,gif,svg',
        ]);

        $gallery = $this->repo->find($id);

        $this->authorize('update', $gallery);

        $imageMediaCollection = collect();

        $images = $request->images ?? [];

        foreach ($images as $image) {

            if ($image instanceof UploadedFile) {
                $imageMedia = ImageMedia::create([
                    'title' => basename($image->getClientOriginalName(), "." . $image->getClientOriginalExtension()),
                ]);

                $imageMedia->addMedia($image)
                    ->toMediaCollection($imageMedia->getMediaCollectionName());

                $imageMediaCollection->push($imageMedia);
            }
        }

        if ($imageMediaCollection->isNotEmpty()) {
            $imageMediaIds = $imageMediaCollection->pluck('id')->toArray();

            $gallery->images()->attach($imageMediaIds);
            $transformed = $this->imageRepo
                ->with('mediaFile')
                ->setPresenter($this->getImagePresenter())
                ->findWhereIn('id', $imageMediaIds);

            return response()->json($transformed, 201);
        }

        return response()->json([], 201);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function attachImages(Request $request, int $id)
    {
        $request->validate([
            'ids'       => 'array',
            'ids.*'     => 'integer',
        ]);

        $gallery = $this->repo->find($id);

        $this->authorize('update', $gallery);

        $gallery->images()->attach($request->get('ids', []));

        return response()->json(['message' => 'Image media were successfully attached'], 200);

    }

    /**
     * @param Request $request
     * @param int $galleryId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function detachImages(Request $request, int $galleryId)
    {
        $request->validate([
            'ids'       => 'array',
            'ids.*'     => 'integer',
        ]);

        $gallery = $this->repo->find($galleryId);

        $this->authorize('update', $gallery);

        $gallery->images()->detach($request->get('ids', []));

        return response()->json(['message' => 'Image media were successfully detached' ], 200);
    }

    /**
     * @return Presenter
     * @throws \Exception
     */
    public function getPresenter(): Presenter
    {
        return new Presenter();
    }

    /**
     * @return ImageMediaPresenter
     * @throws \Exception
     */
    public function getImagePresenter(): ImageMediaPresenter
    {
        return new ImageMediaPresenter();
    }
}

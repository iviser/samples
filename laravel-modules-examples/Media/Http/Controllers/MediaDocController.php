<?php

namespace Modules\Media\Http\Controllers;

use App\Repositories\Criteria\RelationsRequestCriteria;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Flowpaper\Widgets\PdfViewer;
use Modules\Media\Entities\DocMedia;
use Modules\Media\Repositories\DocMediaRepository;
use Modules\Media\Repositories\Presenters\DocMediaPresenter;

class MediaDocController extends Controller
{
    /**
     * @var DocMediaRepository
     */
    protected $repo;

    /**
     * MediaDocController constructor.
     * @param DocMediaRepository $repository
     */
    public function __construct(DocMediaRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('media::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(int $id)
    {
        $doc = DocMedia::findOrFail($id);

        // @TODO: choose viewer depending of document type

        $text = null;

        if (isset($doc->metadata)
            && isset($doc->metadata->migration)
            && isset($doc->metadata->migration->document)
            && isset($doc->metadata->migration->document->text)
        ) {
            $text = $doc->metadata->migration->document->text;
        }

        return view('media::doc.show', [
            "doc" => $doc,
            "text" => $text,
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function dataList(Request $request)
    {
        $relations = $request->get('with', []);
        $limit = $request->get('limit', 25);

        if ($searchString = $request->get('search_string')) {

            $entities = $this->repo
                ->load($relations)
                ->setPresenter($this->getPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                }, $limit);

        } else {

            $entities = $this->repo
                ->pushCriteria(new RelationsRequestCriteria())
                ->setPresenter($this->getPresenter())
                ->paginate($limit);
        }

        return \Response::json($entities, 206);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function dataItem(Request $request, int $id)
    {
        $location = $this->repo
            ->with($relations = $request->get('with', []))
            ->setPresenter($this->getPresenter())
            ->find($id);

        return \Response::json($location, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    protected function getPresenter()
    {
        return new DocMediaPresenter();
    }
}

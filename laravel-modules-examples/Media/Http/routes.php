<?php

Route::group(['middleware' => 'web', 'prefix' => 'media', 'namespace' => 'Modules\Media\Http\Controllers'], function()
{
    Route::get('/', 'MediaController@index');

    Route::get('/document/data', 'MediaDocController@dataList')->name('document.data.list');
    Route::get('/document/{id}/data', 'MediaDocController@dataItem')->name('document.data.item');
    Route::get('document/{id}', 'MediaDocController@show')->name('document.item');

    Route::get('audio/data', 'MediaAudioController@dataList')->name('audio.data.list');
    Route::get('audio/{id}/data', 'MediaAudioController@dataItem')->name('audio.data.item');
    Route::get('audio/{id}', 'MediaAudioController@show')->name('audio.item');

    Route::get('audio-album/data', 'MediaAudioController@dataList')->name('audio-album.data.list');
    Route::get('audio-album/{id}/data', 'MediaAudioController@dataItem')->name('audio-album.data.item');
    Route::get('audio-album/{id}', 'MediaAudioController@show')->name('audio-album.item');

    Route::get('/video/data', 'MediaVideoController@dataList')->name('video.data.list');
    Route::get('/video/{id}/data', 'MediaVideoController@dataItem')->name('video.data.item');
    Route::get('video/{id}', 'MediaVideoController@show')->name('video.item');

    Route::get('/image/data', 'MediaImageController@dataList')->name('image.data.list');
    Route::get('image/{id}/data', 'MediaImageController@dataItem')->name('image.item.data');
    Route::get('image/{id}', 'MediaImageController@show')->name('image.item');
    Route::get('images', 'MediaImageController@index')->name('images');

    Route::get('/image-gallery/data', 'MediaImageGalleryController@dataList')->name('image-gallery.data.list');
    Route::get('image-gallery/{id}/data', 'MediaImageGalleryController@dataItem')->name('image-gallery.item.data');
    Route::get('image-gallery/{id}', 'MediaImageGalleryController@show')->name('image-gallery.item');


});

// Images
Route::group(
    [
        'middleware' => 'auth:api',
        'prefix' => '/api/v1/media/images',
        'namespace' => 'Modules\Media\Http\Controllers\Api',
    ],
    function() {
        Route::get('/', 'RestImagesController@index')->name('api.media.images.index');
        Route::get('/{id}', 'RestImagesController@show')->name('api.media.images.show');
        Route::post('/', 'RestImagesController@store')->name('api.media.images.create');
        Route::post('/{id}', 'RestImagesController@update')->name('api.media.images.update');
        Route::delete('/{id}', 'RestImagesController@destroy')->name('api.media.images.delete');

        Route::post('/{imgId}/file', 'RestImagesController@updateMediaFile')
             ->name('api.media.images.file.update');
    }
);

// Image Gallery controller
Route::group(
    [
        'middleware' => 'auth:api',
        'prefix' => '/api/v1/media/image-galleries',
        'namespace' => 'Modules\Media\Http\Controllers\Api',
    ],
    function() {
        // Gallery CRUD
        Route::get('/', 'RestImageGalleryController@index')->name('api.media.img_gal.index');
        Route::get('/{id}', 'RestImageGalleryController@show')->name('api.media.img_gal.show');
        Route::post('/', 'RestImageGalleryController@store')->name('api.media.img_gal.create');
        Route::post('/{id}', 'RestImageGalleryController@update')->name('api.media.img_gal.update');
        Route::delete('/{id}', 'RestImageGalleryController@destroy')->name('api.media.img_gal.delete');

        // Update gallery cover
        Route::post('/{id}/cover', 'RestImageGalleryController@updateCover')
             ->name('api.media.img_gal.cover.update');

        // Images from the gallery
        Route::get('/{id}/images', 'RestImageGalleryController@showImages')
             ->name('api.media.img_gal.index.images');
        Route::post('/{id}/images', 'RestImageGalleryController@bulkImageStore')
             ->name('api.media.img_gal.store.images');
        // Attach and detach
        Route::post('/{id}/attach/images', 'RestImageGalleryController@attachImages')
             ->name('api.media.img_gal.attach.images');
        Route::post('/{galleryId}/detach/images/', 'RestImageGalleryController@detachImages')
             ->name('api.media.img_gal.detach.images');


    }
);


// Documents
Route::group(
    [
        'middleware' => 'auth:api',
        'prefix' => '/api/v1/media/documents',
        'namespace' => 'Modules\Media\Http\Controllers\Api',
    ],
    function() {
        Route::get('/', 'RestDocsController@index')->name('api.media.docs.index');
        Route::get('/{id}', 'RestDocsController@show')->name('api.media.docs.show');
        Route::post('/', 'RestDocsController@store')->name('api.media.docs.create');
        Route::post('/{id}', 'RestDocsController@update')->name('api.media.docs.update');
        Route::delete('/{id}', 'RestDocsController@destroy')->name('api.media.docs.delete');

        Route::post('/{id}/file', 'RestDocsController@updateMediaFile')
             ->name('api.media.docs.files.update');
    }
);

// Audio album controller
Route::group(
    [
        'middleware' => 'auth:api',
        'prefix' => '/api/v1/media/audio-albums',
        'namespace' => 'Modules\Media\Http\Controllers\Api',
    ],
    function() {
        // Audio Album CRUD
        Route::get('/', 'RestAudioAlbumController@index')->name('api.media.audio_album.index');
        Route::get('/{id}', 'RestAudioAlbumController@show')->name('api.media.audio_album.show');
        Route::post('/', 'RestAudioAlbumController@store')->name('api.media.audio_album.create');
        Route::post('/{id}', 'RestAudioAlbumController@update')->name('api.media.audio_album.update');
        Route::delete('/{id}', 'RestAudioAlbumController@destroy')->name('api.media.audio_album.delete');

        // Update Audio Album cover
        Route::post('/{id}/cover', 'RestAudioAlbumController@updateCover')
             ->name('api.media.audio_album.cover.update');

        // Tracks from the Audio Album
        Route::get('/{id}/tracks', 'RestAudioAlbumController@showTracks')
             ->name('api.media.audio_album.show.tracks');

    }
);

// REST Audio endpoints
Route::group(
    [
        'middleware' => 'auth:api',
        'prefix' => '/api/v1/media/audio',
        'namespace' => 'Modules\Media\Http\Controllers\Api',
    ],
    function() {
        Route::get('/', 'RestAudioController@index')->name('api.media.audio.index');
        Route::get('/{id}', 'RestAudioController@show')->name('api.media.audio.show');
        Route::post('/', 'RestAudioController@store')->name('api.media.audio.create');
        Route::post('/{id}', 'RestAudioController@update')->name('api.media.audio.update');
        Route::delete('/{id}', 'RestAudioController@destroy')->name('api.media.audio.delete');

        Route::post('/{id}/file', 'RestAudioController@updateMediaFile')
             ->name('api.media.audio.files.update');
    }
);

// REST Video endpoints
Route::group(
    [
        'middleware' => 'auth:api',
        'prefix' => '/api/v1/media/video',
        'namespace' => 'Modules\Media\Http\Controllers\Api',
    ],
    function() {
        Route::get('/', 'RestVideoController@index')->name('api.media.video.index');
        Route::get('/{id}', 'RestVideoController@show')->name('api.media.video.show');
        Route::post('/', 'RestVideoController@store')->name('api.media.video.create');
        Route::post('/{id}', 'RestVideoController@update')->name('api.media.video.update');
        Route::delete('/{id}', 'RestVideoController@destroy')->name('api.media.video.delete');

        Route::post('/{id}/file', 'RestVideoController@updateMediaFile')
            ->name('api.media.video.files.update');
    }
);
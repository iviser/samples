<?php

namespace Modules\Media\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Media\Entities\AudioAlbum;

class AudioAlbumMediaRepository extends BaseAppRepository
{
    public function model()
    {
        return AudioAlbum::class;
    }

}
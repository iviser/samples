<?php

namespace Modules\Media\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Media\Entities\DocMedia;

class DocMediaRepository extends BaseAppRepository
{
    public function model()
    {
        return DocMedia::class;
    }

}
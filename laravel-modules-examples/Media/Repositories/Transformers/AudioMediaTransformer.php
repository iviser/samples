<?php

namespace Modules\Media\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use App\Entities\Interfaces\ContentInterface;
use App\Repositories\Transformers\Traits\ReferenceTransformerTrait;
use App\Repositories\Transformers\Traits\TermTransformerTrait;
use Illuminate\Database\Eloquent\Model as Entity;

class AudioMediaTransformer extends EloquentTransformer
{
    use MediaFileIncludeTrait, ReferenceTransformerTrait, TermTransformerTrait;

    protected $defaultIncludes = [
        'mediaFile',
        'album',
        'cover',
        'references',
        'loadedReferences',
        'terms'
    ];

    public function transform(Entity $entity)
    {

        $route = ($entity instanceof ContentInterface) ? $entity->route([], false) : null;

        return [
            'id'                => (int) $entity->id,
            'title'             => $entity->title,
            'body'              => $entity->body,
            'thumbnail'         => $entity->thumb_site_path,
            'route'             => $route,
        ];
    }

    /**
     * @param Entity $entity
     *
     * @return \League\Fractal\Resource\Item|null
     */
    public function includeCover(Entity $entity)
    {
        $cover = $this->getRelationAsItem($entity, 'cover');

        return $cover ? $this->item($cover, new MediaFileTransformer()) : null;
    }

    /**
     * Include album collection
     *
     * @param Entity $entity
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeAlbum(Entity $entity)
    {
        $album = $this->getRelationAsItem($entity, 'album');

        return $album ? $this->item($album, new AudioAlbumMediaTransformer()) : null;
    }

}
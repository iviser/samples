<?php

namespace Modules\Media\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use App\Repositories\Transformers\Traits\ReferenceTransformerTrait;
use Illuminate\Support\Collection;
use Modules\Media\Entities\ImageMediaGallery as Entity;
use Modules\Media\Repositories\Transformers\MediaImageTransformer;
use App\Repositories\Transformers\MediaFileTransformer;

class ImageMediaGalleryTransformer extends EloquentTransformer
{
    use ReferenceTransformerTrait;

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'images',
        'cover',
        'references',
        'loadedReferences'
    ];

    public function transform(Entity $entity)
    {
        return [
            'id'     => (int) $entity->id,
            'title'  => $entity->title,
            'body'   => $entity->body,
        ];
    }

    /**
     * Include images collection
     */
    public function includeImages(Entity $entity)
    {
        $images = $entity->relationLoaded('images')
            ? $entity->getRelation('images')
            : new Collection();

        return $this->collection($images, new MediaImageTransformer());
    }

    public function includeCover(Entity $entity)
    {
        $cover = $this->getRelationAsItem($entity, 'cover');

        return $cover ? $this->item($cover, new MediaFileTransformer()) : null;
    }
}
<?php

namespace Modules\Media\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use App\Entities\Interfaces\ContentInterface;
use App\Repositories\Transformers\Traits\ReferenceTransformerTrait;
use App\Repositories\Transformers\Traits\TermTransformerTrait;
use Illuminate\Database\Eloquent\Model as Entity;

class DocMediaTransformer extends EloquentTransformer
{
    use MediaFileIncludeTrait, ReferenceTransformerTrait, TermTransformerTrait;

    protected $defaultIncludes = [
        'mediaFile',
        'references',
        'loadedReferences',
        'terms'
    ];

    public function transform(Entity $entity)
    {

        $route = ($entity instanceof ContentInterface) ? $entity->route([], false) : null;

        return [
            'id'                => (int) $entity->id,
            'title'             => $entity->title,
            'body'              => $entity->body,
            'thumbnail'         => $entity->thumb_site_path,
            'route'             => $route,
            'date'              => $entity->date,
            'text'              => $entity->text,
            'summary'           => $entity->summary,
        ];
    }

}
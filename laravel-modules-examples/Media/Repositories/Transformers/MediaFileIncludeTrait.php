<?php

namespace Modules\Media\Repositories\Transformers;

use Illuminate\Database\Eloquent\Model as Entity;
use Illuminate\Support\Collection;
use Modules\Media\Entities\Interfaces\MediaEntityInterface;

trait MediaFileIncludeTrait
{

    public function includeMediaFile(Entity $entity)
    {
        $mediaFile = $this->getRelationAsItem($entity, 'mediaFile');

        return $mediaFile ? $this->item($mediaFile, new MediaFileTransformer()) : null;
    }
}

<?php

namespace Modules\Media\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;
use \App\Entities\Interfaces\ContentInterface;
use Modules\Media\Entities\Interfaces\MediaEntityInterface;
use Illuminate\Database\Eloquent\Model as Entity;
use Modules\Media\Repositories\Transformers\MediaFileIncludeTrait;
use Spatie\MediaLibrary\Media as MediaFile;

class MediaTypeTransformer extends EloquentTransformer
{

    use MediaFileIncludeTrait;

    protected $defaultIncludes = [
        'mediaFile',
    ];

    public function transform(Entity $entity)
    {

        $route = ($entity instanceof ContentInterface) ? $entity->route([], false) : null;

        return [
            'id'                => (int) $entity->id,
            'title'             => $entity->title,
            'body'              => $entity->body,
            'thumbnail'         => $entity->thumb_site_path,
            'route'             => $route,
            'date'              => $entity->date,
            'text'              => $entity->text,
        ];
    }

}
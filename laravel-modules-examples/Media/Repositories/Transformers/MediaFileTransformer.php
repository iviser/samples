<?php

namespace Modules\Media\Repositories\Transformers;

use League\Fractal\TransformerAbstract;
use Spatie\MediaLibrary\Media as MediaFile;

class MediaFileTransformer extends TransformerAbstract
{
    public function transform(MediaFile $entity)
    {
        // Get public path
        $publicPath = null;
        if($path = $entity->getPath()) {
            $publicPath = str_replace(storage_path(), '', $path);
        }
        return [
            'id' => (int) $entity->id,
            'name' => $entity->name,
            'file_name' => $entity->file_name,
            'mime_type' => $entity->mime_type,
            'disk' => $entity->disk,
            'size' => $entity->size,
            'public_path' => $publicPath,
            'server_path' => $path,
            'public_url' => $entity->getUrl(),
        ];
    }

}

<?php

namespace Modules\Media\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use App\Entities\Interfaces\ContentInterface;
use App\Repositories\Transformers\Traits\ReferenceTransformerTrait;
use Modules\Media\Entities\AudioAlbum as Entity;

class AudioAlbumMediaTransformer extends EloquentTransformer
{
    use MediaFileIncludeTrait, ReferenceTransformerTrait;

    protected $defaultIncludes = [
        'tracks',
        'cover',
        'references',
        'loadedReferences'
    ];

    public function transform(Entity $entity)
    {

        $route = ($entity instanceof ContentInterface) ? $entity->route([], false) : null;

        return [
            'id'                => (int) $entity->id,
            'title'             => $entity->title,
            'thumbnail'         => $entity->thumb_site_path,
            'route'             => $route,
        ];
    }

    /**
     * Include images collection
     *
     * @param Entity $entity
     *
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTracks(Entity $entity)
    {
        $images = $this->getRelationAsCollection($entity, 'tracks');

        return $this->collection($images, new AudioMediaTransformer());
    }

    /**
     * @param Entity $entity
     *
     * @return \League\Fractal\Resource\Item|null
     */
    public function includeCover(Entity $entity)
    {
        $cover = $this->getRelationAsItem($entity, 'cover');

        return $cover ? $this->item($cover, new MediaFileTransformer()) : null;
    }

}
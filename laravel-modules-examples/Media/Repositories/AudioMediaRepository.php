<?php

namespace Modules\Media\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Media\Entities\AudioMedia;

class AudioMediaRepository extends BaseAppRepository
{
    public function model()
    {
        return AudioMedia::class;
    }

}
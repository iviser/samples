<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\DocMediaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class DocMediaPresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DocMediaTransformer();
    }
}

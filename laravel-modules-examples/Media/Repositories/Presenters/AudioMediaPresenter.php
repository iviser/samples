<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\AudioMediaTransformer;
use Modules\Media\Repositories\Transformers\MediaFileIncludeTrait;
use Prettus\Repository\Presenter\FractalPresenter;

class AudioMediaPresenter extends FractalPresenter
{

    use MediaFileIncludeTrait;

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AudioMediaTransformer();
    }
}

<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\VideoMediaTransformer;
use Modules\Media\Repositories\Transformers\MediaFileIncludeTrait;
use Prettus\Repository\Presenter\FractalPresenter;

class VideoMediaPresenter extends FractalPresenter
{

    use MediaFileIncludeTrait;

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new VideoMediaTransformer();
    }
}

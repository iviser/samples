<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\AudioAlbumMediaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class AudioAlbumMediaPresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AudioAlbumMediaTransformer();
    }
}

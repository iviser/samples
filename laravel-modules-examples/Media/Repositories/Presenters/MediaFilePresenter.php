<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\MediaTypeTransformer;
use Modules\Media\Repositories\Transformers\MediaFileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class MediaFilePresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MediaFileTransformer();
    }
}

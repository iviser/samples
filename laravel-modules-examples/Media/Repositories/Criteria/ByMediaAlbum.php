<?php

namespace Modules\Media\Repositories\Criteria;

use Modules\Media\Entities\Interfaces\MediaEntityInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class ByMediaAlbum implements CriteriaInterface {

    protected $albumId;

    public function __construct(string $albumId = null)
    {
        $this->albumId = $albumId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('album_id', '=', $this->albumId);
    }
}

<?php

namespace Modules\Media\Repositories\Criteria\ImageGallery;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Http\Request;

class GalleryImages implements CriteriaInterface
{

    protected $galleryId;
    protected $request;

    public function __construct(int $galleryId, $request = null)
    {
        $this->galleryId = $galleryId;
        $this->request = $request ?? app(Request::class);
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $userId = $this->galleryId;

        if ($title = $this->request->get('search_string')) {
            $model = $model->where('title', 'ILIKE', "%{$title}%");
        }

        $model = $model->take($this->request->get('limit', 25))
              ->with($this->request->get('with', []))
              ->whereHas('galleries', function ($query) use($userId) {
                  $query->where('id', '=', $userId);
              });

        return $model;
    }

}

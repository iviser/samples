<?php

namespace Modules\Media\Repositories\Criteria\ImageGallery;

use Modules\Media\Entities\Interfaces\MediaEntityInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class WithImages implements CriteriaInterface
{

    protected $imagesLimit;

    public function __construct(int $imagesLimit = 25, int $imagesOffset = 0)
    {
        $this->imagesLimit = $imagesLimit;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $imagesLimit = $this->imagesLimit;

        return $model->with(['images' => function($q) use ($imagesLimit) {
            $q->limit($imagesLimit)->orderBy('id', 'desc');
        }]);
    }

}

<?php

namespace Modules\Media\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Media\Entities\ImageMediaGallery;

class ImageMediaGalleryRepository extends BaseAppRepository
{
    public function model()
    {
        return ImageMediaGallery::class;
    }

}
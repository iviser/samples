<?php

namespace Modules\Media\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Media\Entities\AudioMedia;

class AudioMediaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the people.
     *
     * @param  \App\User $user
     * @param AudioMedia $audioMedia
     * @return mixed
     */
    public function view(User $user, AudioMedia $audioMedia)
    {
        //
    }

    /**
     * Determine whether the user can create people.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the people.
     *
     * @param  \App\User $user
     * @param AudioMedia $audioMedia
     * @return mixed
     */
    public function update(User $user, AudioMedia $audioMedia)
    {
        return $user->hasAnyRole(['super_admin', 'admin', 'editor']);
    }

    /**
     * Determine whether the user can delete the people.
     *
     * @param  \App\User $user
     * @param AudioMedia $audioMedia
     * @return mixed
     */
    public function delete(User $user, AudioMedia $audioMedia)
    {
        return $user->hasAnyRole(['super_admin', 'admin', 'editor']);
    }
}

@extends('media::layouts.master')

@section('stylesheets')

@endsection

@section('page-header')
    <h1>{{ $audio->title }}</h1>
@stop

@section('content')
    <div class="row media-audio">
        <div class="col-md-9" >

            @if ($track = $audio->mediaFile)
                <audio-player single-track="{{$track->getUrl()}}"></audio-player>
            @else
                <p>Sorry, the audio file is no available at the moment</p>
            @endif


            <div class="panel panel-default audio-albums">
                <div class="panel-heading">
                    <h3 class="panel-title">Other Albums</h3>
                </div>

                <div class="panel-body">
                    <div class="media-audio-albums-item">
                        <a href="#">
                            <h4>New album</h4>
                            <img src="http://dev1-ams.ellenwhite.org/storage/app/public/audio_albums/April2017/l20hxWYKYCWy0EK6uqEY.jpg" alt="">
                        </a>
                    </div>
                    <div class="media-audio-albums-item">
                        <a href="#">
                            <h4>New album</h4>
                            <img src="http://dev1-ams.ellenwhite.org/storage/app/public/audio_albums/April2017/l20hxWYKYCWy0EK6uqEY.jpg" alt="">
                        </a>
                    </div>
                    <div class="media-audio-albums-item">
                        <a href="#">
                            <h4>New album</h4>
                            <img src="http://dev1-ams.ellenwhite.org/storage/app/public/audio_albums/April2017/l20hxWYKYCWy0EK6uqEY.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            @if($album = $audio->album)
                <div class="panel panel-default media-audio-metadata">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $album->title }}</h3>
                    </div>

                    <div class="panel-body">
                        <img src="{{ $album->thumbnail }}" alt="">
                        <h4>Other compositions from this album</h4>
                        <ul class="media-audio-metadata__track-list">
                            @foreach($album->tracks as $track)
                                <li><span class="glyphicon glyphicon-music"></span><a href="{{ route('audio.item', ['id' => $track->id]) }}">{{ $track->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
@push('scripts')

@endpush
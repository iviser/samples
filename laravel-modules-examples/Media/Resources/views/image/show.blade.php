@extends('media::layouts.master')

@section('stylesheets')

@endsection

@section('page-header')
    <h1>{{ $image->title }}</h1>
@stop

@section('internal-page')
    <images-single-page image-id="{{$image->id}}"></images-single-page>
@endsection

@push('scripts')

@endpush
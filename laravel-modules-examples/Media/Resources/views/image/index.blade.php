@extends('media::layouts.master')

@section('page-header')
<h1>Images</h1>
@stop

@section('content')
    <images-list></images-list>
@stop

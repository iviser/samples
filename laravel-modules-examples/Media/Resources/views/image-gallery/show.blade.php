@extends('media::layouts.master')

@section('stylesheets')

@endsection

@section('page-header')
    <h1>Image gallery: {{ $gallery->title }}</h1>
@stop

@section('internal-page')
    <div class="container-fluid" id="images">
        <div class="row">
            @foreach($images as $image)
                <a href="{{ $image->route() }}">
                    <img src="{{ $image->thumbnail() }}" >
                </a>
            @endforeach
        </div>
    </div>
    <div class="text-center">
        {{ $images->links() }}
    </div>
@endsection

@push('scripts')

@endpush
@extends('page-base')

@section('page-header')
    <h1>Media</h1>
@stop

@section('content')
	@yield('internal-page')
@stop

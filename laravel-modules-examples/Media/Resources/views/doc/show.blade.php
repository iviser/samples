@extends('media::layouts.master')

@section('stylesheets')

@endsection

@section('page-header')
    <h1>{{ $doc->title }}</h1>
@stop
@php
    $media = $doc->doc;

    $textTabClasses = '';
    $docTabClasses = '';

    if (!$media && ($text)) {
        $textTabClasses = 'active';
    } elseif ($media) {
        $docTabClasses = 'active';
    }
@endphp

@section('internal-page')
    <div class="row">
        <div class="col-md-12">
            <div class="inner">
                <ul id="previewer-tabs" class="nav nav-tabs">
                    @if ($media)
                        <li class="active">
                            <a href="#flowpaper" data-toggle="tab">PDF viewer</a>
                        </li>
                    @endif
                    @if ($text)
                        <li>
                            <a href="#text" data-toggle="tab">Text</a>
                        </li>
                    @endif
                    @if($doc->summary)
                        <li>
                            <a href="#metadata" data-toggle="tab">Metadata</a>
                        </li>
                    @endif
                </ul>

                <div class="tab-content">
                    @if ($media)
                        <div id="flowpaper" class="tab-pane fade in active">
                            <div class="panel-body">
                                <flowpaper-viewer
                                    pdf-path="{{ $media->getPath() }}"
                                ></flowpaper-viewer>
                            </div>
                        </div>
                    @endif
                    @if ($text)
                        <div id="text" class="tab-pane fade">
                            <div class="panel-body col-md-12">
                                {!! $text!!}
                            </div>
                        </div>
                    @endif
                    @if($doc->summary)
                        <div id="metadata" class="tab-pane fade">
                            <div class="panel-body col-md-12">
                                <div class="panel-heading">Summary</div>
                                <div class="panel-body">{!! $doc->summary !!}</div>
                            </div>
                        </div>  
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush
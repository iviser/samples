<?php

namespace Modules\Media\Entities;


use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\References;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\Media as BaseMedia;

class ImageMediaGallery extends Model implements HasMedia, ReferenceInterface
{

    use HasMediaTrait, References;

    const COVER_MEDIA_COLLECTION_NAME = 'cover';

    protected $table = "media_image_galleries";

    protected $fillable = [
        'title',
        'body',
    ];

    protected $contentTypeName = 'media_image_gallery';

    public function images()
    {
         return $this->belongsToMany(
             ImageMedia::class,
             'media_image_gallery_images',
             'gallery_id',
             'image_id')->as('images');
    }

    /**
     * Cover media file
     * The cover must a single image file. Multiple media files with 'cover' media collection name are prohibited
     *
     * @return $this
     */
    public function cover()
    {
        return $this->morphOne(BaseMedia::class, 'model')
                    ->where('collection_name', '=', static::COVER_MEDIA_COLLECTION_NAME);
    }


    public function registerMediaConversions(BaseMedia $media = null)
    {
        $this->addMediaConversion('thumb')
             ->width(150)
             ->sharpen(10);
    }

}

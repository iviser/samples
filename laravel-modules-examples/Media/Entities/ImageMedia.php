<?php

namespace Modules\Media\Entities;

use Modules\Media\GraphQL\Types\MediaImageType;
use Modules\Media\Entities\Media as BasicMedia;
use Modules\Media\Es\Documents\ImageMediaDocModel as EsDoc;
use Modules\Taxonomy\Entities\Traits\TaxonomyTrait;

/**
 * Class ImageMedia
 * @package Modules\Media\Entities
 */
class ImageMedia extends BasicMedia
{
    use TaxonomyTrait;

    protected $fillable = [
        'title', 'body', 'taxonomy', 'media_data', 'metadata', 'user_id'
    ];

    protected $table = 'media_image';

    protected $casts = [
        'taxonomy' => 'object',
        'media_data' => 'object',
        'metadata' => 'object',
    ];

    protected $routeName = 'image.item';

    protected $mappingName = 'media_image';

    protected $contentTypeName = 'media_image';

    public function getMediaCollectionName(): string
    {
        return 'media_image';
    }

    public function getGqlType(): string
    {
        return (new MediaImageType())->name;
    }

    public function galleries()
    {
        return $this->belongsToMany(
            ImageMediaGallery::class,
            'media_image_gallery_images',
            'image_id',
            'gallery_id');
    }

    public function toSearchableArray()
    {
        return (new EsDoc($this))->source();
    }
}

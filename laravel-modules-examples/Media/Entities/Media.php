<?php

namespace Modules\Media\Entities;

use App\Entities\Interfaces\GqlEntity;
use App\Entities\Traits\EsSearchable;
use App\Entities\Traits\TypeTrait;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Interfaces\ContentInterface;
use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\ContentTrait;
use App\Entities\Traits\References;
use Modules\Media\Entities\Interfaces\MediaEntityInterface;
use Modules\Media\Entities\Interfaces\MediaLibraryInterface;
use Modules\Taxonomy\Entities\Traits\TaxonomyTrait;
use Modules\Topics\Entities\Topic;
use Prettus\Repository\Contracts\Presentable;
use Prettus\Repository\Traits\PresentableTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Modules\Common\Entities\Traits\ThumbnailTrait;
use Spatie\MediaLibrary\Media as BaseMedia;

abstract class Media extends Model implements
    HasMedia,
    MediaEntityInterface,
    MediaLibraryInterface,
    HasMediaConversions,
    EntityThumbInterface,
    ContentInterface,
    Presentable,
    GqlEntity,
    ReferenceInterface
{
    use MediaTrait, ThumbnailTrait, ContentTrait, TypeTrait, References, TaxonomyTrait, PresentableTrait, EsSearchable;

    public function media()
    {
        return $this->morphMany(BaseMedia::class, 'model');
    }

    public function mediaFile()
    {
        return $this->morphOne(BaseMedia::class, 'model')
            ->where('collection_name', '=', $this->getMediaCollectionName());
    }

    public function registerMediaConversions(BaseMedia $media = null)
    {
        $this->addMediaConversion('thumb')
             ->width(150)
             ->sharpen(10);
    }

    public function topics()
    {
        return $this->morphToMany(
            Topic::class,
            'model',
            'content_topics_reference');
    }

}

<?php

namespace Modules\Media\Entities;

use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\References;
use Modules\Media\GraphQL\Types\MediaDocType;
use Modules\Media\Entities\Media as BasicMedia;
use Modules\Media\Es\Documents\DocumentMediaDocModel as EsDoc;
use Modules\Taxonomy\Entities\Traits\TaxonomyTrait;

/**
 * Class DocMedia
 * @package Modules\Media\Entities
 */
class DocMedia extends BasicMedia implements ReferenceInterface
{
    use References, TaxonomyTrait;

    protected $fillable = [
        'title', 'body', 'text', 'summary', 'taxonomy', 'media_data', 'metadata', 'user_id'
    ];

    protected $table = 'media_doc';

    protected $casts = [
        'taxonomy' => 'object',
        'references' => 'object',
        'media_data' => 'object',
        'metadata' => 'object',
    ];

    protected $routeName = 'document.item';

    protected $mappingName = 'media_doc';

    protected $contentTypeName = 'media_doc';

    public function getMediaCollectionName(): string
    {
        return 'media_doc';
    }

    public function getDocAttribute($value)
    {
        return $this->getMedia('media_doc')->first() ?: null;
    }

    public function getDocUrlAttribute($value)
    {
        return $this->doc ? $this->doc->getUrl() : null;
    }

    public function getDocPathAttribute($value)
    {
        return $this->doc ? $this->doc->getPath() : null;
    }

    public function getGqlType(): string
    {
        return (new MediaDocType())->name;
    }

    public function toSearchableArray()
    {
        return (new EsDoc($this))->source();
    }
}

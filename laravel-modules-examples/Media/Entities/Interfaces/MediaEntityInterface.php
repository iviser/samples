<?php


namespace Modules\Media\Entities\Interfaces;


interface MediaEntityInterface
{
    public function getMediaCollectionName(): string;
}

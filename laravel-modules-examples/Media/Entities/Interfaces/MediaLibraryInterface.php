<?php


namespace Modules\Media\Entities\Interfaces;

use Spatie\MediaLibrary\Media;

interface MediaLibraryInterface
{
    /**
     * Add media file from url
     *
     * @param string $url
     * @param string $mediaLibrary
     * @return Media
     */
    public function addMediaFileFromUrl(string $url, string $mediaLibrary = ''): Media;

}

<?php

namespace Modules\Media\Entities;

use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\References;
use App\Entities\Traits\TypeTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\Media as BaseMedia;

class AudioAlbum extends Model implements HasMedia, ReferenceInterface
{
    use TypeTrait, HasMediaTrait, References;

    const COVER_MEDIA_COLLECTION_NAME = 'cover';

    protected $fillable = [
        'title',
    ];

    protected $contentTypeName = 'media_audio_album';

    protected $table = 'media_audio_album';

    public function tracks()
    {
        return $this->hasMany(AudioMedia::class, 'album_id');
    }

    /**
     * Cover media file
     * The cover must be a single image file. Multiple media files with 'cover' media collection name are prohibited
     *
     * @return $this
     */
    public function cover()
    {
        return $this->morphOne(BaseMedia::class, 'model')
                    ->where('collection_name', '=', static::COVER_MEDIA_COLLECTION_NAME);
    }


}

<?php

namespace Modules\Media\Tests\Api;

use App\Services\Registries\EntityRegistry;
use Illuminate\Support\Facades\Artisan;
use Modules\Media\Entities\DocMedia;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Tests\Traits\TestReferenceTrait;
use Tests\Traits\TestTermsReferenceTrait;

class DocMediaTest extends TestCase
{
    use TestReferenceTrait, TestTermsReferenceTrait;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $name = $name ?: 'Media Doc test case';

        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Media'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'Taxonomy'
        ]);
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        // Get token
        $user = $this->createUser();
        $resp = $this->getPasswordGrantAccessToken($user);
        $authInfo = \GuzzleHttp\json_decode($resp->getContent());
        $access_token = $authInfo->access_token;

        // Set up headers
        return ['Authorization' => "Bearer {$access_token}"];
    }

    public function createDoc(): DocMedia
    {
        $doc = factory(DocMedia::class)->make();
        $doc->save();

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();
        $doc->ref($referencedACollection);
        $doc->load('references');

        return $doc;
    }

    /**
     * Create album media library correctly.
     *
     * @return void
     */
    public function testDocCreatedCorrectly()
    {
        $docFilename = $this->faker->md5 .'.doc';
        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'summary' => $this->faker->sentence,
            'media' => UploadedFile::fake()->create($docFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/documents?with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
             'title' => $payload['title'],
             'body' => $payload['body'],
             'text' => $payload['text'],
             'summary' => $payload['summary'],
             'mediaFile' => [
                 'file_name' => $payload['media']->name
             ]
         ]);

    }

    public function testDocCreatedWithReferencesCorrectly()
    {
        $docFilename = $this->faker->md5 .'.doc';
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'summary' => $this->faker->sentence,
            'media' => UploadedFile::fake()->create($docFilename),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/documents?with[]=references&with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'text' => $payload['text'],
            'summary' => $payload['summary'],
            'mediaFile' => [
                'file_name' => $payload['media']->name
            ]
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    public function testDocCreatedWithTermsCorrectly()
    {
        $docFilename = $this->faker->md5 .'.doc';

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'summary' => $this->faker->sentence,
            'media' => UploadedFile::fake()->create($docFilename),
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/documents?with[]=terms&with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'text' => $payload['text'],
            'summary' => $payload['summary'],
            'mediaFile' => [
                'file_name' => $payload['media']->name
            ]
        ])
            ->assertJsonCount(count($terms), 'terms');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testDocUpdatedCorrectly()
    {
        $doc = $this->createDoc();

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'summary' => $this->faker->sentence,
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/documents/{$doc->id}",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'text' => $payload['text'],
            'summary' => $payload['summary'],

        ]);

    }

    public function testDocUpdatedWithReferencesCorrectly()
    {
        $doc = $this->createDoc();
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'summary' => $this->faker->sentence,
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/documents/{$doc->id}?with[]=references",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'text' => $payload['text'],
            'summary' => $payload['summary'],
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    public function testDocUpdatedWithTermsCorrectly()
    {
        $doc = $this->createDoc();

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'summary' => $this->faker->sentence,
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/documents/{$doc->id}?with[]=terms",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'text' => $payload['text'],
            'summary' => $payload['summary'],
        ])
            ->assertJsonCount(count($terms), 'terms');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testDocUpdatedMediaCorrectly()
    {
        $doc = $this->createDoc();
        $docFilename = $this->faker()->md5 . '.mp3';

        $payload = [
            'media' => UploadedFile::fake()->create($docFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/documents/{$doc->id}/file",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'mediaFile' => [
                'file_name' => $payload['media']->name
            ]
        ]);

    }
}

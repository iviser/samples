<?php

namespace Modules\Media\Tests\Api;

use App\Services\Registries\EntityRegistry;
use Illuminate\Support\Facades\Artisan;
use Modules\Media\Entities\AudioAlbum;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Tests\Traits\TestReferenceTrait;

class AudioAlbumsTest extends TestCase
{
    use TestReferenceTrait;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $name = $name ?: 'Media Audio Album test case';

        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Media'
        ]);
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        // Get token
        $user = $this->createUser();
        $resp = $this->getPasswordGrantAccessToken($user);
        $authInfo = \GuzzleHttp\json_decode($resp->getContent());
        $access_token = $authInfo->access_token;

        // Set up headers
        return ['Authorization' => "Bearer {$access_token}"];
    }

    public function createAlbum(): AudioAlbum
    {
        $album = factory(AudioAlbum::class)->make();
        $album->save();
        $album->addMedia($this->faker()->image())
              ->toMediaCollection(AudioAlbum::COVER_MEDIA_COLLECTION_NAME);

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();
        $album->ref($referencedACollection);
        $album->load('references');

        $album->load('cover');

        return $album;
    }

    /**
     * Create album media library correctly.
     *
     * @return void
     */
    public function testAudioAlbumCreatedCorrectly()
    {
        $imageFilename = $this->faker->md5 .'.png';
        // Set up request body
        $playload = [
            'title' => $this->faker()->sentence,
            'cover' => UploadedFile::fake()->image($imageFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/audio-albums?with[]=cover',
            $playload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
             'title' => $playload['title'],
             'cover' => [
                 'file_name' => $playload['cover']->name
             ]
         ]);

    }

    public function testAudioAlbumCreatedWithReferencesCorrectly()
    {
        $imageFilename = $this->faker->md5 .'.png';
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $playload = [
            'title' => $this->faker()->sentence,
            'cover' => UploadedFile::fake()->image($imageFilename),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/audio-albums?with[]=references&with[]=cover',
            $playload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $playload['title'],
            'cover' => [
                'file_name' => $playload['cover']->name
            ]
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testAudioAlbumUpdatedCorrectly()
    {
        $album = $this->createAlbum();

        // Set up request body
        $playload = [
            'title' => $this->faker()->sentence,
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/audio-albums/{$album->id}",
            $playload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $playload['title'],
        ]);

    }

    public function testAudioAlbumUpdatedWithReferencesCorrectly()
    {
        $album = $this->createAlbum();
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $playload = [
            'title' => $this->faker()->sentence,
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/audio-albums/{$album->id}?with[]=references",
            $playload,
            $this->getHeaders()
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $playload['title'],
            ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testAudioAlbumUpdatedCoverCorrectly()
    {
        $album = $this->createAlbum();

        $imageFilename = $this->faker()->md5 . '.png';

        $playload = [
            'cover' => UploadedFile::fake()->image($imageFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/audio-albums/{$album->id}/cover",
            $playload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'cover' => [
                'file_name' => $playload['cover']->name
            ]
        ]);

    }
}

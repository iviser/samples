<?php

namespace Modules\Media\Tests\Api;

use App\Services\Registries\EntityRegistry;
use Illuminate\Support\Facades\Artisan;
use Modules\Media\Entities\AudioMedia;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Tests\Traits\TestReferenceTrait;
use Tests\Traits\TestTermsReferenceTrait;

class AudioMediaTest extends TestCase
{
    use TestReferenceTrait, TestTermsReferenceTrait;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $name = $name ?: 'Media Audio test case';

        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Media'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'Taxonomy'
        ]);
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        // Get token
        $user = $this->createUser();
        $resp = $this->getPasswordGrantAccessToken($user);
        $authInfo = \GuzzleHttp\json_decode($resp->getContent());
        $access_token = $authInfo->access_token;

        // Set up headers
        return ['Authorization' => "Bearer {$access_token}"];
    }

    public function createAudio(): AudioMedia
    {
        $audio = factory(AudioMedia::class)->make();
        $audio->save();

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();
        $audio->ref($referencedACollection);
        $audio->load('references');

        return $audio;
    }

    /**
     * Create album media library correctly.
     *
     * @return void
     */
    public function testAudioCreatedCorrectly()
    {
        $audioFilename = $this->faker->md5 .'.mp3';
        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'mediaFile' => UploadedFile::fake()->create($audioFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/audio?with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
             'title' => $payload['title'],
             'body' => $payload['body'],
             'mediaFile' => [
                 'file_name' => $payload['mediaFile']->name
             ]
         ]);

    }

    public function testAudioCreatedWithReferencesCorrectly()
    {
        $audioFilename = $this->faker->md5 .'.mp3';
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'mediaFile' => UploadedFile::fake()->create($audioFilename),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/audio?with[]=references&with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'mediaFile' => [
                'file_name' => $payload['mediaFile']->name
            ]
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    public function testAudioCreatedWithTermsCorrectly()
    {
        $audioFilename = $this->faker->md5 .'.mp3';

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'mediaFile' => UploadedFile::fake()->create($audioFilename),
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/audio?with[]=terms&with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'mediaFile' => [
                'file_name' => $payload['mediaFile']->name
            ]
        ])
            ->assertJsonCount(count($terms), 'terms');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testAudioUpdatedCorrectly()
    {
        $audio = $this->createAudio();

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/audio/{$audio->id}",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body']
        ]);

    }

    public function testAudioUpdatedWithReferencesCorrectly()
    {
        $audio = $this->createAudio();
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/audio/{$audio->id}?with[]=references",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body']
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    public function testAudioUpdatedWithTermsCorrectly()
    {
        $audio = $this->createAudio();

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/audio/{$audio->id}?with[]=terms",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body']
        ])
            ->assertJsonCount(count($terms), 'terms');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testAudioUpdatedMediaFileCorrectly()
    {
        $audio = $this->createAudio();
        $audioFilename = $this->faker()->md5 . '.mp3';

        $payload = [
            'mediaFile' => UploadedFile::fake()->create($audioFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/audio/{$audio->id}/file",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'mediaFile' => [
                'file_name' => $payload['mediaFile']->name
            ]
        ]);

    }
}

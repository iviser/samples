<?php

namespace Modules\Media\Tests\Api;

use App\Services\Registries\EntityRegistry;
use Illuminate\Support\Facades\Artisan;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Entities\ImageMediaGallery;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Tests\Traits\TestReferenceTrait;

class ImageGalleriesTest extends TestCase
{
    use TestReferenceTrait;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $name = $name ?: 'Media Image Gallery test case';

        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Media'
        ]);
    }

    public function createGallery(): ImageMediaGallery
    {
        $gallery = factory(ImageMediaGallery::class)->make();
        $gallery->save();
        $gallery->addMedia($this->faker()->image())
              ->toMediaCollection(ImageMediaGallery::COVER_MEDIA_COLLECTION_NAME);

        $image = $this->createMediaImage();

        $gallery->images()->attach([$image->id]);

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();
        $gallery->ref($referencedACollection);
        $gallery->load('references');

        $gallery->load('cover');
        $gallery->load('images');

        return $gallery;
    }

    public function createMediaImage(): ImageMedia
    {
        $image = factory(ImageMedia::class)->make();
        $image->save();
        $image->addMedia($this->faker()->image())
            ->toMediaCollection($image->getMediaCollectionName());

        $image->load('mediaFile');

        return $image;
    }

    /**
     * Create gallery media library correctly.
     *
     * @return void
     */
    public function testImageGalleryCreatedCorrectly()
    {
        $user = $this->createUser();
        $imageFilename = $this->faker->md5 .'.png';
        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'cover' => UploadedFile::fake()->image($imageFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/image-galleries?with[]=cover',
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
             'title' => $payload['title'],
             'body' => $payload['body'],
             'cover' => [
                 'file_name' => $payload['cover']->name
             ]
         ]);
    }

    public function testImageGalleryCreatedWithReferencesCorrectly()
    {
        $user = $this->createUser();
        $imageFilename = $this->faker->md5 .'.png';
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'cover' => UploadedFile::fake()->image($imageFilename),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/image-galleries?with[]=references&with[]=cover',
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'cover' => [
                'file_name' => $payload['cover']->name
            ]
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    /**
     * Update gallery media library correctly.
     *
     * @return void
     */
    public function testImageGalleryUpdatedCorrectly()
    {
        $user = $this->createUser();
        $gallery = $this->createGallery();

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/image-galleries/{$gallery->id}",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
        ]);
    }

    public function testImageGalleryUpdatedWithReferencesCorrectly()
    {
        $user= $this->createUser();
        $gallery = $this->createGallery();
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/image-galleries/{$gallery->id}?with[]=references",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
            ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    /**
     * Update gallery media library correctly.
     *
     * @return void
     */
    public function testImageGalleryUpdatedCoverCorrectly()
    {
        $user = $this->createUser();
        $gallery = $this->createGallery();

        $imageFilename = $this->faker()->md5 . '.png';

        $payload = [
            'cover' => UploadedFile::fake()->image($imageFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/image-galleries/{$gallery->id}/cover",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'cover' => [
                'file_name' => $payload['cover']->name
            ]
        ]);
    }

    /**
     * Store bulk images for gallery correctly.
     *
     * @return void
     */
    public function testBulkImagesForImageGalleryStoredCorrectly()
    {
        $user = $this->createUser();
        $gallery = $this->createGallery();

        $imageFilename1 = $this->faker()->md5 . '.png';
        $imageFilename2 = $this->faker()->md5 . '.png';
        $imageFilename3 = $this->faker()->md5 . '.png';

        $payload = [
            'images' => [
                UploadedFile::fake()->image($imageFilename1),
                UploadedFile::fake()->image($imageFilename2),
                UploadedFile::fake()->image($imageFilename3)
            ]
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/image-galleries/{$gallery->id}/images",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            0 => [
                'mediaFile' => [
                    'file_name' => $payload['images'][0]->name
                ]
            ],
            1 => [
                'mediaFile' => [
                    'file_name' => $payload['images'][1]->name
                ]
            ],
            2 => [
                'mediaFile' => [
                    'file_name' => $payload['images'][2]->name
                ]
            ],
        ]);
    }

    /**
     * Attach images to gallery correctly.
     *
     * @return void
     */
    public function testImagesForImageGalleryAttachedCorrectly()
    {
        $user = $this->createUser();
        $gallery = $this->createGallery();
        $image1 = $this->createMediaImage();
        $image2 = $this->createMediaImage();
        $image3 = $this->createMediaImage();

        $payload = [
            'ids' => [
                $image1->id,
                $image2->id,
                $image3->id
            ]
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/image-galleries/{$gallery->id}/attach/images",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200);
    }

    /**
     * Detach images to gallery correctly.
     *
     * @return void
     */
    public function testImagesForImageGalleryDetachedCorrectly()
    {
        $user = $this->createUser();
        $gallery = $this->createGallery();

        $payload = [
            'ids' => [
                $gallery->images->first()->id,
            ]
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/image-galleries/{$gallery->id}/detach/images",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200);
    }

    public function testImageGalleryIsDeletedCorrectly()
    {
        $user = $this->createUser();
        $gallery = $this->createGallery();

        $payload = [];

        // Perform a request
        $this->json(
            'DELETE',
            "http://localhost/api/v1/media/image-galleries/{$gallery->id}",
            $payload,
            $this->getUserHeaders($user)
        )
            ->assertStatus(204);
    }
}

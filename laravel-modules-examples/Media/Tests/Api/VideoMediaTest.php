<?php

namespace Modules\Media\Tests\Api;

use App\Services\Registries\EntityRegistry;
use Illuminate\Support\Facades\Artisan;
use Modules\Media\Entities\VideoMedia;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Tests\Traits\TestReferenceTrait;
use Tests\Traits\TestTermsReferenceTrait;

class VideoMediaTest extends TestCase
{
    use TestReferenceTrait, TestTermsReferenceTrait;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $name = $name ?: 'Media Video test case';

        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Media'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'Taxonomy'
        ]);
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        // Get token
        $user = $this->createUser();
        $resp = $this->getPasswordGrantAccessToken($user);
        $authInfo = \GuzzleHttp\json_decode($resp->getContent());
        $access_token = $authInfo->access_token;

        // Set up headers
        return ['Authorization' => "Bearer {$access_token}"];
    }

    public function createVideo(): VideoMedia
    {
        $video = factory(VideoMedia::class)->make();
        $video->save();

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();
        $video->ref($referencedACollection);
        $video->load('references');

        return $video;
    }

    /**
     * Create album media library correctly.
     *
     * @return void
     */
    public function testVideoCreatedCorrectly()
    {
        $videoFilename = $this->faker->md5 .'.mp4';
        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'mediaFile' => UploadedFile::fake()->create($videoFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/video?with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
             'title' => $payload['title'],
             'body' => $payload['body'],
             'mediaFile' => [
                 'file_name' => $payload['mediaFile']->name
             ]
         ]);

    }

    public function testVideoCreatedWithReferencesCorrectly()
    {
        $videoFilename = $this->faker->md5 .'.mp4';
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'mediaFile' => UploadedFile::fake()->create($videoFilename),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/video?with[]=references&with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'mediaFile' => [
                'file_name' => $payload['mediaFile']->name
            ]
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    public function testVideoCreatedWithTermsCorrectly()
    {
        $videoFilename = $this->faker->md5 .'.mp4';

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'mediaFile' => UploadedFile::fake()->create($videoFilename),
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/video?with[]=terms&with[]=mediaFile',
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'mediaFile' => [
                'file_name' => $payload['mediaFile']->name
            ]
        ])
            ->assertJsonCount(count($terms), 'terms');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testVideoUpdatedCorrectly()
    {
        $video = $this->createVideo();

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/video/{$video->id}",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body']
        ]);

    }

    public function testVideoUpdatedWithReferencesCorrectly()
    {
        $video = $this->createVideo();
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/video/{$video->id}?with[]=references",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body']
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');

    }

    public function testVideoUpdatedWithTermsCorrectly()
    {
        $video = $this->createVideo();

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker()->sentence,
            'body' => $this->faker->paragraph,
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/video/{$video->id}?with[]=terms",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body']
        ])
            ->assertJsonCount(count($terms), 'terms');

    }

    /**
     * Update album media library correctly.
     *
     * @return void
     */
    public function testVideoUpdatedMediaFileCorrectly()
    {
        $video = $this->createVideo();

        $videoFilename = $this->faker()->md5 . '.mp4';

        $payload = [
            'mediaFile' => UploadedFile::fake()->create($videoFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/video/{$video->id}/file",
            $payload,
            $this->getHeaders()
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'mediaFile' => [
                'file_name' => $payload['mediaFile']->name
            ]
        ]);

    }
}

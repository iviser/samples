<?php

namespace Modules\Media\Tests\Api;

use App\Services\Registries\EntityRegistry;
use Illuminate\Support\Facades\Artisan;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Entities\ImageMediaGallery;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Tests\Traits\TestReferenceTrait;
use Tests\Traits\TestTermsReferenceTrait;

class MediaImageTest extends TestCase
{
    use TestReferenceTrait, TestTermsReferenceTrait;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $name = $name ?: 'Media Image test case';

        parent::__construct($name, $data, $dataName);
    }

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Media'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'Taxonomy'
        ]);
    }

    public function createMediaImage(): ImageMedia
    {
        $image = factory(ImageMedia::class)->make();
        $image->save();
        $image->addMedia($this->faker()->image())
            ->toMediaCollection($image->getMediaCollectionName());

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();
        $image->ref($referencedACollection);
        $image->load('references');

        $image->load('mediaFile');

        return $image;
    }

    /**
     * Create media image correctly.
     *
     * @return void
     */
    public function testMediaImageCreatedCorrectly()
    {
        $user = $this->createUser();
        $imageFilename = $this->faker->md5 .'.png';
        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'media' => UploadedFile::fake()->image($imageFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/images?with[]=mediaFile',
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
             'title' => $payload['title'],
             'body' => $payload['body'],
             'mediaFile' => [
                 'file_name' => $payload['media']->name
             ]
         ]);
    }

    public function testMediaImageCreatedWithReferencesCorrectly()
    {
        $user = $this->createUser();
        $imageFilename = $this->faker->md5 .'.png';
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'media' => UploadedFile::fake()->image($imageFilename),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/images?with[]=references&with[]=mediaFile',
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'mediaFile' => [
                'file_name' => $payload['media']->name
            ]
        ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    public function testMediaImageCreatedWithTermsCorrectly()
    {
        $user = $this->createUser();
        $imageFilename = $this->faker->md5 .'.png';

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'media' => UploadedFile::fake()->image($imageFilename),
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            'http://localhost/api/v1/media/images?with[]=terms&with[]=mediaFile',
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
            'mediaFile' => [
                'file_name' => $payload['media']->name
            ]
        ])
            ->assertJsonCount(count($terms), 'terms');
    }

    /**
     * Update media image correctly.
     *
     * @return void
     */
    public function testMediaImageUpdatedCorrectly()
    {
        $user = $this->createUser();
        $image = $this->createMediaImage();

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/images/{$image->id}",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200);
        $resp->assertJson([
            'title' => $payload['title'],
            'body' => $payload['body'],
        ]);
    }

    public function testMediaImageUpdatedWithReferencesCorrectly()
    {
        $user= $this->createUser();
        $image = $this->createMediaImage();
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/images/{$image->id}?with[]=references",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
            ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    public function testMediaImageUpdatedWithTermsCorrectly()
    {
        $user= $this->createUser();
        $image = $this->createMediaImage();

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        // Set up request body
        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->text,
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/images/{$image->id}?with[]=terms",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
            ])
            ->assertJsonCount(count($terms), 'terms');
    }

    /**
     * Update image media library correctly.
     *
     * @return void
     */
    public function testMediaImageUpdatedFileCorrectly()
    {
        $user = $this->createUser();
        $image = $this->createMediaImage();

        $imageFilename = $this->faker()->md5 . '.png';

        $payload = [
            'media' => UploadedFile::fake()->image($imageFilename)
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/media/images/{$image->id}/file?with[]=mediaFile",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201);
        $resp->assertJson([
            'mediaFile' => [
                'file_name' => $payload['media']->name
            ]
        ]);
    }

    public function testMediaImageIsDeletedCorrectly()
    {
        $user = $this->createUser();
        $image = $this->createMediaImage();

        $payload = [];

        // Perform a request
        $this->json(
            'DELETE',
            "http://localhost/api/v1/media/images/{$image->id}",
            $payload,
            $this->getUserHeaders($user)
        )
            ->assertStatus(204);
    }
}

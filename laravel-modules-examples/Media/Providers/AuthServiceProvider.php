<?php

namespace Modules\Media\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\Media\Entities\AudioAlbum;
use Modules\Media\Entities\AudioMedia;
use Modules\Media\Entities\DocMedia;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Entities\ImageMediaGallery;
use Modules\Media\Entities\VideoMedia;
use Modules\Media\Policies\AudioAlbumPolicy;
use Modules\Media\Policies\AudioMediaPolicy;
use Modules\Media\Policies\DocMediaPolicy;
use Modules\Media\Policies\ImageMediaGalleryPolicy;
use Modules\Media\Policies\ImageMediaPolicy;
use Modules\Media\Policies\VideoMediaPolicy;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        AudioAlbum::class => AudioAlbumPolicy::class,
        AudioMedia::class => AudioMediaPolicy::class,
        DocMedia::class => DocMediaPolicy::class,
        ImageMedia::class => ImageMediaPolicy::class,
        ImageMediaGallery::class => ImageMediaGalleryPolicy::class,
        VideoMedia::class => VideoMediaPolicy::class
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
<?php

namespace Modules\Media\Console;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Collection;
use App\Entities\Media;
use Modules\Media\Jobs\HealthCheckJob;
use App\Repositories\MediaRepository;

class PerformHealthCheck extends Command
{
    use ConfirmableTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'media:healthcheck {--t|modelType=} {--s|status=} {--ids=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the health of the media entry';

    /** @var MediaRepository */
    protected $mediaRepository;

    /** @var array */
    protected $errorMessages = [];

    public function __construct(MediaRepository $mediaRepository)
    {
        parent::__construct();

        $this->mediaRepository = $mediaRepository;
    }
    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return;
        }

        $mediaFiles = $this->getMediaToBeRegenerated();

        $progressBar = $this->output->createProgressBar($mediaFiles->count());

        $this->errorMessages = [];

        $mediaFiles->each(function (Media $media) use ($progressBar) {
            try {
                $job = new HealthCheckJob($media->id);
                dispatch($job)->onQueue('media');

            } catch (\Exception $exception) {
                $this->errorMessages[$media->id] = $exception->getMessage();
            }

            $progressBar->advance();
        });

        $progressBar->finish();

        if (count($this->errorMessages)) {
            $this->warn('All done, but with some error messages:');

            foreach ($this->errorMessages as $mediaId => $message) {
                $this->warn("Media id {$mediaId}: `{$message}`");
            }
        }

        $this->info('All done!');
    }

    public function getMediaToBeRegenerated(): Collection
    {
        $modelType = $this->option('modelType') ?? null;
        $status = $this->option('status') ?? null;
        $mediaIds = $this->option('ids');

        $repo = $status ? $this->mediaRepository->getWithStatus($status) : $this->mediaRepository;

        if ($mediaIds) {
            if (! is_array($mediaIds)) {
                $mediaIds = explode(',', $mediaIds);
            }

            return $repo->getByIds($mediaIds)->get();
        }

        if ($modelType) {
            return $repo->getByModelType($modelType)->get();
        }

        return $repo->all();
    }

}

<?php

namespace Modules\Media\Console;

use Illuminate\Console\Command;
use Modules\Media\Entities\Media;
use Modules\Media\Jobs\PropagateMediaToSelectelBatch;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MoveMediaToSelectel extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'media:move-to-selectel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {

        $count = \App\Entities\Media::where('disk', '=', 'media')->count();

        $chunkSize = $this->argument('chunck-size');

        if ($count > $chunkSize) {

            $chunks = (int) ($count / $chunkSize);
            $reminder = $count % $chunkSize;

            for ($i = 0 ; $i <= $chunks; $i++) {

                if ($i === $chunks) {
                    $offset = $i * $chunkSize;
                    $limit = $reminder;
                } else {
                    $offset = $i * $chunkSize;
                    $limit = $chunkSize;
                }

                $job = new PropagateMediaToSelectelBatch($limit, $offset, (bool) $this->option('delete-original'));
                dispatch($job->onQueue('media'));
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['chunck-size', InputArgument::OPTIONAL, 'A chunk size of the batch.', 100],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['delete-original', 'd', InputOption::VALUE_NONE, 'Removes original entity'],
        ];
    }
}

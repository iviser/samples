<?php

namespace Modules\Media\GraphQL\Mutations\Audio;

use GraphQL;
use Folklore\GraphQL\Support\Mutation;
use Modules\Media\Repositories\AudioMediaRepository;
use Modules\Media\Repositories\Criteria\MediaFile;
use Modules\Media\Repositories\Presenters\MediaTypePresenter;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use GraphQL\Type\Definition\Type;

class MediaAudioCreate  extends Mutation
{

    protected $attributes = [
        'name' => 'mediaAudioCreate'
    ];

    public function type()
    {
        return GraphQL::type('MediaAudio');
    }

    public function args()
    {
        return [
            'audio' => ['name' => 'audio', 'type' => Type::nonNull(GraphQL::type('MediaAudioCreateInput'))],
        ];
    }

    public function resolve($root, $args)
    {
        // Create media entity
        $repo = app(AudioMediaRepository::class);
        $mediaAudio = $repo->create($args['audio']);

        // Attach album if it was set
        if (isset($args['audio']['album_id']) && $args['audio']['album_id']) {
            $mediaAudio->attach($args['audio']['album_id']);
        }

        // Attach file to media collection
        // Since `MediaAudioCreateInput` GQL type has required field `file` we can assume on $args['audio'] has `file`
        // key without extra checking
        $filePath = base_path() . Storage::disk('temp')->url($args['audio']['file']['temp_filename']);
        $file = new UploadedFile($filePath
            ,$args['audio']['file']['origin_filename']
            ,$args['audio']['file']['origin_type']
            ,$args['audio']['file']['origin_size']);


        $mediaAudio->addMedia($file)
                   ->toMediaCollection($mediaAudio->getMediaCollectionName());

        // As the file was successfully attached, the temporary file might be cleaned up.
        Storage::disk('temp')->delete($args['audio']['file']['temp_filename']);

        $mediaAudio->save();

        return $repo->pushCriteria(new MediaFile($mediaAudio->getMediaCollectionName()))
                    ->setPresenter(new MediaTypePresenter())
                    ->find($mediaAudio->id);
    }

}
<?php

namespace Modules\Media\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use Modules\Media\Entities\ImageMedia;

class MediaImageType extends GraphQLType
{
    protected $attributes = [
        'name' => 'MediaImage',
        'description' => 'A media image type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the media image'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of the media image'
            ],
            'thumbnail' => [
                'type' => Type::string(),
                'description' => 'Thumbnail'
            ],
            'route' => [
                'type' => Type::string(),
                'description' => 'Thumbnail'
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'The body of the media image'
            ],
            'mediaFile' => [
                'type' => GraphQL::type('Media'),
                'description' => 'Attached media file'
            ],
            'date' => [
                'type' => Type::string(),
                'description' => 'The date of the media image'
            ],
            'galleries' => [
                'type' => Type::listOf(\GraphQL::type('MediaImageGallery')),
                'description' => 'The list of the media image galleries this image belongs to'
            ],

        ];
    }

    public function interfaces()
    {
        return [
            GraphQL::type('MediaContentInterface'),
            GraphQL::type('ContentInterface'),
        ];
    }

}
<?php

namespace Modules\Media\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class MediaImageGalleryType extends GraphQLType
{
    protected $attributes = [
        'name' => 'MediaImageGallery',
        'description' => 'A media image gallery type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the media image gallery'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of the media image gallery'
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'The body of the media image gallery'
            ],
            'images' => [
                'type' => Type::listOf(\GraphQL::type('MediaImage')),
                'description' => 'The list of images within the media image gallery'
            ]

        ];
    }

}
<?php

namespace Modules\Media\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class MediaType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Media',
        'description' => 'A media type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the media'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the media'
            ],
            'file_name' => [
                'type' => Type::string(),
                'description' => 'The file_name of the media'
            ],
            'mime_type' => [
                'type' => Type::string(),
                'description' => 'The mime_type to the media file'
            ],
            'disk' => [
                'type' => Type::string(),
                'description' => 'The storage disk of the media'
            ],
            'size' => [
                'type' => Type::string(),
                'description' => 'File size of the media'
            ],
            'public_path' => [
                'type' => Type::string(),
                'description' => 'File\'s public path'
            ],
            'server_path' => [
                'type' => Type::string(),
                'description' => 'File\'s server path'
            ],
            'public_url' => [
                'type' => Type::string(),
                'description' => 'File\'s public_url'
            ],
        ];
    }

}

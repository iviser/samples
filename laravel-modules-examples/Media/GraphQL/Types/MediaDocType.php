<?php

namespace Modules\Media\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use Modules\Media\Entities\DocMedia;

class MediaDocType extends GraphQLType
{
    protected $attributes = [
        'name' => 'MediaDoc',
        'description' => 'A media document type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the media image'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of the media image'
            ],
            'thumbnail' => [
                'type' => Type::string(),
                'description' => 'Thumbnail'
            ],
            'route' => [
                'type' => Type::string(),
                'description' => 'Thumbnail'
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'The body of the media image'
            ],
            'mediaFile' => [
                'type' => GraphQL::type('Media'),
                'description' => 'Attached media file'
            ],
            'date' => [
                'type' => Type::string(),
                'description' => 'The date of the media image'
            ],
            'text' => [
                'type' => Type::string(),
                'description' => 'The text of the media document'
            ],
            'referencing' => [
                'args' => [
                    'collection' => [
                        'type' => Type::string(),
                        'description' => 'Collection of referenced entities',
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('ContentInterface')),
                'description' => 'Collection of entities',

            ],
            'referenced' => [
                'args' => [
                    'collection' => [
                        'type' => Type::string(),
                        'description' => 'Collection of referencing entities',
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('ContentInterface')),
                'description' => 'Collection of entities',
            ],
        ];
    }

    public function resolveReferencingField($root, $args)
    {
        if ($root instanceof DocMedia) {
            $entity = $root;
        } else {
            $entity = DocMedia::where('id', '=', $root['id'])->first();

        }

        $referencing = $entity->referencing(isset($args['collection']) ? $args['collection'] : null);

        return  $referencing->isNotEmpty() ? $referencing : [];
    }

    public function resolveReferencedField($root, $args)
    {
        if ($root instanceof DocMedia) {
            $entity = $root;
        } else {
            $entity = DocMedia::where('id', '=', $root['id'])->first();

        }

        $referenced = $entity->referenced(isset($args['collection']) ? $args['collection'] : null);

        return  $referenced->isNotEmpty() ? $referenced : [];
    }

    public function interfaces()
    {
        return [
            GraphQL::type('MediaContentInterface'),
            GraphQL::type('ContentInterface'),
        ];
    }

}

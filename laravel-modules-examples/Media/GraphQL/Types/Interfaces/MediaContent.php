<?php

namespace Modules\Media\GraphQL\Types\Interfaces;

use GraphQL;
use Folklore\GraphQL\Support\InterfaceType;
use GraphQL\Type\Definition\Type;
use Modules\Media\Entities\DocMedia;
use Modules\Media\Entities\ImageMedia;

class MediaContent extends InterfaceType
{
    protected $attributes = [
        'name' => 'MediaContentInterface',
        'description' => 'Media content type interface.',
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Id'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Title'
            ],
            'mediaFile' => [
                'type' => GraphQL::type('Media'),
                'description' => 'Attached media file'
            ],
            'thumbnail' => [
                'type' => Type::string(),
                'description' => 'Relative path to the thumbnail'
            ],
            'route' => [
                'type' => Type::string(),
                'description' => 'Relative route to the content'
            ],
        ];
    }

    public function resolveType($root) {
        if ($root->model_type === DocMedia::class) {
            return GraphQL::type('MediaDoc');
        } elseif ($root->model_type === ImageMedia::class) {
            return GraphQL::type('MediaImage');
        }
    }

    public function interfaces() {
        return [
            GraphQL::type('ContentInterface')
        ];
    }
}
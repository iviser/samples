<?php

namespace Modules\Media\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class MediaAudioCollection extends GraphQLType
{
    protected $attributes = [
        'name' => 'MediaAudioCollection',
        'description' => 'Collection of media audio'
    ];

    public function fields()
    {
        return [
            'total' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Total items'
            ],
            'offset' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items offset. How many items skipped'
            ],
            'size' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items size. How many items are taken'
            ],
            'items' => [
                'type' => Type::listOf(\GraphQL::type('MediaAudio')),
                'description' => 'Content interface'
            ],
        ];
    }

    public function interfaces() {
        return [
            GraphQL::type('CollectionInterface')
        ];
    }
}


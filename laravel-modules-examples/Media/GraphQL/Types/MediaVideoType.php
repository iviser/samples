<?php

namespace Modules\Media\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use Modules\Media\Entities\VideoMedia;

class MediaVideoType extends GraphQLType
{
    protected $attributes = [
        'name' => 'MediaVideo',
        'description' => 'A media video type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the media video'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of the media video'
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'The body of the media video'
            ],
            'mediaFile' => [
                'type' => GraphQL::type('Media'),
                'description' => 'Attached media file'
            ],
            'thumbnail' => [
                'type' => Type::string(),
                'description' => 'Thumbnail'
            ],
            'route' => [
                'type' => Type::string(),
                'description' => 'Content Route'
            ],
            'referencing' => [
                'args' => [
                    'collection' => [
                        'type' => Type::string(),
                        'description' => 'Collection of referenced entities',
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('ContentInterface')),
                'description' => 'Collection of entities',

            ],
            'referenced' => [
                'args' => [
                    'collection' => [
                        'type' => Type::string(),
                        'description' => 'Collection of referencing entities',
                    ],
                ],
                'type' => Type::listOf(GraphQL::type('ContentInterface')),
                'description' => 'Collection of entities',
            ],

        ];
    }


    public function resolveReferencingField($root, $args)
    {
        if ($root instanceof VideoMedia) {
            $entity = $root;
        } else {
            $entity = VideoMedia::where('id', '=', $root['id'])->first();

        }

        $referencing = $entity->referencing(isset($args['collection']) ? $args['collection'] : null);

        return  $referencing->isNotEmpty() ? $referencing : [];
    }

    public function resolveReferencedField($root, $args)
    {
        if ($root instanceof VideoMedia) {
            $entity = $root;
        } else {
            $entity = VideoMedia::where('id', '=', $root['id'])->first();

        }

        $referenced = $entity->referenced(isset($args['collection']) ? $args['collection'] : null);

        return  $referenced->isNotEmpty() ? $referenced : [];
    }

    public function interfaces()
    {
        return [
            GraphQL::type('MediaContentInterface'),
            GraphQL::type('ContentInterface'),
        ];
    }

}

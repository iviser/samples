<?php

namespace Modules\Media\GraphQL\Types\Input;


use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;


class MediaAudioUpdateInput extends GraphQLType
{
    protected $attributes = [
        'name' => 'MediaAudioUpdateInput',
        'description' => 'Media audio update input type'
    ];

    protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Media audio id',
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Media audio title',
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'Media audio body',
            ],
            'album_id' => [
                'type' => Type::int(),
                'description' => 'The album id which this media audio belongs to',
            ],
            'file' => [
                'type' => GraphQL::type('TempFile'),
                'description' => 'Temporary saved file',
            ]
        ];
    }
}

<?php

namespace Modules\Media\GraphQL\Queries;

use App\GraphQL\Queries\AbstractContentCollectionQuery;
use GraphQL;
use League\Fractal\TransformerAbstract;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Repositories\Transformers\MediaImageTransformer;
use App\Repositories\Criteria\GqlContentCollectionsArgs as ArgsCriteria;
use Modules\Media\Repositories\Criteria\MediaFile;
use Modules\Media\Repositories\Criteria\MoreImagesFromGallery;

class ImagesCollectionQuery extends AbstractContentCollectionQuery
{
    protected $attributes = [
        'name' => 'mediaImagesCollection'
    ];

    public function type()
    {
        return GraphQL::type('MediaImagesCollection');
    }

    public function getRepository()
    {
        return app(\Modules\Media\Repositories\ImageMediaRepository::class);
    }

    public function getItemTransformer(): TransformerAbstract
    {
        return new MediaImageTransformer();
    }

    public function resolve($root, $args)
    {
        $repo = $this->getRepository();
        $repo->pushCriteria(new ArgsCriteria(collect($args)));
        $repo->pushCriteria(new MediaFile((new ImageMedia())->getMediaCollectionName()));
        $repo->pushCriteria(new MoreImagesFromGallery());
        $repo->setPresenter($this->getCollectionPresenter());
        $result = $repo->paginateAdvanced(['*'], $args['take'], $args['skip']);

        return $result;

    }


}

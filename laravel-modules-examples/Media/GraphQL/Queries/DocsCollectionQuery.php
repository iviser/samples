<?php

namespace Modules\Media\GraphQL\Queries;

use App\GraphQL\Queries\AbstractContentCollectionQuery;
use GraphQL;
use League\Fractal\TransformerAbstract;
use Modules\Media\Entities\DocMedia;
use Modules\Media\Entities\ImageMedia;
use App\Repositories\Criteria\GqlContentCollectionsArgs as ArgsCriteria;
use Modules\Media\Repositories\Transformers\MediaTypeTransformer;
use Modules\Media\Repositories\Criteria\MediaFile;
use Modules\Media\Repositories\DocMediaRepository;

class DocsCollectionQuery extends AbstractContentCollectionQuery
{
    protected $attributes = [
        'name' => 'mediaDocsCollection'
    ];

    public function type()
    {
        return GraphQL::type('MediaDocsCollection');
    }

    public function getRepository()
    {
        return app(DocMediaRepository::class);
    }

    public function getItemTransformer(): TransformerAbstract
    {
        return new MediaTypeTransformer();
    }

    public function resolve($root, $args)
    {
        $repo = $this->getRepository();
        $repo->pushCriteria(new ArgsCriteria(collect($args)));
        $repo->pushCriteria(new MediaFile((new DocMedia())->getMediaCollectionName()));
        $repo->setPresenter($this->getCollectionPresenter());

        return $repo->paginateAdvanced(['*'], $args['take'], $args['skip']);

    }


}

<?php

namespace Modules\Media\GraphQL\Queries;

use GraphQL;
use Modules\Media\Entities\DocMedia as Entity;
use Modules\Media\Repositories\DocMediaRepository;
use Modules\Media\Repositories\Presenters\MediaTypePresenter;

class MediaDocQuery extends AbstractMediaTypeQuery
{
    protected $attributes = [
        'name' => 'mediaDoc'
    ];

    public function type()
    {
        return GraphQL::type('MediaDoc');
    }

    public function getPresenter()
    {
        return new MediaTypePresenter();
    }

    public function getRepository()
    {
        return app(DocMediaRepository::class);
    }

    public function getMediaCollectionName(): string
    {
        return (new Entity())->getMediaCollectionName();
    }
}

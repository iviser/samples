<?php

namespace Modules\Media\GraphQL\Queries;

use App\GraphQL\Queries\AbstractContentCollectionQuery;
use GraphQL;
use League\Fractal\TransformerAbstract;
use Modules\Media\Entities\AudioMedia;
use App\Repositories\Criteria\GqlContentCollectionsArgs as ArgsCriteria;
use Modules\Media\Repositories\Transformers\MediaTypeTransformer;
use Modules\Media\Repositories\AudioMediaRepository;
use Modules\Media\Repositories\Criteria\MediaFile;

class AudioCollectionQuery extends AbstractContentCollectionQuery
{
    protected $attributes = [
        'name' => 'mediaAudioCollection'
    ];

    public function type()
    {
        return GraphQL::type('MediaAudioCollection');
    }

    public function getRepository()
    {
        return app(AudioMediaRepository::class);
    }

    public function getItemTransformer(): TransformerAbstract
    {
        return new MediaTypeTransformer();
    }

    public function resolve($root, $args)
    {
        $repo = $this->getRepository();
        $repo->pushCriteria(new ArgsCriteria(collect($args)));
        $repo->pushCriteria(new MediaFile((new AudioMedia())->getMediaCollectionName()));
        $repo->setPresenter($this->getCollectionPresenter());

        return $repo->paginateAdvanced(['*'], $args['take'], $args['skip']);
    }


}

<?php

namespace Modules\Media\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\Repositories\MediaRepository;
use Modules\Media\Repositories\Presenters\MediaFilePresenter;

class MediaQuery extends Query
{
    protected $attributes = [
        'name' => 'mediaFile'
    ];

    public function type()
    {
        return GraphQL::type('Media');
    }


    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' =>  Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args)
    {
        $repository = app(MediaRepository::class);
        $results = $repository->setPresenter(new MediaFilePresenter())->find($args['id']);

        return $results;

    }
}

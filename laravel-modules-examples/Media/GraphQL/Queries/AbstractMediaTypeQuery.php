<?php

namespace Modules\Media\GraphQL\Queries;

use App\GraphQL\Queries\ContentItemQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Modules\Media\Repositories\Criteria\MediaFile;

abstract class AbstractMediaTypeQuery extends ContentItemQuery
{

    public function type()
    {
        return GraphQL::type('MediaImage');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' =>  Type::nonNull(Type::int())],
        ];
    }

    abstract function getMediaCollectionName(): string;

    public function resolve($root, $args)
    {
        $results =  $this->getRepository()
            ->with('mediaFile')
            ->pushCriteria(new MediaFile($this->getMediaCollectionName()))
            ->setPresenter($this->getPresenter())
            ->find($args['id']);

        return $results;
    }
}
<?php

namespace Modules\Media\GraphQL\Queries;

use GraphQL;
use Modules\Media\Entities\AudioMedia as Entity;
use Modules\Media\Repositories\AudioMediaRepository;
use Modules\Media\Repositories\Presenters\MediaTypePresenter;

class MediaAudioQuery extends AbstractMediaTypeQuery
{
    protected $attributes = [
        'name' => 'mediaAudio'
    ];

    public function type()
    {
        return GraphQL::type('MediaAudio');
    }

    public function getPresenter()
    {
        return new MediaTypePresenter();
    }

    public function getRepository()
    {
        return app(AudioMediaRepository::class);
    }

    public function getMediaCollectionName(): string
    {
        return (new Entity())->getMediaCollectionName();
    }
}

<?php

namespace Modules\Media\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;

class PropagateMediaToSelectelBatch implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    protected $limit, $offset;

    public $tries = 2;

    /**
     * @var bool
     */
    protected $removeOriginal;


    public function __construct(int $limit, int $offset, $removeOriginal = false)
    {
        $this->limit = $limit;
        $this->offset = $offset;
        $this->removeOriginal = $removeOriginal;
    }

    public function handle()
    {

        $mediaCollection = \App\Entities\Media::where('disk', '=', 'media')
            ->take($this->limit)
            ->skip($this->offset)
            ->get();

        foreach ($mediaCollection as $entity) {
            $job = new PropagateMediaToSelectel($entity->id, $this->removeOriginal);
            dispatch($job->onQueue('media'));
        }
    }
}

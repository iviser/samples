<?php

namespace Modules\Media\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Media\Entities\Interfaces\MediaEntityInterface;
use Modules\Media\Entities\Interfaces\MediaLibraryInterface;

class AttachOriginalMediaFromUrl implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    /**
     * @var MediaEntityInterface
     */
    protected $entity;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $mediaLibrary;

    /**
     * AttachMediaFromUrl constructor.
     * @param MediaLibraryInterface $entity
     * @param string $url
     * @param string $mediaLibrary
     */
    public function __construct(MediaLibraryInterface $entity, string $url, string $mediaLibrary = '')
    {
        $this->entity = $entity;
        $this->url = $url;
        $this->mediaLibrary = $mediaLibrary;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->entity->addMediaFileFromUrl($this->url, $this->mediaLibrary);
    }
}

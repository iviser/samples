<?php

namespace Modules\Media\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Media\Entities\Interfaces\MediaEntityInterface;
use Modules\Media\Entities\Interfaces\MediaLibraryInterface;

class AttachMediaFromUrl implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    /**
     * @var MediaEntityInterface
     */
    protected $entity;

    /**
     * @var string
     */
    protected $url;

    /**
     * AttachMediaFromUrl constructor.
     * @param MediaEntityInterface $entity
     * @param string $url
     */
    public function __construct(MediaLibraryInterface $entity, string $url)
    {
        $this->entity = $entity;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->entity->addMediaFileFromUrl($this->url);
    }
}

<?php

namespace Modules\Media\Jobs;

use App\Entities\Media;
use App\Services\Media\HealthChecker;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use App\Http\Helpers\File;
use Illuminate\Support\Carbon;

class HealthCheckJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var Media
     */
    protected $media;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @param HealthChecker $checker
     *
     * @return void
     */
    public function handle(HealthChecker $checker)
    {
        $this->media = Media::find($this->id);

        $checker->check($this->media);
    }
}

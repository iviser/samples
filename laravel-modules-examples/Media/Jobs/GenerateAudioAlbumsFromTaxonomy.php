<?php

namespace Modules\Media\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Media\Entities\AudioAlbum;
use Modules\Media\Entities\AudioMedia;
use Modules\Taxonomy\Entities\Term;

class GenerateAudioAlbumsFromTaxonomy implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    protected $trackId;

    public $tries = 2;

    public function __construct(int $id)
    {
        $this->trackId = $id;
    }

    public function handle()
    {
        $track = AudioMedia::findOrFail($this->trackId);
        $taxonomy = $track->taxonomy;
        $albumTermId = isset($taxonomy->audio_albums) ? $taxonomy->audio_albums[0] : null;
        if ($albumTermId) {
            $albumTerm = Term::findOrFail($albumTermId);

            $album = AudioAlbum::where('title', '=', $albumTerm->name)->first();

            if (!$album) {
                $album = AudioAlbum::create(['title' => $albumTerm->name]);
            }

            $track->album_id = $album->id;
            $track->save();
        } else {
            throw new \Exception();
        }


    }
}

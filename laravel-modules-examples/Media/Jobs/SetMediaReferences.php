<?php

namespace Modules\Media\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Import\Services\Manager\ImportManager;

class SetMediaReferences implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    protected $type;

    protected $id;

    public function __construct($type, $id)
    {
        $this->type = $type;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $importManger = new ImportManager();
        $classes = $importManger->getClasses();

        $mediaEntity = $this->type::findOrFail($this->id);
        $sourceId = $mediaEntity->metadata->migration->source_id;

        foreach ($classes as $class) {
            $model = $class->getModel();
            if (get_class($model) === $this->type ) {
                $data = $class->getSourceItemData($sourceId);
                // Set references
                $referencesCollection = collect($data->get('references'));
                $mediaEntity->references = $class->references($referencesCollection);

                $mediaEntity->save();
            }
        }

    }


}

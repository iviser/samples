<?php

namespace Modules\Correspondence\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class CorrTypeEnum extends GraphQLType {

    protected $enumObject = true;

    protected $attributes = [
        'name' => 'CorrespondenceType',
        'description' => 'The types of correspondence',
        'values' => [
            'je_white_letters' => 'je_white_letters',
            'correspondence' => 'correspondence',
            'incoming_correspondence' => 'incoming_correspondence',
            'js_white_letters' => 'js_white_letters',
            'email' => 'email'
        ],
    ];
}

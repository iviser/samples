<?php

namespace Modules\Correspondence\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class EsCorrespondence extends GraphQLType
{
    protected $attributes = [
        'name' => 'EsCorrespondence',
        'description' => 'A correspondence'
    ];

    public function fields()
    {
        return [
            '_id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the elasticsearch document which matches original model id'
            ],
            '_type' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The type of the elasticsearch document'
            ],
            '_index' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Index the elasticsearch document belongs to'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of the es correspondence'
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'The body of the es correspondence'
            ],
            'date' => [
                'type' => Type::string(),
                'description' => 'The date of es the correspondence'
            ],
            'type' => [
                'type' => Type::string(),
                'description' => 'Pdf path'
            ],

        ];
    }

    public function interfaces() {
        return [
            GraphQL::type('EsDocument')
        ];
    }

}

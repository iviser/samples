<?php

namespace Modules\Correspondence\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;


class CorrespondenceCollectionType extends GraphQLType
{
    protected $attributes = [
        'name' => 'CorrespondenceCollectionType',
        'description' => 'Collection of correspondence'
    ];

    public function fields()
    {
        return [
            'total' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Total items'
            ],
            'offset' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items offset. How many items skipped'
            ],
            'size' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items size. How many items are taken'
            ],
            'items' => [
                'type' => Type::listOf(\GraphQL::type('Correspondence')),
                'description' => 'Empty interface'
            ],
        ];
    }


    public function interfaces() {
        return [
            GraphQL::type('CollectionInterface')
        ];
    }

}

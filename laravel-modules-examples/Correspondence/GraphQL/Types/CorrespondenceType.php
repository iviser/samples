<?php

namespace Modules\Correspondence\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class CorrespondenceType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Correspondence',
        'description' => 'A correspondence'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the correspondence'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The title of the correspondence'
            ],
            'body' => [
                'type' => Type::string(),
                'description' => 'The body of the correspondence'
            ],
            'text' => [
                'type' => Type::string(),
                'description' => 'The text of the correspondence'
            ],
            'date' => [
                'type' => Type::string(),
                'description' => 'The date of the correspondence'
            ],
            'type' => [
                'type' => GraphQL::type('CorrespondenceType'),
                'description' => 'Correspondence type'
            ],
            'pdf' => [
                'type' => GraphQL::type('Media'),
                'description' => 'Media type'
            ],
            'receivers' => [
                'type' => Type::listOf(GraphQL::type('Person')),
                'description' => 'Correspondence receivers'
            ],
            'receiver_location' => [
                'type' => GraphQL::type('Location'),
                'description' => 'Correspondence receiver location'
            ],
            'senders' => [
                'type' => Type::listOf(GraphQL::type('Person')),
                'description' => 'Correspondence senders'
            ],
            'sender_location' => [
                'type' => GraphQL::type('Location'),
                'description' => 'Correspondence sender location'
            ],
            'is_public' => [
                'type' => Type::string(),
                'description' => ''
            ],
            'typewritten' => [
                'type' => Type::string(),
                'description' => ''
            ],
            'handwritten' => [
                'type' => Type::string(),
                'description' => ''
            ],
            'letterhead' => [
                'type' => Type::string(),
                'description' => ''
            ],
            'thumbnail' => [
                'type' => Type::string(),
                'description' => 'Relative path to the thumbnail'
            ],
            'route' => [
                'type' => Type::string(),
                'description' => 'Relative route to the content'
            ],

        ];
    }

    public function interfaces() {
        return [
            GraphQL::type('ContentInterface')
        ];
    }

}

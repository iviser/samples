<?php

namespace Modules\Correspondence\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;
use Modules\Correspondence\Builder\CorrespondenceCollectionBuilder;

class CorrCollectionSortFields extends GraphQLType {

    protected $enumObject = true;

    protected $attributes = [
        'name' => 'CorrCollectionSortFields',
        'description' => 'Sortable field of the correspondence collection',
    ];

    public function attributes()
    {
        return [
            'values' => CorrespondenceCollectionBuilder::sortableColumns(),
        ];
    }
}

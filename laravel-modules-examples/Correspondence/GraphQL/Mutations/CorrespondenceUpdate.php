<?php

namespace Modules\Correspondence\GraphQL\Mutations;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Modules\Correspondence\Entities\Correspondence;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CorrespondenceUpdate extends Mutation
{

    protected $attributes = [
        'name' => 'correspondenceUpdate'
    ];

    public function type()
    {
        return GraphQL::type('Correspondence');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())],
            'title' => ['name' => 'title', 'type' => Type::string()],
            'body' => ['name' => 'body', 'type' => Type::string()],
            'date' => ['name' => 'date', 'type' => Type::string()],
            'pdf' => ['name' => 'pdf', 'type' => GraphQL::type('TempFile')], // The filename of the temporary pdf file
        ];
    }

    public function resolve($root, $args)
    {
        $entity = Correspondence::find($args['id']);

        if (!$entity) {
            return null;
        }

        if ($args['title']) {
            $entity->title = $args['title'];
        }
        if ($args['body']) {
            $entity->body = $args['body'];
        }
        if ($args['date']) {
            $entity->date = $args['date'];
        }
        if ($args['pdf'] && Storage::disk('temp')->exists($args['pdf']['temp_filename'])) {

            // 1. Check if correspondence has attached pdf file
            if ($pdf = $entity->pdf) {
                // 1.1. If has delete it
                $pdf->delete();
            }
            // 2. Create media file from temporary file
            $filePath = base_path() . Storage::disk('temp')->url($args['pdf']['temp_filename']);
            $file = new UploadedFile($filePath
                ,$args['pdf']['origin_filename']
                ,$args['pdf']['origin_type']
                ,$args['pdf']['origin_size']);

            $entity->addMedia($file)
                ->toMediaCollection(Correspondence::MEDIA_COLLECTION_LETTERS);
            Storage::disk('temp')->delete($args['pdf']['temp_filename']);

            // @todo Process flowpaper files


        }


        $entity->save();

        return $entity;
    }

}
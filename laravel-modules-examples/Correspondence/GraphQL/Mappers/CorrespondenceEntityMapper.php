<?php

namespace Modules\Correspondence\GraphQL\Mappers;


use App\GraphQL\Mappers\EntityMapper;
use Modules\Correspondence\Entities\Correspondence;

class CorrespondenceEntityMapper extends EntityMapper
{

    public function __construct(Correspondence $model)
    {
        parent::__construct($model);
    }

}
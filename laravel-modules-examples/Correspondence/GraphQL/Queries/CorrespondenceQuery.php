<?php

namespace Modules\Correspondence\GraphQL\Queries;

use App\GraphQL\Queries\ContentItemQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Modules\Correspondence\Repositories\CorrespondenceRepository;
use Modules\Correspondence\Repositories\Presenters\CorrespondencePresenter;

class CorrespondenceQuery extends ContentItemQuery
{
    protected $attributes = [
        'name' => 'correspondence'
    ];

    public function type()
    {
        return GraphQL::type('Correspondence');
    }

    public function getRepository()
    {
        return app(CorrespondenceRepository::class);
    }

    public function getPresenter()
    {
        return new CorrespondencePresenter();
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' =>  Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args)
    {
        $repo = $this->getRepository();
        $repo->with([
            'sendersCollection',
            'receiversCollection',
            'senderLocation',
            'receiverLocation',
            'pdf'
        ]);
        $repo->setPresenter($this->getPresenter());


        return $repo->find($args['id']);
    }
}

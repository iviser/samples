<?php

namespace Modules\Correspondence\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

class CorrespondenceListQuery extends Query
{
    protected $attributes = [
        'name' => 'correspondenceList'
    ];

    public function type()
    {
        return GraphQL::type('CorrespondenceCollectionType');
    }

    public function args()
    {
        return [
            // Filter
            'title' => ['name' => 'title', 'type' => Type::string()],
            'start_date' => ['name' => 'start_date', 'type' => Type::string()],
            'end_date' => ['name' => 'end_date', 'type' => Type::string()],
            'receivers_ids' => ['name' => 'receivers_ids', 'type' => Type::listOf(Type::int())],
            'receiver_location_id' => ['name' => 'receiver_location_id', 'type' => Type::int()],
            'senders_ids' => ['name' => 'senders_ids', 'type' => Type::listOf(Type::int())],
            'sender_location_id' => ['name' => 'sender_location_id', 'type' => Type::int()],
            'correspondence_type' => ['name' => 'correspondence_type', 'type' => Type::string()],
            'public' => ['name' => 'public', 'type' => Type::boolean()],
            'handwritten' => ['name' => 'handwritten', 'type' => Type::boolean()],
            'typewritten' => ['name' => 'typewritten', 'type' => Type::boolean()],
            'letterhead' => ['name' => 'letterhead', 'type' => Type::boolean()],

            // Pagination
            'take' => ['name' => 'take', 'type' => Type::int(), 'defaultValue' => 25],
            'skip' => ['name' => 'skip', 'type' => Type::int(), 'defaultValue' => 0],

            // Ordering
            'order_by' => [
                'name' => 'order_by',
                'type' => GraphQL::type('CorrCollectionSortFields'),
                'defaultValue'=> 'id'
            ],
            'order_direction' => [
                'name' => 'order_direction',
                'type' => GraphQL::type('OrderDirection'),
                'defaultValue'=> 'asc'
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $builder = new \Modules\Correspondence\Builder\CorrespondenceCollectionBuilder();
        $builder->setLimitOffset($args['take'], $args['skip']);
        $builder->addFilterData(collect($args));
        $builder->filter();
        $res = $builder->execute();

        return [
            'total' => $builder->total(),
            'offset' => $args['skip'],
            'size' => $args['take'],
            'items' => $res->all(),
        ];
    }
}

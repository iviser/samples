<?php

namespace Modules\Correspondence\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Correspondence\Entities\Correspondence;
use Modules\Media\Entities\DocMedia;

class ConvertEmailsIntoCorr implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    protected $id;

    protected $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->email = DocMedia::findOrFail($this->id);

        $correspondence = Correspondence::create([
            'title' => $this->email->title,
            'type' => 'email',
            'taxonomy' => $this->email->taxonomy,
            'references' => $this->email->references,
            'metadata' => $this->metadata(),
            'body' => $this->email->body,
            'text' => $this->text(),
            'is_public' => true,
        ]);

        // Attach email media files to newly created correspondence

        if ($media = $this->email->getMedia('media_doc')->first()) {
            $media->model_id = $correspondence->id;
            $media->model_type = Correspondence::class;
            $media->save();
        };

        $this->email->delete();
    }

    public function text()
    {
        if (isset($this->email->metadata->migration) &&
            isset($this->email->metadata->migration->document) &&
            isset($this->email->metadata->migration->document->text) &&
            $text = $this->email->metadata->migration->document->text) {
            return $text;
        }

        return null;
    }

    public function metadata()
    {
        $metadata = null;
        if (isset($this->email->metadata->migration) && $emailMeta = $this->email->metadata->migration) {

            $metadata = [
                'migration' => (object) [
                    'old_url' => (isset($emailMeta->old_url)) ? $emailMeta->old_url : null,
                    'summary' => (isset($emailMeta->summary)) ? $emailMeta->summary : null,
                    'source_id' => (isset($emailMeta->source_id)) ? $emailMeta->source_id : null,
                ],
                'is_public' => true,
                'letterhead' => null,
                'handwritten' => false,
                'typewritten' => null,
                'collection' => null,
            ];
        }

        return (object) $metadata;
    }
}

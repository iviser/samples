<?php

namespace Modules\Correspondence\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Correspondence\Entities\Correspondence;

class ImportPdfFilesFtp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $remotePath = '/b_drive/Incoming Correspondence 1833-1915';
    protected $tries = 1;
    /**
     * Create a new job instance.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // 1. Find correspondence
        $entity = \Modules\Correspondence\Entities\Correspondence::find($this->id);

        if (!$entity) {
            return;
        }

        // Build hypothetical file path
        $dirName = basename($entity->title, ".pdf");
        $path = "{$this->remotePath}/$dirName/$entity->title";

        // 2. Check if the correspondence has pdf on ftp server
        //Returns an array of filenames from the specified directory on success or FALSE on error.
        $ftp = \Storage::createFtpDriver([
            'host'     => '173.73.237.5',
            'username' => 'sergei',
            'password' => '9028hughjwaq',
            'port'     => '2229', // your ftp port
            'timeout'  => '600', // timeout setting
        ]);

        // Check if the file exists
        if ($ftp->exists($path)) {
            $filecontent = $ftp->get($path);

            \Storage::disk('temp')->put($entity->title, $filecontent);

            $newFilPath = \Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix() . '/' . $entity->title;

            $entity->addMedia($newFilPath)
                   ->toMediaCollection(\Modules\Correspondence\Entities\Correspondence::MEDIA_COLLECTION_LETTERS);

            $entity->load('pdf');
            $pdfLocal = $entity->pdf;

            // Dispatch the job moving file to selectel
            dispatch(new \Modules\Media\Jobs\PropagateMediaToSelectel($pdfLocal->id, true));

            \Artisan::call('medialibrary:regenerate', [
                'modelType' => Correspondence::class,
                '--ids' => [$entity->id]
            ]);
        }
    }

}

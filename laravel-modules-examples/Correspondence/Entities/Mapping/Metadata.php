<?php
/**
 * Created by PhpStorm.
 * User: iser
 * Date: 19.05.17
 * Time: 15:40
 */

namespace Modules\Correspondence\Entities\Mapping;


use Illuminate\Support\Collection;

class Metadata
{
    public $migration;
    public $is_public;
    public $group_id;
    public $identifier;
    public $web_sort;
    public $letterhead;
    public $handwritten;
    public $typewritten;
    public $correspondence_id;
    public $lb_ref_num;
    public $meta;
    public $file_loc;
    public $collection;
    public $published;

    public function __construct(Collection $data)
    {
        $this->migration = new \stdClass();
        $this->migration->source_id = $data->get('source_id');
        $this->migration->old_url = $data->get('old_url');

        $this->is_public = $data->get('is_public');
        $this->group_id = $data->get('group_id');
        $this->identifier = $data->get('identifier');
        $this->web_sort = $data->get('web_sort');
        $this->letterhead = $data->get('letterhead');
        $this->handwritten = $data->get('handwritten');
        $this->typewritten = $data->get('typewritten');
        $this->correspondence_id = $data->get('correspondence_id');
        $this->lb_ref_num = $data->get('lb_ref_num');
        $this->meta = $data->get('meta');
        $this->file_loc = $data->get('file_loc');
        $this->collection = $data->get('collection');
        $this->published = $data->get('published');

    }
}
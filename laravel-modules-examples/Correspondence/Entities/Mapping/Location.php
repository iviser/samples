<?php
/**
 * Created by PhpStorm.
 * User: iser
 * Date: 19.05.17
 * Time: 15:21
 */

namespace Modules\Correspondence\Entities\Mapping;


use Illuminate\Support\Collection;

class Location
{
    public $address;
    public $city;
    public $country;
    public $county;
    public $location;
    public $state;

    public function __construct(Collection $data)
    {
        $this->address = $data->get('address');
        $this->city = $data->get('city');
        $this->country = $data->get('country');
        $this->county = $data->get('county');
        $this->location = $data->get('location');
        $this->state = $data->get('state');
    }
}
<?php

namespace Modules\Correspondence\Entities\Mapping;


use Illuminate\Support\Collection;

class Person
{
    /**
     * Reference to the existing person entry id
     * @var integer
     */
    public $model_id;
    /**
     * Reference to the existing person entry model type (namespace)
     * @var string
     */
    public $model_type;

    /**
     * Display name of the person which suppose to be shown to users
     * @var string
     */
    public $display;

    /**
     * First name of the person
     * @var string
     */
    public $first_name;

    /**
     * First name of the person
     * @var string
     */
    public $last_name;

    /**
     * Middle name of the person
     * @var string
     */
    public $middle_name;

    /**
     * Full name of the person
     * @var string
     */
    public $full_name;

    /**
     * Generic name of the person
     * @var string
     */
    public $generic_name;

    /**
     * Maiden name of the person
     * @var string
     */
    public $maiden_name;

    /**
     * Salutation name of the person. F.e. mrs. | mr.
     * @var string
     */
    public $salutation;

    /**
     * Maiden name of the person
     * @var string
     */
    public $search;

    public function __construct(Collection $data) {
        $this->display = $data->get('display');
        $this->first_name = $data->get('first_name');
        $this->full_name = $data->get('full_name');
        $this->generic_name = $data->get('generic_name');
        $this->last_name = $data->get('last_name');
        $this->maiden_name = $data->get('maiden_name');
        $this->middle_name = $data->get('middle_name');
        $this->salutation = $data->get('salutation');
        $this->search = $data->get('search');
        $this->institution = $data->get('institution');
        $this->position = $data->get('position');
    }

}
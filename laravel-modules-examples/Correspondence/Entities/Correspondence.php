<?php

namespace Modules\Correspondence\Entities;

use App\Eloquent\Relations\AdvancedRelations;
use App\Eloquent\Relations\BelongsToManyViaArray;
use App\Entities\Interfaces\GqlEntity;
use App\Entities\Traits\EsSearchable;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Interfaces\ContentInterface;
use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\ContentTrait;
use App\Entities\Traits\References;
use Modules\Common\Entities\Traits\ThumbnailTrait;
use Modules\Correspondence\GraphQL\Types\CorrespondenceType;
use Modules\Location\Entities\Location;
use Modules\Media\Entities\MediaTrait;
use Modules\People\Entities\Person;
use Modules\Taxonomy\Entities\Traits\TaxonomyTrait;
use Nwidart\Modules\Collection;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use Spatie\MediaLibrary\Media;
use Modules\Correspondence\Es\Documents\Correspondence as EsDoc;

class Correspondence extends Model implements
    HasMediaConversions,
    EntityThumbInterface,
    ContentInterface,
    GqlEntity,
    ReferenceInterface
{
    const MEDIA_COLLECTION_LETTERS = 'letter';

    use MediaTrait, ThumbnailTrait, ContentTrait, AdvancedRelations, References, EsSearchable, TaxonomyTrait;

    protected $mappingName = 'correspondence';

    protected $contentTypeName = 'correspondence';

    protected $fillable = [
        'title',
        'body',
        'senders',
        'receivers',
        'type',
        'text',
        'metadata',
        'date',
        'sender_location',
        'receiver_location'
    ];

    protected $casts = [
        'senders' => 'array',
        'receivers' => 'array',
        'metadata' => 'object',
    ];

    protected $table = 'content_correspondence';

    protected $routeName = 'correspondence.item';

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->sharpen(10);
    }

    /**
     * Get receivers of the correspondence
     *
     * @return BelongsToManyViaArray
     */
    public function receiversCollection()
    {
        return $this->BelongsToManyViaArray(Person::class, 'receivers')
            ->withDefault(function () {
                return new Collection();
            });
    }

    /**
     * Get senders of the correspondence
     *
     * @return BelongsToManyViaArray
     */
    public function sendersCollection()
    {
        return $this->BelongsToManyViaArray(Person::class, 'senders')
            ->withDefault(function () {
                return new Collection();
            });
    }

    /**
     * Get sender location
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function senderLocation()
    {
        return $this->hasOne(Location::class, 'id', 'sender_location');
    }

    /**
     * Get receiver location
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function receiverLocation()
    {
        return $this->hasOne(Location::class, 'id', 'receiver_location');
    }


    /**
     * Get pdf file relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function pdf()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
            ->where('collection_name', '=', self::MEDIA_COLLECTION_LETTERS);
    }

    public function getIsPublicAttribute()
    {
        return $this->metadata && isset($this->metadata->is_public) ? $this->metadata->is_public : null;
    }

    public function getTypewrittenAttribute()
    {
        return $this->metadata && isset($this->metadata->typewritten) ? $this->metadata->typewritten : null;
    }

    public function getHandwrittenAttribute()
    {
        return $this->metadata && isset($this->metadata->handwritten) ? $this->metadata->handwritten : null;
    }

    public function getLetterheadAttribute()
    {
        return $this->metadata && isset($this->metadata->letterhead) ? $this->metadata->letterhead : null;
    }

    /**
     * Get gql type of entity
     * @return string
     */
    public function getGqlType(): string
    {
        return (new CorrespondenceType())->name;
    }

    public function toSearchableArray()
    {
        return (new EsDoc($this))->source();
    }
}

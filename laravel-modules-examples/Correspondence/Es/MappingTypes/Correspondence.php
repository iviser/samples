<?php

namespace Modules\Correspondence\Es\MappingTypes;

use Modules\Location\Es\MappingTypes\EsLocationMapping;
use Modules\People\Es\MappingTypes\Person as EsPerson;
use App\Es\MappingTypes\DefaultMapping;
use Phirames\LaraElastic\MappingTypes\MappingTypeInterface;

class Correspondence implements MappingTypeInterface
{

    public static function name(): string
    {
        return 'correspondence';
    }

    public static function properties(): array
    {
        $mapping = [
            'is_public' => ['type' => 'boolean'],
            'letterhead' => ['type' => 'boolean'],
            'handwritten' => ['type' => 'boolean'],
            'typewritten' => ['type' => 'boolean'],

            'senders' => ['type' => 'object', 'properties' => EsPerson::properties()],
            'sender_location' => ['type' => 'object', 'properties' => EsLocationMapping::properties()],
            'receivers' =>  ['type' => 'object', 'properties' => EsPerson::properties()],
            'receiver_location' => ['type' => 'object', 'properties' => EsLocationMapping::properties()],

            'correspondence_type' => ['type' => 'keyword'],

            'correspondence_metadata' => ['type' => 'nested'],
        ];

        $mapping['senders']['properties']['title'] = DefaultMapping::fieldTitle();
        $mapping['sender_location']['properties']['title'] = DefaultMapping::fieldTitle();
        $mapping['receiver']['properties']['title'] = DefaultMapping::fieldTitle();
        $mapping['receiver_location']['properties']['title'] = DefaultMapping::fieldTitle();

        return $mapping;

    }
}

<?php

namespace Modules\Correspondence\Es\Documents;


use App\Es\Documents\EgwEloquentDocModel;
use Modules\Correspondence\Entities\Correspondence as CorrespondenceEntity;
use Modules\Search\Search\Mappings\PdfDocMediaResource;
use Modules\Search\Search\Mappings\Snippet;

class Correspondence extends EgwEloquentDocModel
{
    public function __construct(CorrespondenceEntity $modelEntry)
    {
        parent::__construct($modelEntry);
    }

    public static function modelType(): string
    {
        return CorrespondenceEntity::class;
    }

    public static function mappingType(): string
    {
        return \Modules\Correspondence\Es\MappingTypes\Correspondence::name();
    }

    public function source(): array
    {
        return [

            'title' => $this->modelEntry->title,
            'suggest' => $this->modelEntry->title,

            'body' => $this->modelEntry->body,

            'date' => $this->modelEntry->date ? $this->modelEntry->date: null,

            'snippet' => $this->snippet(),

            'resources' => $this->resources(),
            'media_resources' => $this->mediaResources(),

            'senders' => $this->modelEntry->sendersCollection->toArray(),
            'sender_location' => $this->modelEntry->senderLocation,
            'receivers' => $this->modelEntry->receiversCollection->toArray(),
            'receiver_location' => $this->modelEntry->receiverLocation,
            'correspondence_type' => $this->modelEntry->type,

            'is_public' => isset($this->modelEntry->metadata->is_public) ? $this->modelEntry->metadata->is_public : null,
            'letterhead' => isset($this->modelEntry->metadata->letterhead) ? $this->modelEntry->metadata->letterhead : null,
            'handwritten' => isset($this->modelEntry->metadata->handwritten) ? $this->modelEntry->metadata->handwritten : null,
            'typewritten' => isset($this->modelEntry->metadata->typewritten) ? $this->modelEntry->metadata->typewritten : null,
            'collection' => isset($this->modelEntry->metadata->collection) ? $this->modelEntry->metadata->collection : null,

            'created_at' => $this->modelEntry->created_at ? $this->modelEntry->created_at->toDateTimeString() : null,
            'updated_at' => $this->modelEntry->updated_at ? $this->modelEntry->updated_at->toDateTimeString() : null,
            'deleted_at' => $this->modelEntry->deleted_at ? $this->modelEntry->deleted_at->toDateTimeString() : null,
        ];

    }

    protected function resources(): array
    {
        $resources = [];

        if ($this->modelEntry->pdf) {
            $resources['pdf'] = str_replace(base_path('storage'), '',  $this->modelEntry->pdf->getPath());
        }

        return $resources;

    }

    protected function mediaResources(): array
    {
        $mediaResources = [];

        if ($this->modelEntry->pdf) {
            $path = str_replace(base_path('storage'), '',  $this->modelEntry->pdf->getPath());
            $mediaResources[] = new PdfDocMediaResource($this->modelEntry->pdf->name, $path);
        }


        return $mediaResources;
    }

    /**
     * Create search snippet
     */
    protected function snippet(): Snippet
    {
        $title = $this->modelEntry->title;
        $body = str_limit(strip_tags($this->modelEntry->body), 255);
        $url = route('correspondence.item', ['id' => $this->modelEntry->  id], false);
        $thumbUrl = $this->modelEntry->thumb_site_path;
        $type = 'Correspondence';


        $snippet = new Snippet($title, $body, $url, $thumbUrl, $type);

        return $snippet;
    }
}

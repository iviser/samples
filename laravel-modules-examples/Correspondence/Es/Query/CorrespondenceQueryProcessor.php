<?php


namespace Modules\Correspondence\Es\Query;


use Modules\Search\Search\Query\QueryProcessor;
use Modules\Search\Search\Query\Query;
use Illuminate\Support\Collection;


/**
 * Class CorrespondenceQueryProcessor
 * @package Modules\Correspondence\Search
 */
class CorrespondenceQueryProcessor extends QueryProcessor
{

    protected $specificFilterSection = 'correspondence';

    protected $specificFilters;

    protected $body;


    public function __construct(Query $query, Collection $source)
    {
        parent::__construct($query, $source);

        $this->body = $this->query->getBody();
        $this->specificFilters = $this->getSpecificFilters();
    }

    /**
     * Processes query taking parameters from source
     */
    public function process()
    {
        $fields = [
            'senders.full_name',
            'sender_location.location',
            'receivers.full_name',
            'receiver_location.location',
        ];
        $this->query->addSearchFields($fields);
        // Set corr specific bool filters
        $this->setBoolFilterIfExist('is_public');
        $this->setBoolFilterIfExist('handwritten');
        $this->setBoolFilterIfExist('typewritten');
        $this->setBoolFilterIfExist('letterhead');
        // Set corr specific string multiple term filters
        $this->setStrMultiTermFilter('senders');
        $this->setStrMultiTermFilter('sender_location');
        $this->setStrMultiTermFilter('receivers');
        $this->setStrMultiTermFilter('receiver_location');
        if (isset($this->specificFilters->correspondence_type) && $this->specificFilters->correspondence_type->value) {
            $this->body->addTermsFilter('correspondence_type', $this->specificFilters->correspondence_type->value);
        }

        $this->addAggregations();


    }

    protected function getSpecificFilters()
    {
        if ($filters = $this->getFilters()) {
            if(isset($filters->types) &&
                isset($filters->types->correspondence) &&
                isset($filters->types->correspondence->controllers)) {

                return $filters->types->correspondence->controllers;
            }
        }

        return null;
    }

    protected function addSpecificTermFilter(string $filterName)
    {
        if (is_object($this->specificFilters) && isset($this->specificFilters->$filterName)) {
            // Add term filter in the query only if term is true
            if ($this->specificFilters->$filterName) {
                $this->query->addTermFilter($filterName, (bool) $this->specificFilters->$filterName);
            }
        }

    }

    protected function addAggregations()
    {
        //@todo add aggregations only if needed
        $this->body->addTermsAggregation('senders', 'senders.title.keyword', 10);
        $this->body->addTermsAggregation('sender_location', 'sender_location.title.keyword', 10);
        $this->body->addTermsAggregation('receivers', 'receivers.title.keyword', 10);
        $this->body->addTermsAggregation('receiver_location', 'receiver_location.title.keyword', 10);
        $this->body->addTermsAggregation('corr_type', 'correspondence_type', 10);
    }

    protected function setStrMultiTermFilter(string $name)
    {
        if (isset($this->specificFilters->$name) && $this->specificFilters->$name->value) {
            $this->body->addTermsFilter($name . '.title.keyword', $this->specificFilters->$name->value);
        }
    }

    protected function setBoolFilterIfExist(string $name)
    {
        if (isset($this->specificFilters->$name) && $this->specificFilters->$name->value !== "null") {
            $this->body->addTermFilter($name, $this->specificFilters->$name->value);
        }
    }

}

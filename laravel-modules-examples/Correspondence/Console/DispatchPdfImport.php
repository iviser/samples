<?php

namespace Modules\Correspondence\Console;

use Illuminate\Console\Command;
use Modules\Correspondence\Entities\Correspondence;
use Modules\Correspondence\Import\Batches\IncomingCorrespondenceBatch;
use Modules\Correspondence\Jobs\CorrespondenceBulkImport;

class DispatchPdfImport extends Command
{

    protected $config;

    protected $table;

    protected $chunkSize = 50;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'correspondence:import-ftp-pdf {id?} {--failed}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import pdf files from ftp server and attaches it to correspondence';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if ($id = $this->argument('id')) {
            $entity = Correspondence::findOrFail((int) $id);
            dispatch(new \Modules\Correspondence\Jobs\ImportPdfFilesFtp($entity->id))->onQueue('pdfImport');
        } else {

            if ($this->option('failed')) {
                $entities = \DB::select(\DB::raw("SELECT c.id
                    FROM \"content_correspondence\" as c
                    JOIN  media ON media.model_id = c.id
                    LEFT JOIN (
                    SELECT DISTINCT ON (id) 
                    id, 
                    (json_array_elements(custom_properties -> 'health' -> 'file_status'))->>'status' as status,
                    to_timestamp((json_array_elements(custom_properties -> 'health' -> 'file_status'))->>'date', 'YYYY-MM-DD HH24:MI:SS')as date
                    FROM media
                    ORDER BY id, date  DESC
                    ) as h ON h.id = media.id
                    WHERE c.\"created_at\" > '01 02 2018' AND \"type\" = 'incoming_correspondence' AND h.status = 'failed' 
                ") );

            } else {
                $entities = Correspondence::where('title', 'iLike', '%.pdf')
                                          ->where('created_at', '>', '2018-02-01')
                                          ->has('pdf', '<', 1)
                                          ->get(['id']);

            }

            foreach ($entities as $entity) {
                dispatch(new \Modules\Correspondence\Jobs\ImportPdfFilesFtp($entity->id))->onQueue('pdfImport');
            }
            $this->info('Ok');

        }


    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}

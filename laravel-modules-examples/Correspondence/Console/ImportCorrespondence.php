<?php

namespace Modules\Correspondence\Console;

use Illuminate\Console\Command;
use Modules\Correspondence\Import\Batches\IncomingCorrespondenceBatch;
use Modules\Correspondence\Jobs\CorrespondenceBulkImport;

class ImportCorrespondence extends Command
{

    protected $config;

    protected $table;

    protected $chunkSize = 50;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'correspondence:import-07_02_18 
        {--driver=pgsql} 
        {--host=postgres} 
        {--port=5432} 
        {--db_name=incor} 
        {--db_username=admin} 
        {--db_password=admin} 
        {--table=incoming_correspondence}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->config = [
            'driver' => $this->option('driver'),
            'host' => $this->option('host'),
            'port' => $this->option('port'),
            'database' => $this->option('db_name'),
            'username' => $this->option('db_username'),
            'password' => $this->option('db_password'),
            'schema' => 'public',
        ];

        $this->table = $this->option('table');

        $batch = new IncomingCorrespondenceBatch($this->config, $this->table);

        $count = $batch->getSource()->count();

        if ($count > $this->chunkSize) {

            $chunks = (int) ($count / $this->chunkSize);
            $reminder = $count % $this->chunkSize;

            for ($i = 0 ; $i <= $chunks; $i++) {

                if ($i === $chunks) {
                    $offset = $i * $this->chunkSize;
                    $limit = $reminder;
                } else {
                    $offset = $i * $this->chunkSize;
                    $limit = $this->chunkSize;
                }

                $job = new CorrespondenceBulkImport($this->config, $this->table, $limit, $offset);
                dispatch($job->onQueue('import'));
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}

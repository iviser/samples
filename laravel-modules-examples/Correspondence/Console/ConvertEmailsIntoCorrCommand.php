<?php

namespace Modules\Correspondence\Console;

use Illuminate\Console\Command;
use Modules\Correspondence\Jobs\ConvertEmailsIntoCorr;
use Modules\Media\Entities\DocMedia;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ConvertEmailsIntoCorrCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'correspondence:convert-email-into-corr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = \DB::table('media_doc')->select(\DB::raw('distinct(id)'))
            ->where(\DB::raw('taxonomy -> \'file_category\'  @> \'427\' OR
taxonomy -> \'file_collection\' @> \'81\''))->get();
        foreach ($emails as $email) {
            $job = new ConvertEmailsIntoCorr($email->id);
            $job->handle();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}

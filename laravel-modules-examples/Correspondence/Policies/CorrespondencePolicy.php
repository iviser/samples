<?php

namespace Modules\Correspondence\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Correspondence\Entities\Correspondence;

class CorrespondencePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the people.
     *
     * @param  \App\User $user
     * @param Correspondence $correspondence
     * @return mixed
     */
    public function view(User $user, Correspondence $correspondence)
    {
        //
    }

    /**
     * Determine whether the user can create people.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the people.
     *
     * @param  \App\User $user
     * @param Correspondence $correspondence
     * @return mixed
     */
    public function update(User $user, Correspondence $correspondence)
    {
        return $user->hasAnyRole(['super_admin', 'admin', 'editor']);
    }

    /**
     * Determine whether the user can delete the people.
     *
     * @param  \App\User $user
     * @param Correspondence $correspondence
     * @return mixed
     */
    public function delete(User $user, Correspondence $correspondence)
    {
        return $user->hasAnyRole(['super_admin', 'admin', 'editor']);
    }
}

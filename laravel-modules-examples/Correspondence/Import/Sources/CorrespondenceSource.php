<?php

namespace Modules\Correspondence\Import\Sources;


use Illuminate\Support\Collection;
use Modules\Common\Import\Sources\AbstractDBDbSource;

class CorrespondenceSource extends AbstractDBDbSource
{

    /**
     * Get total amount items
     *
     * @return int
     */
    public function count(): int
    {
        return $this->query()->count();
    }

    /**
     * Get single item
     *
     * @param $identifier;
     * @return mixed
     */
    public function item($identifier)
    {
        return $this->query()->where('id', '=', $identifier)->first();
    }

    /**
     * Get batch
     *
     * @param int $limit
     * @param int $offset
     *
     * @return mixed
     */
    public function batch(int $limit = 25, int $offset = 0): Collection
    {
        return $this->query()->take($limit)->skip($offset)->get();
    }
}
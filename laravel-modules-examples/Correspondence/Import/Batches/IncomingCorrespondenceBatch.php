<?php

namespace Modules\Correspondence\Import\Batches;

use Modules\Common\Import\Batches\AbstractBatch;
use Modules\Common\Import\Mappers\MapperInterface;
use Modules\Common\Import\Sources\DbSourceInterface;
use Modules\Correspondence\Entities\Correspondence;
use Modules\Correspondence\Import\Mappers\IncomingCorrespondenceMapper;
use Modules\Correspondence\Import\Sources\CorrespondenceSource;

class IncomingCorrespondenceBatch extends AbstractBatch
{

    protected $config;

    protected $table;

    public function __construct(array $config, $table)
    {
        $this->config = $config;

        $this->table = $table;
    }

    public function mapper(): MapperInterface
    {
        return new IncomingCorrespondenceMapper();
    }

    public function source(): DbSourceInterface
    {
        return new CorrespondenceSource($this->config, $this->table);
    }

    public function importBulk(int $limit, int $offset)
    {
        $mappedItems = $this->getSource()->batch($limit, $offset);

        foreach ($mappedItems as $item) {
            Correspondence::create($this->getMapper()->map((array) $item));
        }
    }

    public function importItem(int $id)
    {
        $item = $this->getSource()->item($id);

        if ($item) {
            $mapped = $this->getMapper()->map((array) $item);
            Correspondence::created($mapped);
        }
    }

}

<?php

namespace Modules\Correspondence\Import\Mappers;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\Common\Import\Mappers\MapperInterface;
use Modules\Location\Entities\Location;
use Modules\People\Entities\Person;

class IncomingCorrespondenceMapper implements MapperInterface
{
    public function map($data)
    {
        $data = collect($data);
        $senderLocation = $this->senderLocation($data);
        $receiverLocation = $this->receiverLocation($data);

        return [
            'title'             => $data->get('filename'),
            'body'              => $data->get('notes'),
            'date'              => $this->date($data->get('date')),
            'text'              => $data->get('fulltext'),
            'metadata'          => $this->mapMetadata($data),
            'senders'           => $this->senders($data),
            'receivers'         => $this->receivers($data),
            'receiver_location' => $receiverLocation ? ((int) $receiverLocation->id) : null,
            'sender_location'   => $senderLocation ? ((int) $senderLocation->id) : null,
            'type'              => 'incoming_correspondence',
        ];
    }

    public function date(string $date = null)
    {
        if (strlen($date) > 4) {
            $date = Carbon::parse($date);
            return $date->toDateTimeString();
        }

        return null;
    }

    public function mapMetadata(Collection $data)
    {
        $metadata = [
            'meta'              => $data->get('allmetadata'),
            'file_loc'          => null,
            'group_id'          => $data->get('groupid'),
            'websort'           => $data->get('websort'),
            'is_public'         => $this->isPublic($data),
            'published'         => null,
            'migration'         => new \stdClass(),
            'collection'        => null,
            'lb_ref_num'        => $data->get('lbrefnum'),
            'letterhead'        => $data->get('letterhead') ? true : null,
            'handwritten'       => $data->get('handwritten') ? true : null,
            'typewritten'       => $data->get('typewritten') ? true : null,
            'correspondence_id' => $data->get('correspondenceid'),
            'source_id'         => $data->get('id'),
            'source_old_id'     => $data->get('old_id'),
            'migration_date'    => '07_02_2018',
        ];

        return (object) $metadata;
    }

    protected function isPublic(Collection $data)
    {
        $result = null;

        if ($data->get('ispublic') === 'Y') {
            $result = true;

        } elseif ($data->get('ispublic') === 'N') {
            $result = true;
        }
        return $result;
    }

    protected function senders(Collection $data)
    {
        $fields = ['author', 'author2', 'author3', 'author4'];

        $senders = [];

        foreach ($fields as $field) {
            if ($person = $this->getPerson($data->get($field))) {
                $senders[] = (int) $person->id;
            }
        }

        return $senders ?: null;
    }

    protected function receivers(Collection $data)
    {

        $fields = ['receiver', 'receiver2', 'receiver3', 'receiver4'];

        $receivers = [];

        foreach ($fields as $field) {
            if ($person = $this->getPerson($data->get($field))) {
                $receivers[] = (int) $person->id;
            }
        }

        return $receivers ?: null;
    }



    protected function getPerson(string $title)
    {
        $person = null;

        if (strlen($title) > 3) {
            $person = Person::where('title', '=', $title)->first();
            if (!$person) {
                $name = [
                    'last_name' => null,
                    'first_name' => null,
                    'salutation' => null,
                    'maiden_name' => null,
                    'middle_name' => null,
                    'generic_name' => null,
                ];
                Person::create([
                    'title' => $title,
                    'name' => (object) $name,
                ]);
            }
        }

        return $person;
    }

    protected function senderLocation(Collection $data)
    {
        $location = null;

        $title = $data->get('authorlocationsearch');

        // Prepare location address
        $address = [
            'city' => $data->get('authorcity', null),
            'address' => $data->get('authoraddress', null),
            'country' => $data->get('authorcountry', null),
            'county' => $data->get('authorcounty', null),
            'state' => $data->get('authorstate', null),
        ];

        if (is_string($title) && strlen($title) > 2) {
            $location = Location::where('title', '=', $title)->first();
            if (!$location) {
                Location::create([
                    'title' => $title,
                    'address' => $address,
                ]);
            }
        }

        return $location;
    }

    protected function receiverLocation(Collection $data)
    {
        $location = null;


        // Prepare location address
        $address = [
            'address' => $data->get('receiveraddress', null),
            'city' => $data->get('receivercity', null),
            'country' => $data->get('receivercountry', null),
            'county' => $data->get('receivercounty', null),
            'state' => $data->get('receiverstate', null),
        ];

        $address = array_filter($address);

        $title = implode($address, ', ');

        if ($title && is_string($title) && strlen($title) > 2) {
            $location = Location::where('title', '=', $title)->first();
            if (!$location) {
                Location::create([
                    'title' => $title,
                    'address' => $address,
                ]);
            }
        }

        return $location;
    }

}

<?php

namespace Modules\Correspondence\Tests\Api;

use App\Services\Registries\EntityRegistry;
use App\User;
use Illuminate\Http\UploadedFile;
use Modules\Correspondence\Entities\Correspondence;
use Tests\CreatesApplication;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Tests\Traits\TestReferenceTrait;
use Tests\Traits\TestTermsReferenceTrait;

class CorrespondenceApiCrudTest extends TestCase
{
    use CreatesApplication, TestReferenceTrait, TestTermsReferenceTrait;

    public function setUp()
    {
        parent::setUp();
        $this->crateDbTables();
        $this->defineFactories();
        Artisan::call('module:migrate', [
            'module' => 'Correspondence'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'People'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'Location'
        ]);
        Artisan::call('module:migrate', [
            'module' => 'Taxonomy'
        ]);
    }

    /**
     * @param User $user
     * @return Correspondence
     * @throws \Exception
     */
    public function createCorrespondence(User $user): Correspondence
    {
        $filename1 = "{$this->faker->md5}.pdf";

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'pdf' => UploadedFile::fake()->image($filename1),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/correspondence/",
            $payload,
            $this->getUserHeaders($user)
        )
            ->decodeResponseJson();

        $correspondence = Correspondence::find($resp['id']);

        $correspondence->load(['pdf']);

        return $correspondence;
    }

    /**
     * Create correspondence correctly.
     *
     * @return void
     */
    public function testCorrespondenceIsCreatedCorrectly()
    {
        $user = $this->createUser();
        $filename1 = "{$this->faker->md5}.pdf";

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'type' => $this->faker->word,
            'date' => $this->faker->date("Y-m-d"),
            'pdf' => UploadedFile::fake()->image($filename1),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/correspondence?with[]=pdf",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
                'text' => $payload['text'],
                'type' => $payload['type'],
                'pdf' => [
                    'file_name' => $payload['pdf']->name
                ],
            ]);
    }

    public function testCorrespondenceIsCreatedWithReferencesCorrectly()
    {
        $user = $this->createUser();

        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 5)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'type' => $this->faker->word,
            'date' => $this->faker->date("Y-m-d"),
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/correspondence?with[]=references",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
                'text' => $payload['text'],
                'type' => $payload['type'],
            ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    public function testCorrespondenceIsCreatedWithTermsCorrectly()
    {
        $user = $this->createUser();

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'text' => $this->faker->text,
            'type' => $this->faker->word,
            'date' => $this->faker->date("Y-m-d"),
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/correspondence?with[]=terms",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(201)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
                'text' => $payload['text'],
                'type' => $payload['type'],
            ])
            ->assertJsonCount(count($terms), 'terms');
    }

    /**
     * Update correspondence correctly.
     *
     * @return void
     */
    public function testCorrespondenceIsUpdatedCorrectly()
    {
        $user = $this->createUser();
        $correspondence = $this->createCorrespondence($user);

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'text' => $this->faker->text,
            'type' => $this->faker->word,
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/correspondence/{$correspondence->id}",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
                'text' => $payload['text'],
                'type' => $payload['type'],
            ]);
    }

    public function testCorrespondenceIsUpdatedWithReferencesCorrectly()
    {
        $user = $this->createUser();
        $correspondence = $this->createCorrespondence($user);
        $this->app->make(EntityRegistry::class)->add(ReferencedModelA::class, 'referenced_model_a');

        $referencedACollection = factory(ReferencedModelA::class, 8)->create();

        $items = $referencedACollection->map(function ($referencedModel) {
            return [
                'id' => $referencedModel->id,
                'type' => 'referenced_model_a'
            ];
        });

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'text' => $this->faker->text,
            'type' => $this->faker->word,
            'references' => json_encode([
                'items' => $items->toArray()
            ]),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/correspondence/{$correspondence->id}?with[]=references",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
                'text' => $payload['text'],
                'type' => $payload['type'],
            ])
            ->assertJsonCount($items->count(), 'references')
            ->assertJsonCount($items->count(), 'loadedReferences');
    }

    public function testCorrespondenceIsUpdatedWithTermsCorrectly()
    {
        $user = $this->createUser();
        $correspondence = $this->createCorrespondence($user);

        $terms = [
            $this->createTerm()->id,
            $this->createTerm()->id,
        ];

        $payload = [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'date' => $this->faker->date(),
            'text' => $this->faker->text,
            'type' => $this->faker->word,
            'terms' => json_encode($terms),
        ];

        // Perform a request
        $resp = $this->json(
            'POST',
            "http://localhost/api/v1/correspondence/{$correspondence->id}?with[]=terms",
            $payload,
            $this->getUserHeaders($user)
        );

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'body' => $payload['body'],
                'date' => $payload['date'],
                'text' => $payload['text'],
                'type' => $payload['type'],
            ])
            ->assertJsonCount(count($terms), 'terms');
    }

    public function testCorrespondenceIsDeletedCorrectly()
    {
        $user = $this->createUser();
        $correspondence = $this->createCorrespondence($user);

        $payload = [];

        // Perform a request
        $this->json(
            'DELETE',
            "http://localhost/api/v1/correspondence/{$correspondence->id}",
            $payload,
            $this->getUserHeaders($user)
        )
            ->assertStatus(204);
    }
}

<?php

namespace Modules\Correspondence\Repositories\Presenters;

use Modules\Correspondence\Repositories\Transformers\CorrespondenceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class CorrespondencePresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CorrespondenceTransformer();
    }
}

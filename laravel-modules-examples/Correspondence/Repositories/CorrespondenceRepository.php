<?php

namespace Modules\Correspondence\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Correspondence\Entities\Correspondence;

class CorrespondenceRepository extends BaseAppRepository
{
    public function model()
    {
        return Correspondence::class;
    }
}
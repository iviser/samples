<?php

namespace Modules\Correspondence\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use App\Repositories\Transformers\Traits\EloquentRelations;
use App\Repositories\Transformers\Traits\ReferenceTransformerTrait;
use App\Repositories\Transformers\Traits\TermTransformerTrait;
use Modules\Correspondence\Entities\Correspondence as Entity;
use Modules\Location\Repositories\Transformers\LocationTransformer;
use Modules\Media\Repositories\Transformers\MediaFileTransformer;
use Modules\People\Repositories\Transformers\PersonTransformer;

/**
 * Class CorrespondenceTransformer
 * @package Modules\Correspondence\Repositories\Transformers
 */
class CorrespondenceTransformer extends EloquentTransformer
{
    use EloquentRelations, ReferenceTransformerTrait, TermTransformerTrait;

    protected $defaultIncludes = [
        'receivers',
        'senders',
        'receiver_location',
        'sender_location',
        'pdf',
        'references',
        'loadedReferences',
        'terms'
    ];

    public function transform(Entity $entity)
    {
        return [
            'id' => (int) $entity->id,
            'title' => $entity->title,
            'body' => $entity->body,
            'text' => $entity->text,
            'date' => $entity->date,
            'type' => $entity->type,
            'is_public' => $entity->is_public,
            'letterhead' => $entity->letterhead,
            'handwritten' => $entity->handwritten,
            'typewritten' => $entity->typewritten,
            'route' => $entity->route,
            'thumbnail' => $entity->Thumb_site_path,
        ];
    }

    public function includeReceivers(Entity $entity)
    {
        $entities = $this->getRelationAsCollection($entity, 'receiversCollection');

        return $this->collection($entities, new PersonTransformer());
    }

    public function includeSenders(Entity $entity)
    {
        $entities = $this->getRelationAsCollection($entity, 'sendersCollection');

        return $this->collection($entities, new PersonTransformer());

    }

    public function includeReceiverLocation(Entity $entity)
    {
        $entity = $this->getRelationAsItem($entity, 'receiverLocation');

        return $entity ? $this->item($entity, new LocationTransformer()) : null;
    }

    public function includeSenderLocation(Entity $entity)
    {
        $entity = $this->getRelationAsItem($entity, 'senderLocation');

        return $entity ? $this->item($entity, new LocationTransformer()) : null;
    }

    public function includePdf(Entity $entity)
    {
        $entity = $this->getRelationAsItem($entity, 'pdf');

        return $entity ? $this->item($entity, new MediaFileTransformer()) : null;
    }

}

<?php

namespace Modules\Correspondence\Http\Controllers;

use App\Repositories\Criteria\RelationsRequestCriteria;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Correspondence\Entities\Correspondence;
use Modules\Correspondence\Repositories\CorrespondenceRepository;
use Modules\Correspondence\Repositories\Presenters\CorrespondencePresenter;

class CorrespondenceController extends Controller
{
    /**
     * @var CorrespondenceRepository
     */
    protected $repo;

    /**
     * CorrespondenceController constructor.
     * @param CorrespondenceRepository $repository
     */
    public function __construct(CorrespondenceRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('correspondence::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('correspondence::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(int $id)
    {
        $correspondence = $this->repo
            ->with(['pdf', 'senderLocation', 'sendersCollection', 'receiverLocation', 'receiversCollection', 'references'])
            ->setPresenter($this->getPresenter())
            ->find($id);

        return view('correspondence::show', [
            'correspondence' => $correspondence,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('correspondence::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function dataList(Request $request)
    {
        $relations = $request->get('with', []);
        $limit = $request->get('limit', 25);

        if ($searchString = $request->get('search_string')) {

            $entities = $this->repo
                ->load($relations)
                ->setPresenter($this->getPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                }, $limit);

        } else {

            $entities = $this->repo
                ->pushCriteria(new RelationsRequestCriteria())
                ->setPresenter($this->getPresenter())
                ->paginate($limit);
        }

        return \Response::json($entities, 206);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function dataItem(Request $request, int $id)
    {
        $location = $this->repo
            ->with($relations = $request->get('with', []))
            ->setPresenter($this->getPresenter())
            ->find($id);

        return \Response::json($location, 200);
    }

    protected function getPresenter()
    {
        return new CorrespondencePresenter();
    }

}

<?php

namespace Modules\Correspondence\Http\Controllers\Api;

use App\Repositories\Criteria\SortById;
use App\Repositories\Presenters\MediaPresenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Correspondence\Entities\Correspondence as Entity;
use Modules\Correspondence\Repositories\CorrespondenceRepository as Repository;
use Modules\Correspondence\Repositories\Presenters\CorrespondencePresenter as Presenter;

class RestCorrespondenceController extends Controller
{
    /**
     * @var Repository
     */
    private $repo;

    public function __construct(Repository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        //Takes 'pdf', 'senderLocation', 'sendersCollection', 'receiverLocation', 'receiversCollection', 'references', 'terms'
        $relations = $request->get('with', []);

        $entity = $this->repo
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->pushCriteria(new SortById())
            ->paginate(25);

        return response()->json($entity, 206);    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('correspondence::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'             => 'required|max:255',
            'body'              => 'string',
            'text'              => 'string',
            'date'              => 'nullable|date_format:Y-m-d',
            'receivers'         => 'array',
            'receiver_location' => 'integer',
            'senders'           => 'array',
            'sender_location'   => 'integer',
            'pdf'               => 'file:pdf|max:20480',
            'references'        => 'json',
            'terms'             => 'json'
        ]);

        $entity = Entity::create([
            'title'             => $request->get('title'),
            'body'              => $request->get('body'),
            'text'              => $request->get('text'),
            'date'              => $request->get('date') ?: null,
            'type'              => $request->get('type'),
            'receivers'         => $request->get('receivers'),
            'receiver_location' => $request->get('receiver_location'),
            'senders'           => $request->get('senders'),
            'sender_location'   => $request->get('sender_location'),
            'references'        => $request->get('references')
        ]);

        // Attach PDF if it presents in request
        if ($request->file('pdf')) {
            $entity->addMediaFromRequest('pdf')
                   ->toMediaCollection(Entity::MEDIA_COLLECTION_LETTERS);
        }

        $entity->syncRefsRequest($request);
        $entity->syncTermsRequest($request);

        //Takes 'pdf', 'senderLocation', 'sendersCollection', 'receiverLocation', 'receiversCollection', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $entity->load($relations);
        }

        return response()->json($this->getPresenter()->present($entity), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        //Takes 'pdf', 'senderLocation', 'sendersCollection', 'receiverLocation', 'receiversCollection', 'references', 'terms'
        $relations = $request->get('with', []);

        $entity = $this->repo
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->find($id);

        return response()->json($entity, 200);    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('correspondence::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title'             => 'required|max:255',
            'body'              => 'string',
            'text'              => 'string',
            'date'              => 'nullable|date_format:Y-m-d',
            'receivers'         => 'array',
            'receiver_location' => 'integer',
            'senders'           => 'array',
            'sender_location'   => 'integer',
            'references'        => 'json',
            'terms'             => 'json'
        ]);

        $entity = $this->repo->find($id);

        $this->authorize('update', $entity);

        $entity->update([
            'title'             => $request->get('title'),
            'body'              => $request->get('body'),
            'text'              => $request->get('text'),
            'date'              => $request->get('date') ?: null,
            'type'              => $request->get('type'),
            'receivers'         => $request->get('receivers'),
            'receiver_location' => $request->get('receiver_location'),
            'senders'           => $request->get('senders'),
            'sender_location'   => $request->get('sender_location'),
            'references'        => $request->get('references')
        ]);

        $entity->syncRefsRequest($request);
        $entity->syncTermsRequest($request);

        //Takes 'pdf', 'senderLocation', 'sendersCollection', 'receiverLocation', 'receiversCollection', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $entity->load($relations);
        }

        return response()->json($this->getPresenter()->present($entity), 200);
    }

    /**
     * Update correspondence pdf file
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updatePdf(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'pdf' => 'file:pdf|max:204800',
        ]);

        $entity = $this->repo
            ->find($id);

        $this->authorize('update', $entity);

        // Delete pdf if exists
        if ($pdf = $entity->pdf) {
            $pdf->delete();
        }

        //Add new dpf file
        $entity->addMediaFromRequest('pdf')
               ->toMediaCollection(Entity::MEDIA_COLLECTION_LETTERS);

        $entity->load('pdf');

        return response()->json((new MediaPresenter())->present($entity->pdf), 201);
    }

    /**
     * Destroy correspondence pdf file
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroyPdf(int $id)
    {
        $entity = $this->repo
            ->find($id);

        $this->authorize('delete', $entity);

        // Delete pdf file if exist
        if ($pdf = $entity->pdf) {
            return response()->json($pdf->delete(), 204);
        }

        return response()->json(false, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $correspondence = $this->repo->find($id);

        $this->authorize('delete', $correspondence);

        $correspondence->delete();

        return response()->json($correspondence, 204);
    }

    /**
     * @return Presenter
     * @throws \Exception
     */
    public function getPresenter()
    {
        return new Presenter();
    }
}

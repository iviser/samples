<?php

Route::group(['middleware' => 'web', 'prefix' => 'correspondence', 'namespace' => 'Modules\Correspondence\Http\Controllers'], function()
{
    Route::get('/data', 'CorrespondenceController@dataList')->name('correspondence.data.list');
    Route::get('/{id}/data', 'CorrespondenceController@dataItem')->name('correspondence.data.item');

    Route::get('/', 'CorrespondenceController@index')->name('correspondence.index');
    Route::get('/{id}', 'CorrespondenceController@show')->name('correspondence.item');

});

// REST correspondence
Route::group(
    [
        'middleware' => 'auth:api',
        'prefix' => '/api/v1/correspondence',
        'namespace' => 'Modules\Correspondence\Http\Controllers\Api',
    ],
    function() {
        Route::get('/', 'RestCorrespondenceController@index')->name('api.correspondence.index');
        Route::get('/{id}', 'RestCorrespondenceController@show')->name('api.correspondence.show');
        Route::post('/', 'RestCorrespondenceController@store')->name('api.correspondence.create');
        Route::post('/{id}', 'RestCorrespondenceController@update')->name('api.correspondence.update');
        Route::delete('/{id}', 'RestCorrespondenceController@destroy')->name('api.correspondence.delete');

        Route::post('/{id}/pdf', 'RestCorrespondenceController@updatePdf')
             ->name('api.correspondence.pdf.update');
        Route::delete('/{id}/pdf', 'RestCorrespondenceController@destroyPdf')
             ->name('api.correspondence.pdf.delete');
    }
);
@extends('page-base')

@section('stylesheets')

@endsection

@section('page-header')
    <h1>Correspondence</h1>
@stop

@section('content')
    <div class="row">
        <correspondence-list></correspondence-list>
    </div>
@endsection

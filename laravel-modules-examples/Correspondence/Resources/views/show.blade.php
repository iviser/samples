@extends('page-base')

@section('page-header')
    <h1>{{ $correspondence['title'] }}</h1>
@stop

@section('content')
    <correspondence-single corr-data="{{ json_encode($correspondence) }}"></correspondence-single>
@endsection

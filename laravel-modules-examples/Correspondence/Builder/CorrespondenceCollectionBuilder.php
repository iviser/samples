<?php

namespace Modules\Correspondence\Builder;


use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Mockery\Exception;
use Modules\Correspondence\Entities\Correspondence;
use Modules\Location\Entities\Location;
use Modules\People\Entities\Person;

class CorrespondenceCollectionBuilder
{

    /**
     * @var string
     */
    protected $corTable;

    /**
     * @var Collection
     */
    protected $filtersData;

    /**
     * @var Builder
     */
    protected $query;

    /**
     * @var Collection
     */
    protected $result;

    protected $personCols = [
        'senders',
        'receivers'
    ];

    protected $personLocCols = [
        'receiver_location',
        'sender_location'
    ];

    protected static $sortableColumns = [
        'title',
        'date',
        'type',
        'receiver_location',
        'sender_location',
        'letterhead',
        'typewritten',
        'handwritten',
        'public',
    ];


    public function __construct()
    {
        $this->corTable = (new Correspondence())->getTable();
    }

    public static function sortableColumns()
    {
        return static::$sortableColumns;
    }

    /**
     * Initiate query builder
     */
    public function init(): CorrespondenceCollectionBuilder
    {
        $this->query = \DB::table($this->corTable);

        return $this;
    }

    /**
     * Get query
     * @return Builder
     * @throws \Exception
     */
    public function query(): Builder
    {
        if ($this->query) {
            return $this->query;
        }

        $this->init();

        if (!$this->query) {
            throw new Exception('Can not init query');
        }

        return $this->query;
    }


    /**
     * Add filters data
     *
     * @param Collection $data
     */
    public function addFilterData(Collection $data)
    {
        $this->filtersData = $data;
    }

    /**
     * Getters
     */
    public function filtersData(): Collection
    {
        if ($this->filtersData) {
            return $this->filtersData;
        }

        return $this->filtersData = collect();
    }

    /**
     * Add filters
     */
    public function filter()
    {
        if ($title = $this->filtersData()->get('title')) {
            $this->query()->where('title', 'ilike', "%{$title}%");
        }

        if ($corType = $title = $this->filtersData()->get('correspondence_type')) {
            $this->query()->where('type', '=', $corType);
        }
        $this->addDateFilter();

        $this->addBoolFilter('public', 'is_public');
        $this->addBoolFilter('handwritten');
        $this->addBoolFilter('typewritten');
        $this->addBoolFilter('letterhead');

        $this->addSendersFilters();
        $this->addReceiverFilters();

        $orderField = $this->filtersData()->get('order_by');
        $orderDirection = $this->filtersData()->get('order_direction');
        $this->order($orderField, $orderDirection);
    }

    public function addSendersFilters()
    {
        if ($filterVal = $this->filtersData()->get('senders_ids')) {
            $ids = \GuzzleHttp\json_encode($filterVal);
            $this->query()->whereRaw("senders @> '$ids'");
        }

        if ($filterVal = $this->filtersData()->get('sender_location_id')) {
            $this->query()->where('sender_location', $filterVal);
        }
    }

    public function addReceiverFilters()
    {
        if ($filterVal = $this->filtersData()->get('receivers_ids')) {
            $ids = \GuzzleHttp\json_encode($filterVal);
            $this->query()->whereRaw("receivers @> '$ids'");
        }

        if ($filterVal = $this->filtersData()->get('receiver_location_id')) {
            $this->query()->where('receiver_location', $filterVal);
        }
    }

    public function addDateFilter()
    {
        if (($startDate = $this->filtersData()->get('start_date')) &&
            ($endDate = $this->filtersData()->get('end_date'))) {

            $carbonStartDate = Carbon::parse($startDate)->toDateString();
            $carbonEndDate = Carbon::parse($endDate)->toDateString();
            $this->query()->where([
                ["date", ">=", $carbonStartDate],
                ["date", "<", $carbonEndDate],
            ]);

        } elseif ($startDate = $this->filtersData()->get('start_date')) {

            $carbonStartDate = Carbon::parse($startDate)->toDateString();

            $this->query()->where("date", "=", $carbonStartDate);
        }
    }

    /**
     * Perform boolean filtering
     * @param string $argName
     * @param string $fieldName
     */
    public function addBoolFilter(string $argName, string $fieldName = '')
    {
        if (!$fieldName) {
            $fieldName = $argName;
        }
        if (!is_null($filterVal = $this->filtersData()->get($argName))) {
            $bool =\GuzzleHttp\json_encode($filterVal);

            $this->query()->whereRaw("metadata->>'{$fieldName}' = '{$bool}'");
        }
    }

    /**
     * Set limit and offset. Used for pagination
     *
     * @param int $limit
     * @param int $offset
     */
    public function setLimitOffset($limit = 25, $offset = 0)
    {
        $this->query()->take($limit);
        $this->query()->skip($offset);
    }

    /**
     * Set ordering
     * @todo add parameters
     */
    public function order($field = null, $direction = null)
    {
        $orderField = "{$this->corTable}.id";

        if ($field === 'letterhead' ||
            $field === 'typewritten' ||
            $field === 'handwritten') {

            $orderField = "{$this->corTable}.metadata->{$field}";

        } elseif($field === 'public') {
            $orderField = "{$this->corTable}.metadata->is_public";

        }
        elseif ($field)  {

            $orderField = "{$this->corTable}.{$field}";
        }

        $this->query()->orderBy($orderField, $direction);
    }

    /**
     * Count total items of current query
     * @return int
     */
    public function total(): int
    {
        return $this->query()->getCountForPagination();
    }

    /**
     * execute query
     * @return mixed
     */
    public function execute(): Collection
    {
        $this->setResult($this->query()->get());

        return $this->getResult();
    }

    /**
     * Set results
     */
    protected function setResult(Collection $data)
    {
        $this->loadPersons($data);
        $this->loadPersonsLocation($data);
        $this->extractMetadata($data);
        $this->result = $data;
    }

    protected function loadPersons(Collection $data)
    {
        $pids = [];
        // Scan for persons ids
        foreach ($this->personCols as $colName) {
            $data->each(function($item, $key) use (&$pids, $colName) {
                if ($item->$colName) {
                    $item->$colName = \GuzzleHttp\json_decode($item->$colName);
                    foreach ($item->$colName as $pid) {
                        $pids[] = $pid;
                    }
                }
            });
        }

        $people = collect();
        // Load persons
        if ($pids) {
            Person::whereIn('id', array_unique($pids))
                ->get()
                ->each(function($item, $key) use ($people) {
                    $people->put($item->id, $item);
                });

        }

        if ($people->isNotEmpty()) {
            foreach ($this->personCols as $colName) {
                $data->each(function($item, $key) use ($people, $colName) {
                    $originColVals = $item->$colName;
                    $item->$colName = [];
                    if ($originColVals) {
                        foreach ($originColVals as $pid) {
                            $item->$colName[] = $people->get($pid);
                        }
                    }
                });
            }
        }

    }

    protected function loadPersonsLocation(Collection $data)
    {
        $lids = [];
        // Scan for persons ids
        foreach ($this->personLocCols as $colName) {
            $data->each(function($item, $key) use (&$lids, $colName) {
                if ($item->$colName) {
                    $lids[] = $item->$colName;
                }
            });
        }

        $locations = collect();
        // Load persons
        if ($lids) {
            Location::whereIn('id', array_unique($lids))
                ->get()
                ->each(function($item, $key) use ($locations) {
                    $locations->put($item->id, $item);
                });

        }

        if ($locations->isNotEmpty()) {
            foreach ($this->personLocCols as $colName) {
                $data->each(function($item, $key) use ($locations, $colName) {
                    if ($item->$colName) {
                        $item->$colName = $locations->get($item->$colName);
                    }
                });
            }
        }

    }

    protected function extractMetadata(Collection $data)
    {
        $data->each(function ($val, $key) {
            if ($metadataJSON = $val->metadata) {
                $metadata = (array) \GuzzleHttp\json_decode($metadataJSON);
                foreach ($metadata as $key => $item) {
                    $val->$key = $item;
                }
            }
        });

    }

    /**
     * Getter
     * @return Collection
     */
    public function getResult(): Collection {
        if ($this->result) {
            return $this->result;
        }

        return $this->execute();
    }
}
<?php

namespace Modules\Correspondence\Providers;

use App\Es\Indices\EgwIndex;
use App\Services\Registries\EntityRegistry;
use Illuminate\Support\ServiceProvider;
use \Modules\Correspondence\Entities\Correspondence as CorrespondenceEntity;
use Modules\Correspondence\Console\ConvertEmailsIntoCorrCommand;
use Modules\Correspondence\Console\DispatchPdfImport;
use Modules\Correspondence\Console\ImportCorrespondence;
use Modules\Correspondence\Es\Documents\Correspondence;
use Modules\Correspondence\Es\MappingTypes\Correspondence as CorrMappingType;
use Modules\Correspondence\Es\Query\CorrespondenceQueryProcessor;
use Modules\Correspondence\GraphQL\Mutations\CorrespondenceUpdate;
use Phirames\LaraElastic\LaraElastic;

class CorrespondenceServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->app->make(EntityRegistry::class)->add(CorrespondenceEntity::class);

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerGraphQL();
        $this->registerEs();
        $this->commands([
            ConvertEmailsIntoCorrCommand::class,
            ImportCorrespondence::class,
            DispatchPdfImport::class,
        ]);

    }

    public function registerEs()
    {
        $laraElastic = $this->app->make(LaraElastic::class);
        $laraElastic->indices()
            ->registry()
            ->getIndex(EgwIndex::name())
            ->addMapping(CorrMappingType::name(), CorrMappingType::properties());

        $laraElastic->docs()
            ->registry()->add(Correspondence::class);

        $laraElastic->query()->addProcessor(CorrespondenceQueryProcessor::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('correspondence.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'correspondence'
        );
    }

    protected function registerGraphQL()
    {
        \GraphQL::addSchema('default', [
            'query' => [
                'correspondence' => \Modules\Correspondence\GraphQL\Queries\CorrespondenceQuery::class,
                'correspondenceList' => \Modules\Correspondence\GraphQL\Queries\CorrespondenceListQuery::class,
            ],
            'mutation' => [

            ]
        ]);

        \GraphQL::addSchema('secret', [
            'query' => [
            ],
            'mutation' => [
                'correspondenceUpdate' => CorrespondenceUpdate::class,
            ]
        ]);


        // Register graphQL types
        \GraphQL::addType(\Modules\Correspondence\GraphQL\Types\CorrespondenceType::class, 'Correspondence');
        \GraphQL::addType(\Modules\Correspondence\GraphQL\Types\EsCorrespondence::class, 'EsCorrespondence');
        \GraphQL::addType(\Modules\Correspondence\GraphQL\Types\CorrTypeEnum::class, 'CorrespondenceType');
        \GraphQL::addType(\Modules\Correspondence\GraphQL\Types\CorrCollectionSortFields::class, 'CorrCollectionSortFields');
        \GraphQL::addType(\Modules\Correspondence\GraphQL\Types\CorrespondenceCollectionType::class, 'CorrespondenceCollectionType');
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/correspondence');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/correspondence';
        }, \Config::get('view.paths')), [$sourcePath]), 'correspondence');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/correspondence');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'correspondence');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'correspondence');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}

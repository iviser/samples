<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrespondenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_correspondence', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('body')->nullable();
            $table->integer('user_id')->nullable();

            $table->date('date')->nullable();

            $table->jsonb('taxonomy')->nullable();

            $table->jsonb('references')->nullable();
            /*
             * Metadata
             *
             * {
             *      'migrations': {
             *          'source_id': 0, // integer
             *          'old_url': 'http://example.com/path/to/content' // string
             *       }
             *      'to': 'string'
             *      'number': 'string'
             *      'collection': 'string'
             *      'date': 'string'
             *      'location': 'string'
             *      'published': 'string'
             *      'to': 'string'
             *      'to': 'string'
             * }
             *
             */
            $table->jsonb('metadata')->nullable();

            $table->text('text')->nullable();
            $table->text('type')->nullable();
            $table->jsonb('senders')->nullable();
            $table->jsonb('sender_location')->nullable();
            $table->jsonb('receivers')->nullable();
            $table->jsonb('receiver_location')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_correspondence');
    }
}

<?php

namespace Modules\Correspondence\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EgwPermissionsSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \App\Entities\Permission::firstOrCreate([
            'name'       => 'correspondence_create',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'correspondence_read',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'correspondence_update',
        ]);
        \App\Entities\Permission::firstOrCreate([
            'name'       => 'correspondence_delete',
        ]);
    }
}

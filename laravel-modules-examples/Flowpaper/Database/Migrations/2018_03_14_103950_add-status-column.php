<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pdf_processed', function (Blueprint $table) {
            $table->string('status')->default(\Modules\Flowpaper\Entities\Pdf::STATUS_UNPROCESSED);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pdf_processed', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}

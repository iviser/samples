<?php

Route::group(['middleware' => 'web', 'prefix' => 'flowpaper', 'namespace' => 'Modules\Flowpaper\Http\Controllers'], function()
{
    Route::get('/', 'FlowpaperController@index');
    // API
    Route::get('/api/v1/documents', 'FlowpaperApiController@documents');
    Route::get('/api/v1/documents/{id}', 'FlowpaperApiController@document');
    Route::post('/api/v1/documents/process', 'FlowpaperApiController@getDocument');
});

<?php

namespace Modules\Flowpaper\Widgets;

class PdfViewer
{
    public $template = 'flowpaper::viewers.flowpaper';
    public $cacheLifeTime = 1;
	public $friendlyName = 'PdfViewer';
    public $contextAs = '$file';

	public function data(string $source)
	{
	    return new \SplFileInfo($source);
	}
}
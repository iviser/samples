<?php

return [
    'name' => 'Flowpaper',

    /*
     * The file system which used to store source files
     * E.g. temp | s3 | local
     */
    'default_filesystem' => 'temp',

    'source' => \Modules\Flowpaper\Processor\Source::class,

    /*
     * The class instantiated as service container. Generates paths for flowpaper files
     */
    'path-generator' => \Modules\Flowpaper\PathGenerators\PathGenerator::class,

    'default-processor' => 'local',

    'processor' => [
        'local' => [
            'namespace' => \Modules\Flowpaper\Processor\PdfLocalProcessor::class,

            /*
             * Paths to the utilities which process pdf
             */
            'pdf2swf'   => '/usr/local/bin/pdf2swf',
            'pdfinfo'   => '/usr/local/bin/pdfinfo',
            'swfrender' => '/usr/local/bin/swfrender',
            'pdf2json'  => '/usr/local/bin/pdf2json',

            /*
             * File system where all flow paper entries will be stored
             * Before set up any file system it must be created in the filesystems.conf
             */
            'filesystem' => 'flowpaper',

            /*
             * The directory within the root of the filesystem disk. All flow paper entries will be stored in
             * this directory
             * E.g. path from the root of the project
             *
             * /storage/flowpaper/flowpaper-entries
             *
             * where `flowpaper` is the disk name and `flowpaper-entries` is entries_dir name
             *
             */
            'entries_dir' => 'flowpaper-entries',

            /*
             * Directory names for extracted files whithin the flowpaper entry
             *
             * E.g. path from the root of the project
             *
             * /storage/flowpaper/flowpaper-entries/432/swf
             *
             * where `flowpaper`         - the disk name,
             *       `flowpaper-entries` - entries_dir name
             *       `432`               - id of the flowpaper entiry
             *       `swf`               - directory name within flowpaper entry
             *
             */
            'pdfDir'  => 'pdf',
            'swfDir'  => 'swf',
            'pngDir'  => 'png',
            'jsonDir' => 'js'
        ],
    ],
];

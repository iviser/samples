<?php

namespace Modules\Flowpaper\Console;

use Illuminate\Console\Command;
use Modules\Flowpaper\Factories\PdfSourceFactory;
use Modules\Flowpaper\Jobs\ProcessPdfJob;
use Modules\Flowpaper\Processor\PdfLocalProcessor;
use Modules\Flowpaper\Repositories\FlowpaperRepository;
use Modules\Flowpaper\Services\SelectelPropagator;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MoveToSelectel extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'flowpaper:to-selectel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moves flowpaper file entries to selectel cloud';

    /**
     * Execute the console command.
     *
     * @param FlowpaperRepository $repo
     * @param SelectelPropagator $propagator
     *
     * @return mixed
     */
    public function handle(FlowpaperRepository $repo, SelectelPropagator $propagator)
    {
        if ($id = $this->argument('id')) {
            $propagator->setEntry($repo->find($id))
                       ->propagate();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['id', InputArgument::REQUIRED, 'Id of flowpaper entry'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}

<?php

namespace Modules\Flowpaper\PathGenerators;

use Modules\Flowpaper\Entities\FlowpaperEntry;
use Storage;
use Modules\Flowpaper\Processor\Interfaces\PdfPathPatternsInterface;

class PathPatternGetter implements PdfPathPatternsInterface
{
    /**
     * @var
     */
    protected $entity;

    protected $entriesDir;

    protected $swfPattern = '{[*,0].swf,}';
    protected $pngPattern = '{page}.png';
    protected $jsonPattern = 'json.js';

    public function __construct(FlowpaperEntry $entity)
    {
        $this->entity = $entity;
        $this->entriesDir = config('flowpaper.processor.local.entries_dir', 'fp-entries');

    }

    /**
     * Gets path of the all flowpaper entries
     *
     * @param string|null $subdir
     *
     * @return string
     */
    public function entryDir(string  $subdir = null): string
    {
        $dir =  "{$this->entriesDir}/{$this->entity->id}";

        return $subdir
            ? $dir  . "/" . config("flowpaper.processor.local.{$subdir}")
            : $dir;
    }

    /**
     * Returns pdf path pattern
     * @return string
     */
    public function pdf()
    {
        $processedFiles = $this->entity->processed_files;

        $pdf = isset($processedFiles->pdf) ? $processedFiles->pdf : null;


        return Storage::disk($this->entity->getDiskName())
                      ->url($pdf);
    }

    /**
     * {@inheritdoc}
     */
    public function swf()
    {
        $patternPath = "{$this->entryDir('swfDir')}/$this->swfPattern";

        return Storage::disk($this->entity->getDiskName())
               ->url($patternPath);
    }

    /**
     * {@inheritdoc}
     */
    public function png()
    {

        $patternPath = "{$this->entryDir('pngDir')}/$this->pngPattern";

        return Storage::disk($this->entity->getDiskName())
                      ->url($patternPath);

    }

    /**
     * {@inheritdoc}
     */
    public function json()
    {
        $patternPath = "{$this->entryDir('jsonDir')}/$this->jsonPattern";

        return Storage::disk($this->entity->getDiskName())
                      ->url($patternPath);
    }

}

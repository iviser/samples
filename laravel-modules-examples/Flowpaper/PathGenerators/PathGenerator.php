<?php

namespace Modules\Flowpaper\PathGenerators;

use Modules\Flowpaper\Entities\FlowpaperEntry;
use Storage;

class PathGenerator
{

    /**
     * Filesystem
     *
     * @var mixed
     */
    protected $filesystem;

    /**
     * Filesystem
     *
     * @var mixed
     */
    protected $entriesDir;

    public function __construct()
    {
        $this->filesystem = config('flowpaper.processor.local.filesystem', 'local');
        $this->entriesDir = config('flowpaper.processor.local.entries_dir', 'fp-entries');
    }

    /**
     * Gets the storage path basing on file system
     *
     * @return mixed
     */
    public function filesystemPath(): string
    {
        return Storage::disk($this->filesystem)
                      ->getDriver()
                      ->getAdapter()
                      ->getPathPrefix();
    }

    /**
     * Gets path of the all flowpaper entries
     *
     * @return string
     */
    public function entriesPath(): string
    {
        $disk = $this->filesystemPath();

        return "{$disk}/{$this->entriesDir}";
    }

    /**
     * @param FlowpaperEntry $entity
     *
     * Output directory for the Flowpaper Entry
     *
     * @return string
     */
    public function outputEntryDir(FlowpaperEntry $entity, string $dir = null): string
    {
        $directory = "{$this->entriesPath()}/{$entity->id}";

        if (!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        if ($dir) {
            $conversionDir = config("flowpaper.processor.local.{$dir}", $dir);
            $directory = "{$directory}/{$conversionDir}";
        }

        if (!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        return $directory;
    }

}

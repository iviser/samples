<?php

namespace Modules\Flowpaper\Entities;


interface FlowpaperEntry
{
    /**
     * @return string
     */
    public function getDiskName(): string;
}
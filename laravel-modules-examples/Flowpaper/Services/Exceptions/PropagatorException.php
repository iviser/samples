<?php

namespace Modules\Flowpaper\Services\Exceptions;


class PropagatorException extends \Exception
{
    public static function missingEntry()
    {
        return new static('Flow paper entry is not set');
    }

    public static function unprocessedEntry($id)
    {
        return new static("The entry [{$id}] is not processed.");
    }
}

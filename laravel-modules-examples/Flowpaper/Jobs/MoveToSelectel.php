<?php

namespace Modules\Flowpaper\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Flowpaper\Repositories\FlowpaperRepository;

class moveToSelectel implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    protected $id;
    protected $filesystem;

    /**
     * Create a new job instance.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
        $this->filesystem = config('flowpaper.processor.local.filesystem');
    }

    /**
     * Execute the job.
     *
     * @param FlowpaperRepository $repo
     *
     * @return void
     */
    public function handle(FlowpaperRepository $repo)
    {
        $entity = $repo->find($this->id);
        if ($entity->processed_files) {

            $this->propagateFile($entity->processed_files->pdf);
            $this->propagateFile($entity->processed_files->json);
            $this->propagateFiles($entity->processed_files->pngs);
            $this->propagateFiles($entity->processed_files->swfs);

        }
        \Storage::disk('selectel');

        $entity->disk = 'selectel';
        $entity->save();

    }

    protected function propagateFile(string $filePath)
    {
        $file = \Storage::disk($this->filesystem)->get($filePath);

        \Storage::disk('selectel')->put(
            "{$this->filesystem}/{$filePath}",
            $file);

    }

    protected function propagateFiles(array $filePaths)
    {
        foreach ($filePaths as $filePath) {
            $this->propagateFiles($filePath);

        }
    }
}

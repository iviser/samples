<?php

namespace Modules\Flowpaper\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Flowpaper\Factories\PdfSourceFactory;
use Modules\Flowpaper\Processor\PdfLocalProcessor;

class ProcessPdfJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    protected $source;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @param string $source
     */
    public function __construct($source)
    {
        $this->source = $source;
    }

    /**
     * Execute the job.
     *
     * @param PdfSourceFactory $factory
     *
     * @param PdfLocalProcessor $processor
     *
     * @return void
     */
    public function handle(PdfSourceFactory $factory, PdfLocalProcessor $processor)
    {
        $source = $factory->make($this->source);
        $processor->setSource($source)->process();
    }
}

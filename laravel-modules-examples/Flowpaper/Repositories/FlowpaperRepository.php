<?php

namespace Modules\Flowpaper\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Flowpaper\Entities\Pdf;

class FlowpaperRepository extends BaseAppRepository
{
    public function model()
    {
        return Pdf::class;
    }

}
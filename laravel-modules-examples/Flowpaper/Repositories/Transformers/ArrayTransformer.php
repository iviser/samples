<?php

namespace Modules\Flowpaper\Repositories\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Flowpaper\Entities\Pdf as Entity;

class ArrayTransformer extends TransformerAbstract
{
    public function transform(Entity $entity)
    {

        return [
            'id'              => (int) $entity->id,
            'source_file'     => $entity->source_file,
            'disk'            => $entity->disk,
            'processed_files' => $entity->processed_files ?: null,
            'patterns'        => $entity->path_patterns,
            'status'          => $entity->status,
        ];
    }
}
<?php

namespace Modules\Flowpaper\Processor;

use Modules\Flowpaper\Processor\Interfaces\PdfProcessorInterface;
use Modules\Flowpaper\Processor\Interfaces\SourceInterface;

/**
 * Class PdfProcessor
 * @package Modules\Flowpaper\Processor
 */
abstract class PdfProcessor implements PdfProcessorInterface
{

    /**
     * @var string
     */
    protected $source;

    /**
     * PdfProcessor constructor.
     * @param SourceInterface $source local path or url of existing pdf file
     *
     * @return PdfProcessor
     */
    public function setSource(SourceInterface $source)
    {
        $this->source = $source;

        return $this;
    }

}

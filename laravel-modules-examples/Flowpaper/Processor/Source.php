<?php

namespace Modules\Flowpaper\Processor;

use Modules\Flowpaper\Processor\Exceptions\FlowpaperSource;
use Modules\Flowpaper\Processor\Interfaces\SourceInterface;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;

/**
 * Class SourceGetter
 * Source object gets a local or a remote file and copies it on demand to the filesystem. The default file system
 * is taken from configuration file.
 *
 * @property bool copied
 * @package Modules\Flowpaper\Processor
 */
class Source implements SourceInterface
{

    /**
     * @var string the path or url to the source file
     */
    protected $source;

    /**
     * @var string the path to the destination file
     */
    protected $destination;

    /**
     * SourceGetter constructor.
     *
     * @param string $source Absolute server path or url to the pdf file
     *   Note: the file must be readable.
     *
     * @param string $filesystem The filesystem where source is being copied. Note: this is not a destination for
     *   processed files. You can change the default filesystem in `flowpaper.default_filesystem` configurations
     *
     * @throws FlowpaperSource
     */
    public function __construct(string $source, string $filesystem = null)
    {
        $this->source = $source;

        if (!self::fileExist($source)) {
            throw FlowpaperSource::fileDoesNotExists($source);
        }

        $this->setDestination($filesystem);

    }

    /**
     * Sets file system
     * @param null $filesystem
     */
    private function setDestination($filesystem = null)
    {
        // Get directory path of the file system
        $dir = \Storage::disk($filesystem ?: config('flowpaper.default_filesystem'))
                       ->getDriver()
                       ->getAdapter()
                       ->getPathPrefix();

        // Calculate the md5 hash of a UNIX timestamp and append to the directory path to prevent deleting of file by
        // other process
        $dir .= md5(time());
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $this->destination = $dir . "/" . basename($this->source);
    }

    /**
     * @param string $source
     *
     * @return bool
     */
    public static function fileExist(string $source): bool
    {
        return self::isUrl($source)
            ? @fclose(fopen(self::safeUrl($source),'r'))
            : file_exists($source);
    }

    /**
     * @param string $source
     *
     * @return bool
     */
    public static function isUrl(string $source): bool
    {
        return filter_var(self::safeUrl($source), FILTER_VALIDATE_URL);
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function safeUrl(string $string): string
    {
        return str_replace(' ', '%20', $string);
    }

    /**
     * {@inheritdoc}
     */
    public function getSrcFilePath(): string
    {
        return $this->source;
    }

    /**
     * {@inheritdoc}
     */
    public function getDistFilePath(): string
    {
        if (!self::fileExist($this->destination)) {

            $this->copied = copy(self::safeUrl($this->source), $this->destination);
        }

        return $this->destination;
    }

    public function deleteDistFile()
    {
        try {
            $dir = dirname($this->destination);

            $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);

            $files = new \RecursiveIteratorIterator($it,
                \RecursiveIteratorIterator::CHILD_FIRST);

            foreach($files as $file) {
                if ($file->isDir()){
                    rmdir($file->getRealPath());
                } else {
                    unlink($file->getRealPath());
                }
            }

            rmdir($dir);
            return true;

        } catch (\Exception $e) {

            return false;
        }

    }

}
<?php

namespace Modules\Flowpaper\Processor;

use Illuminate\Database\Eloquent\Model;
use Modules\Flowpaper\Entities\Pdf;
use Modules\Flowpaper\Processor\Exceptions\FlowpaperProcessorException;
use Modules\Flowpaper\PathGenerators\PathGenerator;
use Modules\Flowpaper\Processor\Interfaces\SourceInterface;
use \Storage;
use \File;

class PdfLocalProcessor extends PdfProcessor
{

    /**
     * @var Pdf
     */
    protected $model;

    /**
     * @var string
     */
    protected $pdf2swf;

    /**
     * @var string
     */
    protected $pdfinfo;

    /**
     * @var string
     */
    protected $swfrender;

    /**
     * @var string
     */
    protected $pdf2json;

    protected $pdfFile;
    protected $swfFiles;
    protected $pngFiles;
    protected $jsonFile;

    /**
     * Filesystem
     *
     * @var mixed
     */
    protected $filesystem;

    /**
     * Filesystem
     *
     * @var mixed
     */
    protected $entriesDir;

    /**
     * @var PathGenerator
     */
    protected $pathGenerator;

    public function __construct(PathGenerator $pathGenerator)
    {

        $this->pathGenerator = $pathGenerator;

        $this->pdf2swf   = config('flowpaper.processor.local.pdf2swf', 'pdf2swf');
        $this->pdfinfo   = config('flowpaper.processor.local.pdfinfo', 'pdfinfo');
        $this->swfrender = config('flowpaper.processor.local.swfrender', 'swfrender');
        $this->pdf2json  = config('flowpaper.processor.local.pdf2json', 'pdf2json');

        $this->filesystem = config('flowpaper.processor.local.filesystem', 'local');
        $this->entriesDir = config('flowpaper.processor.local.entries_dir', 'fp-entries');

        $this->swfFiles = [];
        $this->pngFiles = [];
        $this->jsonFile = '';
    }

    /**
     * Get document model
     * @return Model
     */
    public function document()
    {
        return $this->model;
    }

    /**
     * Get total amount of book pages
     *
     * @return int
     */
    public function countPages() {
        $command = "'{$this->pdfinfo}' " . $this->pdfFile;
        exec($command, $output);
        $pagecount = 0;
        foreach($output as $op) {
            // Extract the number
            if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1)
            {
                $pagecount = intval($matches[1]);
                break;
            }
        }
        return $pagecount;
    }

    /**
     * {@inheritdoc}
     */
    public function setSource(SourceInterface $source)
    {
        parent::setSource($source);


        $this->model = Pdf::firstOrCreate([
            'source_file' => $source->getSrcFilePath(),

        ], ['disk' => $this->filesystem,]);

        return $this;
    }

    public function pdfFile(): string
    {
        return $this->pdfFile ?: $this->savePdf();
    }

    /**
     * Get an array of swf files extracted from pdf file
     *
     * @return array
     */
    public function swfFiles(): array
    {
        return !empty($this->swfFiles) ? $this->swfFiles : $this->toSwf();
    }

    /**
     * Get an array of png files extracted from pdf file
     *
     * @return array
     */
    public function pngFiles(): array
    {
        return !empty($this->pngFiles) ? $this->pngFiles : $this->toPng();
    }

    /**
     * Get json file extracted from pdf file
     *
     * @return string
     */
    public function jsonFile(): string
    {
        return $this->jsonFile ?: $this->toJson();
    }

    public function getModel()
    {
        if (!$this->model) {
            throw new \Exception('The model is not set');
        }

        return $this->model;
    }

    /**
     * Get absolute path from the root of the server to the output dir
     *
     * @see Modules/Flowpaper/Config/config.php
     *
     * @param string $dir
     * @return string
     */
    protected function outputDir(string $dir): string
    {
        $directory =  $this->pathGenerator->outputEntryDir(
            $this->getModel(),
            $dir);

        return $directory;
    }


    /**
     * Save pdf file
     */
    protected function savePdf(): string
    {

        $this->pdfFile = $this->outputDir('pdfDir') . '/' . basename($this->source->getDistFilePath());

        if (!File::copy($this->source->getDistFilePath(), $this->pdfFile))
        {
            throw FlowpaperProcessorException::cannotCopyFile($this->source->getDistFilePath());
        }

        $pdfSplInfo = new \SplFileInfo($this->pdfFile);

        return $this->pdfFile = $pdfSplInfo->getRealPath();
    }

    /**
     * Creates swf file from pdf using pdf2swf library
     *
     * @return array
     * @throws FlowpaperProcessorException
     */
    protected function toSwf(): array
    {
        // Get number of pages
        $pageAmount = $this->countPages();
        $directory = $this->outputDir("swfDir");

        $pagesPerTime = 70;
        $steps = floor($pageAmount / $pagesPerTime);
        //Execute command in pipe to reviewing current progress of conversion
        for ($i = 0; $i <= $steps; $i++) {
            $start  = 1 + $i * $pagesPerTime;
            if ($i == $steps) {
                $end = $pageAmount;
            }
            else {
                $end = ($i+1) * $pagesPerTime;
            }
            $range = $start . "-" . $end;
            $command = "{$this->pdf2swf} '{$this->pdfFile}' -o '{$directory}/%.swf' -f -T 9 -t -s storeallcharacters -p {$range}";
            $handle = popen($command, 'r');
            //Try to make conversion
            while (!feof($handle)) {
                //Check current state every 2 seconds
                sleep(2);
                $read = fread($handle, 2096);
                $pattern = '/(FATAL|ERROR)/i';
                preg_match($pattern, $read, $m);
                if (!empty($m)) {
                    throw FlowpaperProcessorException::swfConversionFailed($read);
                }
                //Get current page and write serialized string with current progress in db.
                $pattern = '/page\s(\d+)/';
                preg_match($pattern, $read, $m);
            }
            pclose($handle);
        }

        foreach (\File::files(rtrim($directory, '\/')) as $swf) {
            $this->swfFiles[] = $swf->getRealPath();
        }

        return $this->swfFiles;
    }

    /**
     * Creates swf file from pdf using pdf2swf library
     *
     * @return array
     */
    protected function toPng(): array
    {
        $directory = $this->outputDir('pngDir');
        foreach ($this->swfFiles() as $delta => $swf) {
            $pageNum = $delta + 1;
            $png = "{$directory}/{$pageNum}.png";
            $command = "{$this->swfrender} '{$swf}' -o '{$png}' -X 1024 -s keepaspectratio";
            exec($command);
            if (file_exists($png)) {
                $pngSplInfo = new  \SplFileInfo($png);
                $this->pngFiles[] = $pngSplInfo->getRealPath();
            }
        }

        return $this->pngFiles;
    }

    /**
     * Creates json file from json
     * @return string
     */
    protected function toJson(): string
    {
        $directory = $this->outputDir('jsonDir');
        $json = "{$directory}/json.js";
        $command = "{$this->pdf2json} '{$this->pdfFile}' -enc UTF-8 -compress -hidden '{$json}'";
        exec($command);

        if (file_exists($json)) {
            $pngSplInfo = new  \SplFileInfo($json);
            $this->jsonFile = $pngSplInfo->getRealPath();
        }

        return $this->jsonFile;
    }

    /**
     * {@inheritdoc}
     */
    public function process()
    {
        // Set pending status before processing
        $this->setPendingStatus();

        // Perform processing
        try {
            $diskPath = $this->pathGenerator->filesystemPath();
            $processedFiles = new \stdClass();
            $processedFiles->pdf = str_replace($diskPath, '' ,$this->pdfFile());
            $processedFiles->swfs = str_replace($diskPath, '' ,$this->swfFiles());
            $processedFiles->pngs = str_replace($diskPath, '' ,$this->pngFiles());
            $processedFiles->json = str_replace($diskPath, '' ,$this->jsonFile());

            // Set processed files paths and status 'ok'
            $this->model->processed_files = $processedFiles;
            $this->model->status = Pdf::STATUS_OK;

        } catch (FlowpaperProcessorException $exception) {

            // If something went wrong set 'failed' status
            $this->model->status = Pdf::STATUS_FAILED;
        }

        $this->model->save();

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isProcessed(): bool
    {
        return $this->model->status === $this->model::STATUS_OK;
    }

    /**
     * Check if the pdf in processing
     */
    public function isPending(): bool
    {
        return $this->model->status === $this->model::STATUS_PENDING;
    }

    /**
     * Check if the pdf processing was failed
     */
    public function isFailed(): bool
    {
        return $this->model->status === $this->model::STATUS_FAILED;
    }

    /**
     * Check if the pdf is unprocessed
     */
    public function isUnprocessed(): bool
    {
        return $this->model->status === $this->model::STATUS_UNPROCESSED || is_null($this->model->status);
    }

    /**
     * Set model status to pending
     */
    public function setPendingStatus()
    {
        if(!$this->isPending()) {
            $this->model->status = $this->model::STATUS_PENDING;
            $this->model->save();
        }

        return $this;
    }

    /**
     * Set model status to failed
     */
    public function setFailedStatus()
    {
        if(!$this->isPending()) {
            $this->model->status = $this->model::STATUS_FAILED;
            $this->model->save();
        }
        
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public static function available(): bool
    {
        // This pdf processor must available at any time
        return true;
    }
}

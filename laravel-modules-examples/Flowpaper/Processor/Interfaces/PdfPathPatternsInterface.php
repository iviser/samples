<?php

namespace Modules\Flowpaper\Processor\Interfaces;

/**
 * Interface FlowpaperProcessorInterface
 *
 * processes pdf files to make it viewable form Flowpaper viewer
 *
 * @package Modules\Flowpaper\Processor
 */
interface PdfPathPatternsInterface
{

    /**
     * Returns patten path of swf files
     * @return mixed
     */
    public function swf();

    /**
     * Returns patten path of png files
     * @return mixed
     */
    public function png();

    /**
     * Returns patten path of png files
     * @return mixed
     */
    public function json();

}

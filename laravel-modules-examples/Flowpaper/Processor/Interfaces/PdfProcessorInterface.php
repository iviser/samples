<?php

namespace Modules\Flowpaper\Processor\Interfaces;

/**
 * Interface FlowpaperProcessorInterface
 *
 * processes pdf files to make it viewable form Flowpaper viewer
 *
 * @package Modules\Flowpaper\Processor
 */
interface PdfProcessorInterface
{

    /**
     * Checks if the pdf file is processed
     * @return bool
     */
    public function isProcessed(): bool;

    /**
     * Processes pdf file
     * Creates swf, png files, extracts text to json file
     * @return mixed
     */
    public function process();

    /**
     * Checks if the pdf processor is available and ready for processing
     * @return bool
     */
    public static function available(): bool;

}

@extends('page-base')

@section('title')
    Research centers and branch offices
@stop

@section('page-header')
    <h1>Research centers and branch offices</h1>
@stop

@section('content')
    <branches></branches>
@stop
@extends('page-base')

@section('title')
    Staff members
@stop

@section('page-header')
    <h1>Staff members</h1>
@stop

@section('content')
<staff-page></staff-page>
@stop
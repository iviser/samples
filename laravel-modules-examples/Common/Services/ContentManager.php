<?php


namespace Modules\Common\Services;

use Illuminate\Support\Collection;
use League\Flysystem\Exception;
use Modules\Common\Services\Interfaces\ContentInterface;
use Modules\Common\Services\Interfaces\ContentRouteInterface;
use \Config;
use \Module;
use Illuminate\Database\Eloquent\Model;


class ContentManager implements ContentInterface, ContentRouteInterface
{

    /**
     * @var Collection;
     */
    protected $content_types;

    /**
     * {@inheritdoc}
     */
    public function contentTypes(): Collection
    {
        if (!$this->content_types) {

            $namespaces = [];

            $modules = Module::enabled();

            foreach ($modules as $module) {
                $contentTypes = Config::get("{$module->getLowerName()}.content_types");
                if ($contentTypes) {
                    $namespaces = array_unique (array_merge ($namespaces, $contentTypes));
                }
            }

            $instances = collect();
            foreach ($namespaces as $namespace) {
                $model = new $namespace;
                $instances->put(str_slug($model->getTable()), $model);
            }

            $this->content_types = $instances;
        }

        return $this->content_types;
    }


    public function routeModel(string $routeName): Model
    {
        $model = $this->contentTypes()->get($routeName);

        if (is_null($model)) {
            throw new Exception('Model was not found');
        }

        return $model;
    }

    public function modelItem(string $routeName, int $id): Model
    {
        $model = $this->contentTypes()->get($routeName);

        if (is_null($model)) {
            throw new Exception('Model was not found');
        }

        return $model->findOrFail($id);

    }

    /**
     * Check if the route exists
     *
     * @param string $routeName
     * @return bool
     */
    public function routeExist(string $routeName): bool
    {
        return (bool) $this->contentTypes()->get($routeName);
    }

    /**
     * @param Model $model
     * @return string
     */
    public function routeName(Model $model): string
    {
        return with($model)->getTable();
    }

}

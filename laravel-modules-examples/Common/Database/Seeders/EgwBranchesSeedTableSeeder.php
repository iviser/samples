<?php

namespace Modules\Common\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;
use Modules\Common\Entities\Branch;

class EgwBranchesSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Create branch offices and research centers
        $this->createBranch([
            'title' => 'Adventist International Institute of Advanced Studies',
            'type' => Branch::TYPE_BRANCH_OFFICE,
            'domain' => 'aiias',
            'address' => $this->address(
                'PH',
                'Pasay City',
                null,
                null,
                'Adventist International Institute of Advanced Studies (AIIAS)',
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                14.202203400000,
                120.967208400000
            ),
            'contacts' => $this->contacts(
                null,
                'aiias@whiteestate.org',
                null,
                null,
                'Reuel Almocera',
                'Almocera@aiias.edu',
                null
            ),
        ]);
        // AUA
        $this->createBranch([
            'title' => 'Adventist University of Africa',
            'type' => Branch::TYPE_BRANCH_OFFICE,
            'domain' => 'aua',
            'address' => $this->address(
                'KE',
                null,
                null,
                null,
                'University of Eastern Africa Baraton',
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                0.257820800000,
                35.086556600000
            ),
            'contacts' => $this->contacts(
                null,
                'aua@whiteestate.org',
                null,
                null,
                'Anna Galeniece',
                'galeniecea@aua.ac.ke',
                null
            ),
        ]);
        // CAS
        $this->createBranch([
            'title' => 'Adventist University of France',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'cas',
            'address' => $this->address(
                'CH',
                'Geneva',
                null,
                null,
                'Saléve Adventist Institute / Institut Adventiste du Saléve',
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                46.198392200000,
                6.142296100000
            ),
            'contacts' => $this->contacts(
                null,
                'campusadventiste@whiteestate.org',
                null,
                null,
                'Jean-Luc-Rolland',
                'jl.rolland@campusadventiste.edu',
                 'Campus Adventiste du Saleve'
            ),
        ]);
        // CAR
        $this->createBranch([
            'title' => 'Andrews University',
            'type' => Branch::TYPE_BRANCH_OFFICE,
            'domain' => 'car',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'car@whiteestate.org',
                null,
                null,
                'Merlin D. Burt',
                'burt@andrews.edu',
                'Center for Adventist Research'
            ),
        ]);
        // UAA
        $this->createBranch([
            'title' => 'Antillean Adventist University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'uaa',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'uaa@whiteestate.org',
                null,
                null,
                'Pedro Cortes Burgos',
                'plcb511@yahoo.com',
                'Universidad Adventista de las Antillas'
            ),
        ]);
        // UAA
        $this->createBranch([
            'title' => 'Avondale College of Higher Education',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'avondale',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'avondale@whiteestate.org',
                null,
                null,
                'John Skrzypaszek',
                'John.skrzypaszek@avondale.edu.au',
                null
            ),
        ]);
        // BU
        $this->createBranch([
            'title' => 'Babcock University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'bu',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'babcock@whiteestate.org',
                null,
                null,
                'Adelowo Adetunji',
                'afadesta@yahoo.com',
                null
            ),
        ]);
        // BU
        $this->createBranch([
            'title' => 'Bogenhofen Seminary',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'bogi',
            'address' => $this->address(
                'AT',
                'Bogenhofen',
                null,
                'Bogenhofen 1',
                'Bogenhofen Seminary',
                null,
                'Oberösterreich',
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'bogenhofen@whiteestate.org',
                null,
                null,
                'Markus Kutzschbach',
                'kutzschbach@gmx.net',
                'Seminar Schloss Bogenhofen'
            ),
        ]);
        // UNSAP
        $this->createBranch([
            'title' => 'Brazil Adventist University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'unasp',
            'address' => $this->address(
                'BR',
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'unasp@whiteestate.org',
                null,
                null,
                'Renato Stencel',
                'Renato.stencel@unasp.edu.br',
                'Centro Universitario Adventista de Sao Paulo - UNASP'
            ),
        ]);
        // UNADECA
        $this->createBranch([
            'title' => 'Central American Adventist University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'unadeca',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'unadeca@whiteestate.org',
                null,
                null,
                'Franz Rios',
                'franzrios@hotmail.com',
                'Universidad Adventista de Centro America - UNADECA'
            ),
        ]);
        // HBC
        $this->createBranch([
            'title' => 'Helderberg College',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'hbc',
            'address' => $this->address(
                'ZA',
                'Cape',
                null,
                'P.O. Mount Clair',
                'Helderberg College',
                null,
                'Somerset West',
                null
            ),
            'geocode' => $this->geocode(
                -34.061115000000,
                18.836886000000
            ),
            'contacts' => $this->contacts(
                null,
                'helderberg@whiteestate.org',
                null,
                null,
                'Passmore Hachalinga',
                'hachalingap@sid.adventist.org',
                'Universidad Adventista de Centro America - UNADECA'
            ),
        ]);
        // LLU
        $this->createBranch([
            'title' => 'Loma Linda University',
            'type' => Branch::TYPE_BRANCH_OFFICE,
            'domain' => 'llu',
            'address' => $this->address(
                'US',
                'Loma Linda',
                '92354',
                '24783 Lawton Ave.',
                'Helderberg College',
                null,
                'CA',
                null
            ),
            'geocode' => $this->geocode(
                34.044519000000,
                117.263849000000
            ),
            'contacts' => $this->contacts(
                null,
                'llu@whiteestate.org',
                null,
                null,
                'Theodore Levterov',
                'tlevterov@llu.edu',
                'Loma Linda Heritage Research Center'
            ),
        ]);
        // UM
        $this->createBranch([
            'title' => 'Montemorelos University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'um',
            'address' => $this->address(
                'MX',
                'Montemorelos',
                null,
                null,
                'Montemorelos University / Universidad de Montemorelos',
                null,
                'Nuevo León',
                null
            ),
            'geocode' => $this->geocode(
                25.192621000000,
                -99.844158900000
            ),
            'contacts' => $this->contacts(
                null,
                'um@whiteestate.org',
                null,
                null,
                'Juan Jose Andrade',
                'jjandrade@um.edu.mx',
                'Universidad de Montemorelos'
            ),
        ]);

        // IAENE
        $this->createBranch([
            'title' => 'Gerson Rodrigues',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'iaene',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'gersoncr@andrews.edu',
                null,
                null,
                'Radica Antic',
                'rantic@newbold.ac.uk',
                'Faculdade Adventista da Bahia'
            ),
        ]);

        // NCU
        $this->createBranch([
            'title' => 'Northern Caribbean University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'ncu',
            'address' => $this->address(
                'TT',
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'ncu@whiteestate.org',
                null,
                null,
                'Robert A Wright',
                'rright@ncu.edu.jm',
                'Faculdade Adventista da Bahia'
            ),
        ]);

        // OU
        $this->createBranch([
            'title' => 'Oakwood University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'ou',
            'address' => $this->address(
                'TT',
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'ou@whiteestate.org',
                null,
                null,
                'Director',
                null,
                null
            ),
        ]);

        // UPEU
        $this->createBranch([
            'title' => 'Peruvian Union University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'upeu',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'upeu@whiteestate.org',
                null,
                null,
                'Gluder Quispe',
                'gluderquispe@teologia.edu.pe',
                'Universidad Peruana Union'
            ),
        ]);

        // UAP
        $this->createBranch([
            'title' => 'River Plate Adventist University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'uap',
            'address' => $this->address(
                'AR',
                'Puiggari',
                null,
                null,
                'River Plate Adventist University / Universidad Adventista del Plata',
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                35.201050000000,
                -91.831833400000
            ),
            'contacts' => $this->contacts(
                null,
                'uap@whiteestate.org',
                null,
                null,
                'Sergio Becerra',
                'ciwdirec@uap.edu.ar',
                'Universidad Adventista del Plata'
            ),
        ]);

        // SYU
        $this->createBranch([
            'title' => 'Sahmyook University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'syu',
            'address' => $this->address(
                'KR',
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'sahmyook@whiteestate.org',
                null,
                null,
                'Hyunsok John Doh',
                'Hyunsokdoh1010@hotmail.com',
                'Sahmyook Daehakgyo'
            ),
        ]);

        // SWAU
        $this->createBranch([
            'title' => 'Southwestern Adventist University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'swau',
            'address' => $this->address(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'swau@whiteestate.org',
                null,
                null,
                'Alfredo Vergel',
                'avergel@swau.edu',
                null
            ),
        ]);

        // SPICER
        $this->createBranch([
            'title' => 'Spicer Adventist University',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'spicer',
            'address' => $this->address(
                'IN',
                'Poona',
                null,
                null,
                'Spicer Memorial College',
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                18.564455700000,
                73.820218400000
            ),
            'contacts' => $this->contacts(
                null,
                'spicer@whiteestate.org',
                null,
                null,
                'Jesin Israel Kollabathula',
                'jesinisrael@gmail.com',
                null
            ),
        ]);

        // UEAB
        $this->createBranch([
            'title' => 'University of Eastern Africa, Baraton',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'ueab',
            'address' => $this->address(
                'KE',
                'Poona',
                null,
                null,
                'University of Eastern Africa Baraton',
                null,
                null,
                null
            ),
            'geocode' => $this->geocode(
                0.257820800000,
                35.086556600000
            ),
            'contacts' => $this->contacts(
                null,
                'baraton@whiteestate.org',
                null,
                null,
                'James Mutua',
                'mutuaj@ueab.ac.ke; egwrc@ueab.ac.ke',
                null
            ),
        ]);

        // ZAU
        $this->createBranch([
            'title' => 'Zaoksky Adventist Seminary and Institute',
            'type' => Branch::TYPE_RESEARCH_CENTER,
            'domain' => 'zau',
            'address' => $this->address(
                'RU',
                'Zaokski village',
                null,
                '43 A Rudneva street',
                'Zaokski Theological Seminary / Zaokskaya Seminariya Adventistov',
                null,
                'Tula Region',
                null
            ),
            'geocode' => $this->geocode(
                null,
                null
            ),
            'contacts' => $this->contacts(
                null,
                'zaoksky@whiteestate.org',
                null,
                null,
                'Andrusiak Vsevolod',
                'andrusiakvsv@hotmail.com',
                'Zaokskaya Adventistkaya Akademiya i Institut'
            ),
        ]);
    }

    public function address(
        $country = null,
        $locality = null,
        $postal_code = null,
        $thoroughfare = null,
        $organisation_name = null,
        $dependent_locality = null,
        $administrative_area = null,
        $sub_administrative_area = null
    )
    {
        $obj = new \stdClass();
        $obj->country = $country;
        $obj->locality = $locality;
        $obj->postal_code = $postal_code;
        $obj->thoroughfare = $thoroughfare;
        $obj->organisation_name = $organisation_name;
        $obj->dependent_locality = $dependent_locality;
        $obj->administrative_area = $administrative_area;
        $obj->sub_administrative_area = $sub_administrative_area;

        return $obj;
    }

    public function geocode($lat = null, $lng = null)
    {
        $obj = new \stdClass();

        $obj->lat = $lat;
        $obj->lng = $lng;

        return $obj;
    }

    public function contacts(
        $fax = null,
        $email = null,
        $phone = null,
        $website = null,
        $person_name = null,
        $person_email = null,
        $institution_local_name = null
    )
    {
        $obj = new \stdClass();

        $obj->fax = $fax;
        $obj->email = $email;
        $obj->phone = $phone;
        $obj->website = $website;
        $obj->person_name = $person_name;
        $obj->person_email = $person_email;
        $obj->institution_local_name = $institution_local_name;

        return $obj;
    }

    public function createBranch(array $data)
    {
        if (isset($data['title']) && $data['title']) {
            $branch = Branch::where('title', '=', $data['title'])->get()->first();
            if ($branch) {
                $branch->update($data);

            } else {
                $branch = Branch::create($data);
            }

            return $branch;
        }

        throw new Exception('Incorrect data');
    }
}

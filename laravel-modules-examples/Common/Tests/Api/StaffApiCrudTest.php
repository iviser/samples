<?php

namespace Modules\Common\Tests\Api;

use App\Services\Registries\EntityRegistry;
use Illuminate\Http\UploadedFile;
use Modules\Common\Entities\Staff;
use Modules\People\Entities\Person;
use Tests\CreatesApplication;
use Tests\Mock\Models\ReferencedModelA;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Tests\Traits\TestReferenceTrait;
use Tests\Traits\TestTermsReferenceTrait;

class StaffApiCrudTest extends TestCase
{
    use CreatesApplication;

    public function setUp()
    {
        parent::setUp();
        Artisan::call('module:migrate', [
            'module' => 'Common'
        ]);
    }

    public function createStaff(): Staff
    {
        $staff = factory(Staff::class)->make();
        $staff->save();
        $staff->addMedia($this->faker->image())
            ->toMediaCollection(Staff::AVATAR_MEDIA_COLLECTION_NAME);

        $staff->load('avatar');

        return $staff;
    }

    /**
     * Create staff correctly.
     *
     * @return void
     */
    public function testStaffIsCreatedCorrectly()
    {
        $user = $this->createUser();
        $fileName = "{$this->faker->word}.jpg";
        $payload = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'avatar' => UploadedFile::fake()->image($fileName),
        ];

        // Perform a request
        $resp = $this->json('POST', "http://localhost/api/v1/staff?with[]=avatar", $payload, $this->getUserHeaders($user));

        $resp->assertStatus(201)
            ->assertJson([
                'title' => $payload['title'],
                'description' => $payload['description'],
                'avatar' => [
                    'file_name' => $payload['avatar']->name
                ]
            ]);
    }

    /**
     * Update staff correctly.
     *
     * @return void
     */
    public function testStaffIsUpdatedCorrectly()
    {
        $user = $this->createUser();
        $staff = $this->createStaff();

        $payload = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
        ];

        // Perform a request
        $resp = $this->json('POST', "http://localhost/api/v1/staff/{$staff->id}", $payload, $this->getUserHeaders($user));

        $resp->assertStatus(200)
            ->assertJson([
                'title' => $payload['title'],
                'description' => $payload['description'],
            ]);
    }

    /**
     * Update staff avatar correctly.
     *
     * @return void
     */
    public function testStaffUpdatedAvatarCorrectly()
    {
        $user = $this->createUser();
        $staff = $this->createStaff();

        $imageFilename = $this->faker->md5 . '.png';

        $payload = [
            'avatar' => UploadedFile::fake()->image($imageFilename)
        ];

        // Perform a request
        $resp = $this->json('POST', "http://localhost/api/v1/staff/{$staff->id}/avatar", $payload, $this->getUserHeaders($user));

        $resp->assertStatus(201)
        ->assertJson([
            'file_name' => $payload['avatar']->name
        ]);
    }

    public function testStaffIsDeletedCorrectly()
    {
        $user = $this->createUser();
        $staff = $this->createStaff();

        $payload = [];

        // Perform a request
        $this->json('DELETE', "http://localhost/api/v1/staff/{$staff->id}", $payload, $this->getUserHeaders($user))
            ->assertStatus(204);
    }

    /**
     * Delete staff avatar correctly.
     *
     * @return void
     */
    public function testStaffDeletedAvatarCorrectly()
    {
        $user = $this->createUser();
        $staff = $this->createStaff();

        $payload = [];

        // Perform a request
        $resp = $this->json('DELETE', "http://localhost/api/v1/staff/{$staff->id}/avatar", $payload, $this->getUserHeaders($user));

        $resp->assertStatus(204);
    }
}

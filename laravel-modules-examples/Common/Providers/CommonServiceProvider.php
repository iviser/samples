<?php

namespace Modules\Common\Providers;

use App\Services\ContentTypes\ContentTypeRegistry;
use App\Services\Registries\EntityRegistry;
use Illuminate\Support\ServiceProvider;
use Modules\Common\Entities\SitePage;
use Modules\Common\GraphQL\Mutations\StaffCreate;
use Modules\Common\GraphQL\Mutations\StaffDelete;
use Modules\Common\GraphQL\Mutations\StaffDeleteAvatar;
use Modules\Common\GraphQL\Mutations\StaffUpdate;
use Modules\Common\GraphQL\Types\Input\StaffCreateInput;
use Modules\Common\GraphQL\Types\Input\StaffUpdateInput;
use Modules\Common\GraphQL\Types\Staff;
use Modules\Common\GraphQL\Types\StaffCollectionType;
use Modules\Common\Repositories\SitePagesRepository;
use Modules\Common\Repositories\StaffRepository;
use Modules\Common\Services\ContentManager;
use Modules\Common\GraphQL\Queries\StaffQuery;
use Modules\Common\GraphQL\Queries\StaffCollectionQuery;
use Modules\Common\Services\References\ReferenceRegistry;

class CommonServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerRepo();
        $this->registerContentTypes();
        $this->app->make(EntityRegistry::class)->add(SitePage::class, 'site_page');

    }

    protected function registerContentTypes()
    {
        $registry = $this->app->make(ContentTypeRegistry::class);
        $registry->addContentType('site_page', SitePage::class);
    }

    protected function registerRepo()
    {
        $this->app->bind(SitePagesRepository::class, function ($app) {
            return new SitePagesRepository($app);
        });
        $this->app->bind(StaffRepository::class, function ($app) {
            return new StaffRepository($app);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ContentManager::class, function($app) {
           return new ContentManager();
        });

        $this->registerGraphQL();
    }

    protected function registerGraphQL()
    {
        \GraphQL::addSchema('default', [
            'query' => [
                'staff' => StaffQuery::class,
                'staffCollection' => StaffCollectionQuery::class,
            ],
            'mutation' => [
            ]
        ]);

        \GraphQL::addSchema('secret', [
            'query' => [
            ],
            'mutation' => [
                'staffUpdate'       => StaffUpdate::class,
                'staffCreate'       => StaffCreate::class,
                'staffDelete'       => StaffDelete::class,
                'staffDeleteAvatar' => StaffDeleteAvatar::class,
            ]
        ]);


        // Register graphQL types
        \GraphQL::addType(Staff::class, 'Staff');
        \GraphQL::addType(StaffCollectionType::class, 'StaffCollectionType');

        // Input types
        \GraphQL::addType(StaffCreateInput::class, 'StaffCreateInput');
        \GraphQL::addType(StaffUpdateInput::class, 'StaffUpdateInput');

    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('common.php'),
        ]);
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'common'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/common');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/common';
        }, \Config::get('view.paths')), [$sourcePath]), 'common');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/common');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'common');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'common');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}

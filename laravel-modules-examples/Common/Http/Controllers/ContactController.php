<?php

namespace Modules\Common\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Common\Entities\Branch;
use Modules\Common\Entities\Submission;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function branches()
    {
        return view('common::contacts.branches');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('common::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(int $id)
    {
        return view('common::contacts.branch', [
            'branch' => Branch::findOrFail($id),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('common::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Faq data
     * @param Request $request
     * @return Response
     */
    public function data(Request $request)
    {

        $faqModel = new Branch();
        $query = $faqModel->newQuery();

        if ($skip = $request->get('skip')) {
            $query->skip($skip);
        }

        if ($take = $request->get('take')) {
            $query->skip($take);
        }

        if ($q = $request->get('q')) {
            $query->where('title', 'like', "%{$q}%")
                ->orWhere('descriptions', 'like', "%{$q}%");
        }

        return response()->json([
            'data' => $query->get(),
            'count' => $query->count(),
            'get' => $request->get('q'),
        ]);
    }

    /**
     * Faq data
     * @param integer $id
     * @param Request $request
     * @return Response
     */
    public function dataId($id, Request $request)
    {

        $faqModel = new Branch();
        $query = $faqModel->newQuery();
        $query->where('id', '=', $id);

        return response()->json([
            'data' => $query->get()->first(),
        ]);
    }

    public function feedback(Request $request)
    {
        $choosenType = $request->get('choosenType');

        return view('common::contacts.feedback',[
            'choosenType'  => $choosenType
        ]);
    }

    public function feedbackPost(Request $request)
    {
        $input = $request->input('params');

        $sender = new \stdClass();
        $sender->email = $input['email'];
        $sender->name = $input['name'];

        $receiver = new \stdClass();
        $receiver->email = $input['mailTo'];


        Submission::create([
            'sender' => $sender,
            'receiver' => $receiver,
            'message' => $input['message'],
        ]);

        // Make job which send the mail to the receiver
//        \Mail::queue('emails.welcome', $data, function ($message) {
//            //
//        });

        return response()->json(['success']);    }
}

<?php

namespace Modules\Common\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


/**
 * Class StaffController
 * @package Modules\Common\Http\Controllers
 */
class StaffController extends Controller
{

    /**
     * Shows staff
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('common::staff.index');

    }

}
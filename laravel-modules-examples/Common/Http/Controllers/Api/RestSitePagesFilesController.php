<?php

namespace Modules\Common\Http\Controllers\Api;

use App\Repositories\Presenters\MediaPresenter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Common\Entities\SitePage;
use Modules\Common\Repositories\SitePagesRepository as Repository;
use Modules\Common\Repositories\Presenters\SitePagesPresenter as Presenter;

class RestSitePagesFilesController extends Controller
{
    /**
     * @var Repository
     */
    private $repo;

    public function __construct(Repository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $sitePageId
     *
     * @return Response
     */
    public function index(int $sitePageId)
    {
        $sitePage = $this->repo->with('files')->find($sitePageId);

        return \Response::json((new MediaPresenter())->present($sitePage->files), 200);;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('common::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int $sitePageId
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request, int $sitePageId)
    {
        $sitePage = $this->repo->find($sitePageId);

        $request->validate([
            'files.*' => 'file|max:20480',
        ]);

        if($request->file('files')) {
            $fileAdders = $sitePage->addMultipleMediaFromRequest(['files']);

            $fileAdders->each(function($fileAdder) {
                $fileAdder->toMediaCollection(SitePage::FILES_MEDIA_COLLECTION_NAME);
            });
        };

        $sitePage->load('files');

        return \Response::json((new MediaPresenter())->present($sitePage->files), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param int $sitePageId
     * @param int $fileId
     *
     * @return Response
     */
    public function show(int $sitePageId, int $fileId)
    {

        $sitePage = $this->repo->with('files')->find($sitePageId);

        $file = $sitePage->files()->where('id', '=', $fileId)->first();

        if ($file) {
            return \Response::json((new MediaPresenter())->present($file), 200);
        } else {
            return \Response::json([], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('common::edit');
    }

    /**
     * Update the specified resource in storage.
     * @apram int $id
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, int $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $sitePageId
     * @param int $fileId
     *
     * @return Response
     */
    public function destroy(int $sitePageId, int $fileId)
    {
        $sitePage = $this->repo->with('files')->find($sitePageId);

        $file = $sitePage->files()->where('id', '=', $fileId)->first();

        if ($file) {
            return \Response::json($file->delete($fileId), 204);
        } else {
            return \Response::json(null, 404);
        }
    }

    public function createPresenter()
    {
        return new Presenter();
    }
}

<?php

namespace Modules\Common\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Criteria\RelationsRequestCriteria;
use App\Repositories\Presenters\MediaPresenter;
use Illuminate\Http\Request;
use Modules\Common\Repositories\Presenters\StaffPresenter;
use Modules\Common\Repositories\StaffRepository;
use Modules\Common\Entities\Staff as Entity;

class RestStaffController extends Controller
{
    /**
     * @var StaffRepository
     */
    private $repo;

    public function __construct(StaffRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $entities = $this->repo
            ->pushCriteria(new RelationsRequestCriteria())
            ->setPresenter($this->getPresenter())
            ->paginate();

        return response()->json($entities, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('staff::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'avatar'   => 'file|max:2048|image:jpeg,png,bmp,gif,svg',
        ]);

        $staff = Entity::create($request->all());

        //Add new media file
        if ($request->file('avatar')) {
            $staff->addMediaFromRequest('avatar')
                ->toMediaCollection(Entity::AVATAR_MEDIA_COLLECTION_NAME);
        }

        if ($relations = $request->get('with', [])) {
            $staff->load($relations);
        }

        return response()->json($this->getPresenter()->present($staff), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        $staff = $this->repo
            ->pushCriteria(new RelationsRequestCriteria())
            ->setPresenter($this->getPresenter())
            ->find($id);

        return response()->json($staff, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('staff::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required|max:255',
        ]);

        $staff = $this->repo->find($id);

        $this->authorize('update', $staff);

        $staff->update($request->all());

        if ($relations = $request->get('with', [])) {
            $staff->load($relations);
        }

        return response()->json($this->getPresenter()->present($staff), 200);
    }

    /**
     * Updates media file nested in the content media type entity
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateAvatar(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'avatar' => 'file|max:2048|image:jpeg,png,bmp,gif,svg',
        ]);

        $entity = $this->repo
            ->find($id);

        $this->authorize('update', $entity);

        // Delete avatar if exists
        if ($avatar = $entity->avatar) {
            $avatar->delete();
        }

        //Add new media file
        $entity->addMediaFromRequest('avatar')
            ->toMediaCollection(Entity::AVATAR_MEDIA_COLLECTION_NAME);

        $entity->load('avatar');

        return response()->json((new MediaPresenter())->present($entity->avatar), 201);
    }

    /**
     * Updates media file nested in the content media type entity
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroyAvatar(int $id)
    {
        $entity = $this->repo
            ->find($id);

        $this->authorize('delete', $entity);

        // Delete existing media file
        if ($avatar = $entity->avatar) {
            return response()->json($avatar->delete(), 204);
        }

        return response()->json(false, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $staff = $this->repo->find($id);

        $this->authorize('delete', $staff);

        $staff->delete();

        return response()->json($staff, 204);
    }

    /**
     * @return StaffPresenter
     * @throws \Exception
     */
    public function getPresenter()
    {
        return new StaffPresenter();
    }
}
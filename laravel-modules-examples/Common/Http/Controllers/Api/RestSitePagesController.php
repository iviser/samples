<?php

namespace Modules\Common\Http\Controllers\Api;

use App\Repositories\Presenters\MediaPresenter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Common\Repositories\SitePagesRepository as Repository;
use Modules\Common\Repositories\Presenters\SitePagesPresenter as Presenter;
use Modules\Common\Entities\SitePage as Entity;

class RestSitePagesController extends Controller
{
    /**
     * @var Repository
     */
    private $repo;

    public function __construct(Repository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($searchString = $request->get('search_string')) {

            $entities = $this->repo
                ->load(['cover', 'files'])
                ->setPresenter($this->getPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                });

        } else {
            $entities = $this->repo
                ->with(['cover', 'files'])
                ->setPresenter($this->getPresenter())
                ->paginate();
        }


        return \Response::json($entities, 206);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('common::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title'   => 'required|max:255',
            'summary' => 'string|max:1024',
            'body'    => 'required',
            'cover'   => 'file|max:2048|image:jpeg,png,bmp,gif,svg',
            'files.*' => 'file|max:20480',

        ]);
        $entity = Entity::create($request->all());

        if($request->file('files')) {
            $fileAdders = $entity->addMultipleMediaFromRequest(['files']);

            $fileAdders->each(function($fileAdder) {
                $fileAdder->toMediaCollection(Entity::FILES_MEDIA_COLLECTION_NAME);
            });
        };

        //Add new media file
        if ($request->file('cover')) {
            $entity->addMediaFromRequest('cover')
                     ->toMediaCollection(Entity::COVER_MEDIA_COLLECTION_NAME);
        }


        $entity->load('cover', 'files');

        return \Response::json($this->getPresenter()->present($entity), 201);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(int $id)
    {
        $entity = $this->repo
            ->with(['cover', 'files'])
            ->setPresenter($this->getPresenter())
            ->find($id);

        return \Response::json($entity, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('common::edit');
    }

    /**
     * Update the specified resource in storage.
     * @apram int $id
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title'   => 'required|max:255',
            'summary' => 'string|max:1024',
            'body'    => 'required',
        ]);

        $entity = $this->repo->find($id);
        $entity->update($request->all());

        $entity->load('files', 'cover');

        return \Response::json($this->getPresenter()->present($entity), 200);
    }

    /**
     * Updates media file nested in the content media type entity
     * @param Request $request
     * @param int $id
     */
    public function updateCover(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'cover' => 'file|max:2048|image:jpeg,png,bmp,gif,svg',
        ]);

        $entity = $this->repo
            ->find($id);

        // Delete existing cover if exists
        if ($cover = $entity->cover) {
            $cover->delete();
        }

        //Add new media file
        $entity->addMediaFromRequest('cover')
              ->toMediaCollection(Entity::COVER_MEDIA_COLLECTION_NAME);

        $entity->load('cover');

        return \Response::json((new MediaPresenter())->present($entity->cover), 201);
    }

    /**
     * Updates media file nested in the content media type entity
     * @param Request $request
     * @param int $id
     */
    public function destroyCover(Request $request, int $id)
    {
        $entity = $this->repo
            ->find($id);

        // Delete existing media file
        if ($cover = $entity->cover) {
            return \Response::json($cover->delete(), 204);
        }

        return \Response::json(false, 204);
    }

    /**
     * Remove the specified resource from storage.
     * @apram int $id
     * @return Response
     */
    public function destroy(int $id)
    {
        return \Response::json($this->repo->delete($id), 204);
    }

    public function getPresenter()
    {
        return new Presenter();
    }
}

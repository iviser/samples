<?php

// Branch offices routes
Route::group(['middleware' => 'web', 'prefix' => 'offices', 'namespace' => 'Modules\Common\Http\Controllers'], function()
{
    Route::get('/', 'ContactController@branches')->name('common.contact.branches');
    Route::get('/data/', 'ContactController@data')->name('common.contact.data');
    Route::get('/data/{id}', 'ContactController@dataId')->name('common.contact.data.id');
});

// Contact-us routes
Route::group(['middleware' => 'web', 'prefix' => 'contact-us', 'namespace' => 'Modules\Common\Http\Controllers'], function()
{
    Route::post('/post', 'ContactController@feedbackPost')->name('common.contact.feedback.post');
    Route::get('/', 'ContactController@feedback')->name('common.contact.feedback');
});

// Staff routes
Route::group(['middleware' => 'web', 'prefix' => 'staff', 'namespace' => 'Modules\Common\Http\Controllers'], function()
{
    Route::get('/', 'StaffController@index')->name('common.staff.index');
});

Route::group(['middleware' => 'web', 'prefix' => 'site-pages', 'namespace' => 'Modules\Common\Http\Controllers'], function()
{
    Route::get('{id}', 'SitePagesController@index')->name('site_pages.item');
});

// Site pages routes
Route::group(
    ['middleware' => 'auth:api', 'prefix' => '/api/v1/site-pages', 'namespace' => 'Modules\Common\Http\Controllers\Api'],
    function() {
        Route::get('/', 'RestSitePagesController@index')->name('api.site_pages.index');
        Route::get('{id}', 'RestSitePagesController@show')->name('api.site_pages.show');
        Route::post('/', 'RestSitePagesController@store')->name('api.site_pages.create');
        Route::post('{id}', 'RestSitePagesController@update')->name('api.site_pages.update');
        Route::delete('{id}', 'RestSitePagesController@destroy')->name('api.site_pages.delete');

        // Cover
        Route::post('/{id}/cover', 'RestSitePagesController@updateCover')
             ->name('api.site_pages.cover.update');
        Route::delete('/{id}/cover', 'RestSitePagesController@destroyCover')
             ->name('api.site_pages.cover.delete');

        // Files
        Route::get('{site_page_id}/files', 'RestSitePagesFilesController@index')
             ->name('api.site_pages.files.index');
        Route::get('{site_page_id}/files/{file_id}', 'RestSitePagesFilesController@show')
             ->name('api.site_pages.files.show');
        Route::post('{site_page_id}/files', 'RestSitePagesFilesController@store')
             ->name('api.site_pages.files.create');
        Route::delete('{site_page_id}/files/{file_id}', 'RestSitePagesFilesController@destroy')
             ->name('api.site_pages.files.delete');
    }
);

//REST Staff routes
Route::group(
    ['middleware' => 'auth:api', 'prefix' => '/api/v1/staff', 'namespace' => 'Modules\Common\Http\Controllers\Api'],
    function() {
        Route::get('/', 'RestStaffController@index')->name('api.staff.index');
        Route::get('{id}', 'RestStaffController@show')->name('api.staff.show');
        Route::post('/', 'RestStaffController@store')->name('api.staff.create');
        Route::post('{id}', 'RestStaffController@update')->name('api.staff.update');
        Route::delete('{id}', 'RestStaffController@destroy')->name('api.staff.delete');

        // Avatar
        Route::post('/{id}/avatar', 'RestStaffController@updateAvatar')
            ->name('api.staff.avatar.update');
        Route::delete('/{id}/avatar', 'RestStaffController@destroyAvatar')
            ->name('api.staff.avatar.delete');
    }
);
<?php

namespace Modules\Common\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Modules\Common\Repositories\Presenters\StaffPresenter;
use Modules\Common\Repositories\StaffRepository;

class StaffQuery extends Query
{
    protected $attributes = [
        'name' => 'staff'
    ];

    public function type()
    {
        return GraphQL::type('Staff');
    }

    /**
     * @return StaffRepository
     */
    private function getRepository()
    {
        return app(StaffRepository::class);
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' =>  Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args)
    {
        $result = $this->getRepository()->with(['avatar'])
            ->setPresenter(new StaffPresenter())
            ->findByField('id', $args['id']);

        return $result ? $result[0] : null;
    }
}
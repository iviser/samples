<?php

namespace Modules\Common\GraphQL\Types\Input;


use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class StaffUpdateInput extends GraphQLType
{
    protected $attributes = [
        'name' => 'StaffUpdateInput',
        'description' => 'Staff input type'
    ];

    protected $inputObject = true;

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the staff entry'
            ],
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Staff\'s title',
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'Staff\'s description',
            ],
            'avatar' => [
                'type' => GraphQL::type('TempFile'),
                'description' => 'Person\'s image file'
            ],
        ];
    }
}

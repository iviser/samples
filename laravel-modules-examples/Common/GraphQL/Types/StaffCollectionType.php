<?php

namespace Modules\Common\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class StaffCollectionType extends GraphQLType
{
    protected $attributes = [
        'name' => 'StaffCollectionType',
        'description' => 'Collection of staff'
    ];

    public function fields()
    {
        return [
            'total' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Total items'
            ],
            'offset' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items offset. How many items skipped'
            ],
            'size' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items size. How many items are taken'
            ],
            'items' => [
                'type' => Type::listOf(\GraphQL::type('Person')),
                'description' => 'Empty interface'
            ],
        ];
    }

}


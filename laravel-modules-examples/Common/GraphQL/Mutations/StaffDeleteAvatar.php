<?php

namespace Modules\Common\GraphQL\Mutations;

use App\Entities\Media;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Modules\Common\Repositories\StaffRepository;
use Storage;

class StaffDeleteAvatar extends Mutation
{

    protected $attributes = [
        'name' => 'staffDeleteAvatar'
    ];

    public function type()
    {
        return Type::boolean();
    }

    /**
     * @return StaffRepository
     */
    private function getRepository()
    {
        return app(StaffRepository::class);
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args)
    {
        try {

            $entity = $this->getRepository()->find($args['id']);

            if (($avatar = $entity->avatar) instanceof Media) {
                $avatar->forceDelete();
            }

            return true;

        } catch (\Exception $e) {

            return false;

        }

    }

}
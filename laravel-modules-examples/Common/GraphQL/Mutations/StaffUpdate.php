<?php

namespace Modules\Common\GraphQL\Mutations;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Modules\Common\Entities\Staff;
use Modules\Common\Repositories\Presenters\StaffPresenter;
use Modules\Common\Repositories\StaffRepository;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StaffUpdate extends Mutation
{

    protected $attributes = [
        'name' => 'staffUpdate'
    ];

    public function type()
    {
        return GraphQL::type('Staff');
    }

    /**
     * @return StaffRepository
     */
    private function getRepository()
    {
        return app(StaffRepository::class);
    }

    public function args()
    {
        return [
            'staff' => ['name' => 'staff', 'type' => Type::nonNull(GraphQL::type('StaffUpdateInput'))],
        ];
    }

    public function resolve($root, $args)
    {
        $entity = $this->getRepository()
            ->with(['avatar'])
            ->setPresenter(new StaffPresenter())
            ->update( $args['staff'], $args['staff']['id']);

        if($entity && isset($args['staff']['avatar']) && !empty($avatar = $args['staff']['avatar'])) {

            $entity = Staff::find($args['staff']['id']);

            // TODO handle this logic via factory class
            $filePath = base_path() . \Storage::disk('temp')->url($avatar['temp_filename']);

            $file = new UploadedFile(
                $filePath,
                $avatar['origin_filename'],
                $avatar['origin_type'],
                $avatar['origin_size']
            );

            // We need to delete the old avatar
            if ($entity->avatar) {
                $entity->avatar->delete();
            }

            $entity->addMedia($file)->toMediaCollection('avatar', 'media');
            $entity->save();

            // Clean up temp file
            Storage::disk('temp')->delete($avatar['temp_filename']);

            // Load updated entity
            $entity = $this->getRepository()
                           ->with(['avatar'])
                           ->setPresenter(new StaffPresenter())
                           ->update( $args['staff'], $args['staff']['id']);
        }


        return $entity ?: null;
    }

}
<?php

namespace Modules\Common\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use Modules\Common\Entities\Traits\ThumbnailTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Media;

class Branch extends Model implements HasMediaConversions, EntityThumbInterface
{
    use HasMediaTrait;
    use ThumbnailTrait;

    // Branch types
    const TYPE_BRANCH_OFFICE = 'branch_office';
    const TYPE_RESEARCH_CENTER = 'research_center';

    protected $fillable = [];

    protected $table = 'branches';

    protected $casts = [
        'address' => 'object',
        'geocode' => 'object',
        'contacts' => 'object',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->sharpen(10);
    }
}

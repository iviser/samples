<?php

namespace Modules\Common\Entities;


use App\Entities\Media;
use App\Entities\Traits\EsSearchable;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Interfaces\ContentInterface;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use App\Entities\Traits\ContentTrait;
use Modules\Common\Entities\Traits\ThumbnailTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class SitePage extends Model implements HasMedia, EntityThumbInterface, ContentInterface
{
    use HasMediaTrait, ContentTrait, ThumbnailTrait, EsSearchable;

    const FILES_MEDIA_COLLECTION_NAME = 'site_page_files';
    const COVER_MEDIA_COLLECTION_NAME = 'cover';

    protected $table = 'site_pages';

    protected $contentTypeName = 'site_page';

    protected $fillable = [
        'title', 'summary', 'body',
    ];

    protected $routeName = 'site_pages.item';


    public function files()
    {
        return $this->media()->where('collection_name', '=', self::FILES_MEDIA_COLLECTION_NAME);
    }

    /**
     * Cover media file
     *
     * @return $this
     */
    public function cover()
    {
        return $this->morphOne(Media::class, 'model')
                    ->where('collection_name', '=', static::COVER_MEDIA_COLLECTION_NAME);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
             ->width(150)
             ->sharpen(10);
    }



}
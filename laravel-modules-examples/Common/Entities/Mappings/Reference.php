<?php

namespace Modules\Common\Entities\Mappings;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reference
 * @package Modules\Common\Entities\Mappings
 */
class Reference
{

    /**
     * @var string
     */
    public $type;

    /**
     * @var int
     */
    public $id;

    /**
     * Reference constructor.
     *
     * @param string $type
     * @param int $id
     */
    public function __construct(string $type, int $id)
    {
        $this->id = $id;
        $this->type = $type;
    }

    /**
     * Creates a reference form Model Entry
     *
     * @param Model $modelEntry
     *
     * @return Reference
     */
    public static function make(Model $modelEntry): Reference
    {
        return  new static(get_class($modelEntry), (integer) $modelEntry->id);
    }

    /**
     * Loads referenced model entry or fails
     */
    public function load()
    {
        return ($this->type)::find($this->id);
    }
}
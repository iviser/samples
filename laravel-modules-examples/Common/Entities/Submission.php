<?php

namespace Modules\Common\Entities;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $fillable = ['subject', 'sender', 'receiver', 'message'];

    protected $casts = [
        'sender' => 'object',
        'receiver' => 'object',
    ];

    protected $table = 'submissions';

}

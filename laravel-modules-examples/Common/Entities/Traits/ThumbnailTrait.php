<?php


namespace Modules\Common\Entities\Traits;
use App\Http\Helpers\File;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

/**
 * Class ThumbnailTrait
 * @package Modules\Common\Entities\Traits
 */
trait ThumbnailTrait
{
    /**
     * Get snippet thumbnail url
     *
     * @return string
     */
    public function getThumbUrlAttribute($value): string
    {
        $thumb = null;

        if ($media = $this->getMedia()->first()) {
            $thumb = $media->getUrl('thumb');
        }

        return $thumb ?: '';
    }

    public function getThumbnailAttribute($value)
    {
        return $this->getThumbSitePathAttribute($value);
    }

    /**
     * Get snippet thumbnail url
     *
     * @return string
     */
    public function getThumbSitePathAttribute($value): string
    {
        $thumb = null;

        if ($media = $this->media->first()) {

            if (File::exists($media->getPath('thumb'))) {
                $thumb = str_replace(base_path('storage'), '', $media->getPath('thumb')) ?: null;
            }
        }


        return $thumb ?: $this->getDefaultThumbSitePathAttribute($value);
    }

    /**
     * {@inheritdoc}
     *
     * @return string
     */
    public function getDefaultThumbSitePathAttribute($value): string
    {
        $contentDefaultDir = public_path('files/default/' . str_replace(['_'], '-',$this->getTable()));
        $path = (file_exists($contentDefaultDir)) ? $contentDefaultDir : public_path('files/default/content-thumbnails');

        $files = glob($path . '/*.*');
        if (!$files) {
            throw new FileNotFoundException("Files are not found in:'{$path}'");
        }
        $file = array_rand($files);

        return str_replace(base_path('public'), '', $files[$file]);
    }

}

<?php

namespace Modules\Common\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Entities\MediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Staff extends Model implements HasMedia
{

    use MediaTrait;

    const AVATAR_MEDIA_COLLECTION_NAME = 'avatar';

    protected $fillable = [
        'title',
        'description',
        'contacts',
    ];

    protected $table = 'staff';

    public function avatar()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
                    ->where('collection_name', 'avatar');
    }
}

<?php

namespace Modules\Common\Import\Sources;


interface DbSourceInterface
{

    /**
     * Get total amount items
     *
     * @return int
     */
    public function count() :int;


    /**
     * Get single item
     *
     * @param $identifier;
     * @return mixed
     */
    public function item($identifier);

    /**
     * Get batch
     *
     * @param int $limit
     * @param int $offset
     *
     * @return mixed
     */
    public function batch(int $limit, int $offset);
}
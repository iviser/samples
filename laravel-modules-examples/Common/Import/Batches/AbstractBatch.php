<?php

namespace Modules\Common\Import\Batches;


use Mockery\Exception;
use Modules\Common\Import\Mappers\MapperInterface;
use Modules\Common\Import\Sources\DbSourceInterface;

abstract class AbstractBatch implements BatchInterface
{

    protected $name;

    /**
     * @var DbSourceInterface
     */
    protected $source;

    /**
     * @var MapperInterface
     */
    protected $mapper;

    /**
     * @return string
     */
    public function name(): string
    {
        if (!$this->name) {
            throw new Exception('Name property is not set');
        }

        return $this->name;
    }

    abstract protected function source(): DbSourceInterface;

    public function getSource(): DbSourceInterface
    {
        if (!$this->source) {
            $this->source = $this->source();
        }

        return $this->source;
    }

    abstract public function mapper(): MapperInterface;

    public function getMapper(): MapperInterface
    {
        if (!$this->mapper) {
            $this->mapper = $this->mapper();
        }

        return $this->mapper;
    }

    abstract public function importBulk(int $limit, int $offset);

    abstract public function importItem(int $id);


}

<?php

namespace Modules\Common\Repositories\Presenters;

use Modules\Common\Repositories\Transformers\SitePageTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class SitePagesPresenter extends FractalPresenter
{

    public function getTransformer()
    {
        return new SitePageTransformer();
    }

}
<?php

namespace Modules\Common\Repositories\Presenters;

use Modules\Common\Repositories\Transformers\StaffTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class StaffPresenter extends FractalPresenter
{

    public function getTransformer()
    {
        return new StaffTransformer();
    }

}
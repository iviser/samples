<?php

namespace Modules\Common\Repositories\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Common\Entities\Staff as Entity;
use Modules\Media\Repositories\Transformers\MediaFileTransformer;

class StaffTransformer extends TransformerAbstract
{

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'avatar'
    ];

    public function transform(Entity $entity)
    {
        return [
            'id' => (int) $entity->id,
            'title' => $entity->title,
            'description' => $entity->description,
        ];
    }

    public function includeAvatar(Entity $entity)
    {
        $avatar = $entity->relationLoaded('avatar')
            ? $entity->getRelation('avatar')
            : null;

        return $avatar ? $this->item($avatar, new MediaFileTransformer()) : null;
    }

}
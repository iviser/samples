<?php

namespace Modules\Common\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Common\Entities\Staff;

class StaffRepository extends BaseAppRepository
{
    public function model()
    {
        return Staff::class;
    }
}
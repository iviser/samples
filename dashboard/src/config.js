import Vue from 'vue'
import vueConfig from 'vue-config'

const config = {
  graphQL: {
    public: {
      uri: process.env.GRAPHQL_PUBLIC
    },
    secret: {
      uri: process.env.GRAPHQL_SECRET
    }
  },
  app_api: {
    host: process.env.EGW_API_HOST,
    prefix: 'api/v1/',
    endpoints: {
      file_upload: 'file-upload'
    }
  },
  egw_dev: process.env.EGW_DEV,
  tokenStorage: {
    name: 'tokenStorage'
  }
}

Vue.use(vueConfig, config)

export default config

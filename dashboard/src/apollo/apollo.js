import Vue from 'vue'
import ApolloClient from 'apollo-client'
import { createNetworkInterface } from 'apollo-upload-client'

import VueApollo from 'vue-apollo'
import config from '../config'

// Create the apollo client
const apolloClient = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: config.graphQL.public.uri,
    transportBatching: true
  }),
  connectToDevTools: true
})

const secretNetworkInterface = createNetworkInterface({
  uri: config.graphQL.secret.uri,
  transportBatching: true
})

secretNetworkInterface.use([{
  applyMiddleware (req, next) {
    if (!req.options.headers) {
      req.options.headers = {}  // Create the header object if needed.
    }
    if (localStorage.getItem(config.tokenStorage.name)) {
      let tokenObj = JSON.parse(localStorage.getItem(config.tokenStorage.name))
      req.options.headers.Authorization = `Bearer ${tokenObj.access_token}`
    }
    next()
  }
}])

// Create secure apollo client
const secretApolloClient = new ApolloClient({
  networkInterface: secretNetworkInterface,
  connectToDevTools: true
})

// Install the vue plugin
Vue.use(VueApollo)

export default new VueApollo({
  clients: {
    public: apolloClient,
    secret: secretApolloClient
  },
  defaultClient: apolloClient
})

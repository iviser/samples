import config from '../../config'

// initial state
const state = {
  accessToken: {}
}

// getters
const getters = {
}

// actions
const actions = {

}

// mutations
const mutations = {
  storeAccessToken: function () {
    state.accessToken = JSON.parse(localStorage.getItem(config.tokenStorage.name))
  }

}

export default {
  namespaced: true, // allows to have own getters, setter, actions and mutations detached from global store's scope
  state,
  getters,
  actions,
  mutations
}

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App'
import store from './store'
import apolloProvider from './apollo/apollo'
import config from './config'
import router from './router'

// Global custom components
import Pager from './global/components/Pager'
import MultiselectRemote from './global/components/MultiselectRemote'
import SelectRemote from './global/components/SelectRemote'
import ImageUploader from './global/components/ImageUploader'
import AudioUploader from './global/components/AudioUploader'
import FilesUploader from './global/components/FilesUploader'
import Ckeditor from './global/components/Ckeditor'
import Datepicker from './global/components/Datepicker'
import Spinner from 'vue-strap/src/Spinner'
import * as VueGoogleMaps from 'vue2-google-maps'

// Load global components
Vue.component('pager', Pager)
Vue.component('multiselect-remote', MultiselectRemote)
Vue.component('select-remote', SelectRemote)
Vue.component('image-uploader', ImageUploader)
Vue.component('audio-uploader', AudioUploader)
Vue.component('files-uploader', FilesUploader)
Vue.component('ckeditor', Ckeditor)
Vue.component('datepicker', Datepicker)
Vue.component('spinner', Spinner)

Vue.use(VueResource)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAtKJall6X5HN_b5hJW21Eq345uDsaf92c',
    v: '3',
    libraries: 'places'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  config,
  router,
  store,
  apolloProvider,
  template: '<App/>',
  components: { App },
  mounted () {
    store.commit('user/storeAccessToken')
  }
})

import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'
import Search from '@/views/Search'
import Content from '@/views/content/Index'
// Content Views
import PublicationIndex from '@/views/content/publication/Index'
import PublicationEdit from '@/views/content/publication/Edit'
import CorrespondenceIndex from '@/views/content/correspondence/Index'
import CorrespondenceEdit from '@/views/content/correspondence/Edit'
import PersonsList from '@/views/content/people/Index'
import PersonEdit from '@/views/content/people/Edit'
import NewsList from '@/views/content/news/Index'
import NewsEdit from '@/views/content/news/Edit'
import ArticlesList from '@/views/content/articles/Index'
import ArticlesEdit from '@/views/content/articles/Edit'
import LessonsList from '@/views/content/lessons/Index'
import LessonsEdit from '@/views/content/lessons/Edit'
import LocationsList from '@/views/content/locations/Index'
import LocationsEdit from '@/views/content/locations/Edit'
import LetterbooksList from '@/views/content/letterbooks/Index'
import LetterbooksEdit from '@/views/content/letterbooks/Edit'
import EventsList from '@/views/content/events/Index'
import EventsEdit from '@/views/content/events/Edit'
import ImagesList from '@/views/content/images/Index'
import ImagesEdit from '@/views/content/images/Edit'
import DocumentsList from '@/views/content/documents/Index'
import DocumentsEdit from '@/views/content/documents/Edit'
import AudioList from '@/views/content/audio/Index'
import AudioEdit from '@/views/content/audio/Edit'
import MediaList from '@/views/media/Index'
import MediaEdit from '@/views/media/Edit'

// Serve pages
import Login from '@/views/pages/Login'

Vue.use(Router)

const router = new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: localStorage.getItem('tokenStorage') ? '/admin/dashboard' : '/login'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/admin',
      redirect: '/admin/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'search',
          name: 'Search settings',
          component: Search
        },
        {
          path: 'media',
          name: 'Media',
          component: MediaList,
          children: [
            {
              path: ':id',
              name: 'MediaEdit',
              component: MediaEdit
            }
          ]
        },
        {
          path: 'content',
          name: 'Content',
          component: Content,
          children: [
            {
              path: 'publication',
              name: 'publications',
              component: PublicationIndex,
              children: [
                {
                  path: ':id',
                  name: 'PublicationEdit',
                  component: PublicationEdit
                }
              ]
            },
            {
              path: 'correspondence',
              name: 'correspondence',
              component: CorrespondenceIndex,
              children: [
                {
                  path: ':id',
                  name: 'CorrespondenceEdit',
                  component: CorrespondenceEdit
                }
              ]
            },
            {
              path: 'people',
              name: 'People',
              component: PersonsList,
              children: [
                {
                  path: ':id',
                  name: 'PersonEdit',
                  component: PersonEdit
                }
              ]
            },
            {
              path: 'news',
              name: 'News',
              component: NewsList,
              children: [
                {
                  path: ':id',
                  name: 'NewsEdit',
                  component: NewsEdit
                }
              ]
            },
            {
              path: 'articles',
              name: 'Articles',
              component: ArticlesList,
              children: [
                {
                  path: ':id',
                  name: 'ArticlesEdit',
                  component: ArticlesEdit
                }
              ]
            },
            {
              path: 'lessons',
              name: 'Lessons',
              component: LessonsList,
              children: [
                {
                  path: ':id',
                  name: 'LessonsEdit',
                  component: LessonsEdit
                }
              ]
            },
            {
              path: 'locations',
              name: 'Locations',
              component: LocationsList,
              children: [
                {
                  path: ':id',
                  name: 'LocationsEdit',
                  component: LocationsEdit
                }
              ]
            },
            {
              path: 'letterbooks',
              name: 'Letterbooks',
              component: LetterbooksList,
              children: [
                {
                  path: ':id',
                  name: 'LetterbooksEdit',
                  component: LetterbooksEdit
                }
              ]
            },
            {
              path: 'events',
              name: 'Events',
              component: EventsList,
              children: [
                {
                  path: ':id',
                  name: 'EventsEdit',
                  component: EventsEdit
                }
              ]
            },
            {
              path: 'images',
              name: 'Images',
              component: ImagesList,
              children: [
                {
                  path: ':id',
                  name: 'ImagesEdit',
                  component: ImagesEdit
                }
              ]
            },
            {
              path: 'documents',
              name: 'Documents',
              component: DocumentsList,
              children: [
                {
                  path: ':id',
                  name: 'DocumentsEdit',
                  component: DocumentsEdit
                }
              ]
            },
            {
              path: 'audio',
              name: 'Audio',
              component: AudioList,
              children: [
                {
                  path: ':id',
                  name: 'AudioEdit',
                  component: AudioEdit
                }
              ]
            }
          ]
        }
      ]
    }
  ]
})

export default router
